﻿using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ApolloWebsite.Department
{
    public partial class Login : System.Web.UI.Page
    {
        HttpClient client;
        private Logger Logger = LogManager.GetCurrentClassLogger();
        protected void Page_Load(object sender, EventArgs e)
        {
            txtUserID.Focus();
            client = Logic.SyncLogins.InitiateHttp(txtUserID.Text, txtPassword.Text);
            //FeedBackQuestionInitScript feedbackScript = new FeedBackQuestionInitScript();
            //if (!IsPostBack)
            //{
            //    feedbackScript.RunInitScript();
            //}
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                Logger.Info("Starts When Submit Button Clicks in Default.aspx");
                string struri = "api/Login/Validate?userName=" + txtUserID.Text + "&password=" + txtPassword.Text + "&loginType=D";
                HttpResponseMessage response = client.GetAsync(struri).Result;
                if (response.StatusCode.Equals(HttpStatusCode.OK))
                {
                    long companyId = response.Content.ReadAsAsync<long>().Result;
                    if (companyId > 0)
                    {
                        Session["adminusername"] = txtUserID.Text;
                        Session["adminpassword"] = txtPassword.Text;
                        Logic.SyncLogins syncLogin = new Logic.SyncLogins();
                        Logic.SyncLogins.InitiateHttp(txtUserID.Text, txtPassword.Text);
                        syncLogin.InitiateSync(companyId, "D");
                        //Session["companyadminid"] = strCompanyId;
                        //HttpCookie username = new HttpCookie("username");
                        //username.Value = txtUserID.Text;
                        //username.Expires = DateTime.Now.AddHours(1);
                        //Response.Cookies.Add(username);
                        //Encoding iso = Encoding.GetEncoding("ISO-8859-1");
                        //Encoding utf8 = Encoding.UTF8;
                        //byte[] utfBytes = utf8.GetBytes(txtPassword.Text);
                        //byte[] isoBytes = Encoding.Convert(utf8, iso, utfBytes);
                        //HttpCookie password = new HttpCookie("password");
                        //password.Value = Convert.ToBase64String(isoBytes);
                        //password.Expires = DateTime.Now.AddHours(1);
                        //Response.Cookies.Add(password);
                        Session["adminType"] = "D";
                        Session["adminloginid"] = companyId;
                        Response.Redirect("../Home.aspx");
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(GetType(), "notifier15", "wariningAlert('Please enter valid User Id and Password');", true);
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "notifier15", "wariningAlert('Please enter valid User Id and Password');", true);
                }
                Logger.Info("Ends When Submit Button Clicks in Default.aspx");
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }
    }
}