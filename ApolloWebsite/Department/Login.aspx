﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="ApolloWebsite.Department.Login" %>

<!DOCTYPE html>

<html>
  <head>
    <meta charset="UTF-8">
    <title>Hospitality | Department Login</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="../dist/css/AdminLTE.min.css" rel="stylesheet" />
    <!-- iCheck -->
    <link href="../plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl("../bootstrap/css/CustomStyle.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl("../bootstrap/css/alertify/themes/default.rtl.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl("../bootstrap/css/alertify/bootstrap-theme.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl("../bootstrap/css/alertify/bootstrap-theme.min.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl("../bootstrap/css/alertify/alertify.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl("../bootstrap/css/alertify/alertify.min.css")%>" rel="stylesheet" type="text/css" />
    <script src="<%= Page.ResolveUrl("../bootstrap/js/alertify/alertify.js")%>"></script>
    <script src="<%= Page.ResolveUrl("../bootstrap/js/alertify/jquery-1.10.2.js")%>"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="icon" type="image/ico" href="Icons/TOCico.ico"/>
       <script type="text/javascript">
           function successAlert(msg) {
               alertify.success(msg);
           }
           function warningAlert(msg) {
               alertify.warning(msg);
           }
           function errorAlert(msg) {
               alertify.error(msg);
           }
           function messageAlert(msg) {
               alertify.message(msg);
           }
           function ConfirmDelete() {
               var x = confirm("Are you sure you want to Delete?");
               if (x == true)
                   return true;
               return false;
           }
    </script>
    <%--<script src="http://code.jquery.com/jquery-1.9.0.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>--%>
    <script>
        $(document).ready(function () {
            $(".username").keyup(function () {
                var username = $(".username").val();
                if (username == "") {
                    $(".usernameError").text("Please Enter UserID.");
                    return false;
                }
                else {
                    $(".usernameError").text("");
                }
            });
            $(".password").keyup(function () {
                var password = $(".password").val();
                if (password == "") {
                    $(".passwordError").text("Please Enter Password.");
                    return false;
                }
                else {
                    $(".passwordError").text("");
                }
            });
            $(".validate").click(function () {
                var count = 0;
                var username = $(".username").val();
                var password = $(".password").val();
                if (username == "") {
                    $(".usernameError").text("Please Enter UserID.");
                    count = count + 1;
                }
                if (password == "") {
                    $(".passwordError").text("Please Enter Password.");
                    count = count + 1;
                }
                if (count > 0) {
                    return false;
                }
            });
        });
    </script>
  </head>
  <body class="login-page">
      <form id="form1" runat="server" autocomplete="off">
    <div class="login-box">
      <div class="login-logo">
        <a href="javascript:void(0);"><b>Hospitality</b> Backend</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
          <span class="usernameError error-code"></span>
          <div class="form-group has-feedback">
            <asp:TextBox ID="txtUserID" runat="server" class="form-control username" placeholder="User ID" ></asp:TextBox>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div> 
          <span class="passwordError error-code"></span>
          <div class="form-group has-feedback">
            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" class="form-control password" 
                placeholder="Password" ></asp:TextBox>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">
              
            </div><!-- /.col -->
            <div class="col-xs-4">
              <asp:Button ID="btnSubmit" runat="server" Text="Sign In" class="btn btn-primary btn-block btn-flat validate"
                  OnClick="btnSubmit_Click"/>
            </div><!-- /.col -->
          </div>
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
    <!-- jQuery 2.1.4 -->
    <script src="../plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="../plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
</form>
  </body>
</html>