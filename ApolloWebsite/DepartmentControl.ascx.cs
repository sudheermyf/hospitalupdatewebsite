﻿using ApolloDB.DBFolder;
using ApolloDB.Models;
using ApolloWebsite.Logic;
using CMCAPI.Client.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ApolloWebsite
{
    public partial class DepartmentControl : System.Web.UI.UserControl
    {
        List<StoreDTO> lstTotalStores;
        SyncLogins synclogin = new SyncLogins();
        HttpClient client;
        string username = string.Empty, password = string.Empty;
        protected string adminType = string.Empty;
        long adminid = 0;
        public delegate void BindSelection(string abcd);
        public event BindSelection bindRelatedData;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["adminloginid"] != null)
            {
                adminType = Convert.ToString(Session["adminType"], CultureInfo.CurrentCulture);
                if (Int64.TryParse(Convert.ToString(Session["adminloginid"], CultureInfo.CurrentCulture), out adminid) && adminid > 0)
                {

                }
                else
                {
                    Response.Redirect("../Default.aspx");
                }
                username = Convert.ToString(Session["adminusername"], CultureInfo.CurrentCulture);
                password = Convert.ToString(Session["adminpassword"], CultureInfo.CurrentCulture);
            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }
            client = Logic.SyncLogins.InitiateHttp(username, password);
            client.DefaultRequestHeaders.Add("AdminType", adminType);
            if (!IsPostBack)
            {
                switch (adminType)
                {
                    case "A":
                        ddlDepartments.Visible = true;
                        lblDepartments.Visible = true;
                        ddlStores.Visible = true;
                        departments.Attributes.Remove("hidedept");
                        LoadDepartments();
                        break;
                    case "D":
                        lblDepartments.Visible = false;
                        ddlDepartments.Visible = false;
                        departments.Attributes.Add("class", "hidedept");
                        LoadStoresUnderDepartment();
                        break;
                    default:
                        lblDepartments.Visible = false;
                        ddlDepartments.Visible = false;
                        departments.Attributes.Add("class", "hidedept");
                        lblStores.Visible = false;
                        ddlStores.Visible = false;
                        break;
                }
            }
        }
        private void LoadDepartments()
        {
            ddlDepartments.DataSource = null;
            ddlDepartments.DataBind();
            List<DepartmentDTO> lstNewDepartments = new List<DepartmentDTO>();
            lstNewDepartments = synclogin.GetDepartments();
            if (lstNewDepartments != null && lstNewDepartments.Count > 0)
            {
                ddlDepartments.DataSource = lstNewDepartments;
                ddlDepartments.DataBind();
            }
        }
        private void LoadStoresUnderDepartment()
        {

            if (lstTotalStores == null)
            {
                lstTotalStores = new List<StoreDTO>();
            }
            ddlStores.Items.Clear();
            lstTotalStores = synclogin.GetStoresUnderDepartment(adminid);
            if (lstTotalStores.Count > 0)
            {
                ddlStores.DataSource = lstTotalStores;
                ddlStores.DataTextField = "StoreName";
                ddlStores.DataValueField = "SNo";
                ddlStores.DataGroupField = "DeptName";
                ddlStores.DataBind();
            }
        }
        protected void ddlDepartments_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<long> lstDepartments = new List<long>();
            lstDepartments= Array.ConvertAll(ddlDepartments.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
            //lstDepartments.AddRange(ddlDepartments.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray());
            List<StoreDTO> lstStores = new List<StoreDTO>();
            ddlStores.Items.Clear();
            lstDepartments.ForEach(c =>
            {
                lstStores = synclogin.GetStoresUnderDepartment(c);
                if (lstTotalStores == null)
                {
                    lstTotalStores = new List<StoreDTO>();
                }
                lstTotalStores.AddRange(lstStores);
            });
            if (lstTotalStores.Count > 0)
            {
                ddlStores.DataSource = lstTotalStores;
                ddlStores.DataTextField = "StoreName";
                ddlStores.DataValueField = "SNo";
                ddlStores.DataGroupField = "DeptName";
                ddlStores.DataBind();
            }
            bindRelatedData("Asdf");
        }
    }
}