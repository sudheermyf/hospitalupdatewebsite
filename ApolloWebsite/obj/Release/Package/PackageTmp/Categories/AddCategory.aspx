﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMasterPage.Master" AutoEventWireup="true" CodeBehind="AddCategory.aspx.cs"  Inherits="ApolloWebsite.Categories.AddCategory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">    
    <script src="http://code.jquery.com/jquery-1.9.0.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>
    <style>
        .text1 {
            word-wrap: break-word;
        }
    </style>
<%--     <script type="text/javascript">
         function DisplayAlertAndRedirect() {
             successAlert("Deleted Successfully");
             location.href = "AddCategory.aspx";
             return;
         }
    </script>--%>
    <script>        
        $(document).ready(function () {
            
            $(".categDesc").keyup(function () {
                var categDesc = $(".categDesc").val();
                if (categDesc.length > 100)   //categName maximum length is 50 characters
                {
                    $(".categDescError").text("maximum 100 characters");
                    return false;
                }
                else {
                    $(".categDescError").text("");
                }
            });
            $(".categName").keyup(function () {
                var categName = $(".categName").val();
                if (categName == "") {
                    $(".categNameError").text("Please Enter Category Name.");
                    return false;
                }
                if (categName.length < 3)    //categName minimum length is 3 characters
                {
                    $(".categNameError").text("minimum 3 characters");
                    return false;
                }
                if (categName.length > 30)   //categName maximum length is 30 characters
                {
                    $(".categNameError").text("maximum 30 characters");
                    return false;
                }
                else {
                    $(".categNameError").text("");
                }
            });
            $(".imageupload1").change(function () {
                var btnText = $(".savebtn").val();
                var imageupload1 = $(".imageupload1").val();
                var pngimg = imageupload1.substring(imageupload1.lastIndexOf(".") + 1, imageupload1.length);
                
                if (btnText == "Submit") {
                    if (imageupload1 == "") {
                        $(".fuCategoryImageError").text("Please Choose Image");
                        return false;
                    }
                    if (pngimg.toUpperCase() != "PNG") {
                        $(".fuCategoryImageError").text("Please Choose PNG Format Only");
                        return false;
                    }
                }
                if (btnText == "Update") {                    
                    if (imageupload1 != "") {
                        if (pngimg.toUpperCase() != "PNG") {
                            $(".fuCategoryImageError").text("Please Choose PNG Format Only");
                            return false;
                        }
                        else {
                            $(".fuCategoryImageError").text("");
                        }
                    }
                }
                else {
                    $(".fuCategoryImageError").text("");
                }
            });
            $(".imageupload1").blur(function () {
                var imageupload1 = $(".imageupload1").val();
                var btnText = $(".savebtn").val();
                if (btnText == "Update") {
                    if (imageupload1 == "") {
                        $(".fuCategoryImageError").text("");
                    }
                }
            });
            $(".imageupload2").change(function () {
                var btnText = $(".savebtn").val();
                var imageupload2 = $(".imageupload2").val();
                var pngimg = imageupload2.substring(imageupload2.lastIndexOf(".") + 1, imageupload2.length);

                if (btnText == "Submit") {
                    if (imageupload2 == "") {
                        $(".fuBannerImageError").text("Please Choose Image");
                        return false;
                    }
                    if (pngimg.toUpperCase() != "PNG") {
                        $(".fuBannerImageError").text("Please Choose PNG Format Only");
                        return false;
                    }
                }
                if (btnText == "Update") {
                    if (imageupload2 != "") {
                        if (pngimg.toUpperCase() != "PNG") {
                            $(".fuBannerImageError").text("Please Choose PNG Format Only");
                            return false;
                        }
                        else {
                            $(".fuBannerImageError").text("");
                        }
                    }
                }
                else {
                    $(".fuBannerImageError").text("");
                }
            });
            $(".imageupload2").blur(function () {
                var imageupload2 = $(".imageupload2").val();
                var btnText = $(".savebtn").val();
                if (btnText == "Update") {
                    if (imageupload2 == "") {
                        $(".fuBannerImageError").text("");
                    }
                }
            });
            $(".savebtn").click(function () {
                debugger;
                var btnText = $(".savebtn").val();
                var count = 0;
                var categName = $(".categName").val();
                var imageupload1 = $(".imageupload1").val();
                var imageupload2 = $(".imageupload2").val();
                var pngimg1 = imageupload2.substring(imageupload2.lastIndexOf(".") + 1, imageupload2.length);
                var pngimg = imageupload1.substring(imageupload1.lastIndexOf(".") + 1, imageupload1.length);
                
                var categDesc = $(".categDesc").val();
                if (categDesc.length > 100)   //categName maximum length is 100 characters
                {
                    $(".categDescError").text("maximum 100 characters");
                    count = count + 1;
                }
                if (categName.length < 3)    //categName minimum length is 3 characters
                {
                    $(".categNameError").text("minimum 3 characters");
                    count = count + 1;
                }
                if (categName.length > 50)   //categName maximum length is 50 characters
                {
                    $(".categNameError").text("maximum 50 characters");
                    count = count + 1;
                }
                if (categName == "") {
                    $(".categNameError").text("Please Enter Category Name.");
                    count = count + 1;
                }
                if (btnText == "Submit") {
                    if (pngimg.toUpperCase() != "PNG") {
                        $(".fuCategoryImageError").text("Please Choose PNG Format Only");
                        count = count + 1;
                    }
                    if (imageupload1 == "") {
                        $(".fuCategoryImageError").text("Please Choose Image");
                        count = count + 1;
                    }
                    //fuBannerImageError
                }
                else if (btnText == "Update") {                   
                    if (imageupload1 != "") {
                        if ($('#fuCateg').text() == "Please Choose PNG Format Only") {
                            count = count + 1;
                        }
                    }
                    if(imageupload2!="")
                    {
                        if ($('#fuBanner').text() == "Please Choose PNG Format Only") {
                            count = count + 1;
                        }
                    }
                }
                if (count > 0) {
                    return false;
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content-header">
        <h1>
            <asp:Label ID="lblPageHeading" runat="server" Text="Category" /><small>(Add / View)</small></h1>
        <ol class="breadcrumb">
            <li><a href="<%=Page.ResolveUrl("~/Home.aspx") %>"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a>Categories</a></li>
            <li class="active"><a href="javascript:void(0);">Category</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <div class="form-group">
                            <asp:Label ID="lblID" runat="server"  Visible="false"></asp:Label>
                            <asp:Label ID="Label1" runat="server" Text="Category Name"></asp:Label>
                            <asp:TextBox ID="txtCategoryName" runat="server"  class="form-control categName" placeholder="Enter CategoryName"/>
                            <span id="categError" runat="server" class="categNameError error-code"></span>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="lblDesc" runat="server" Text="Description"></asp:Label>
                            <asp:TextBox ID="txtCategDesc" runat="server" class="form-control categDesc" placeholder="Enter Description"/>
                             <span class="categDescError error-code"></span>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label3" runat="server" Text="Image"></asp:Label>
                            <asp:FileUpload ID="fuCategoryImage" CssClass="imageupload1" runat="server"/>
                            <asp:HiddenField ID="hdGuid" runat="server" />
                            <span id="fuCateg" class="fuCategoryImageError error-code"></span>
                            <p class="badge bg-light-blue">Best fit icon size is 100 x 100 pixels.</p>
                            <asp:Image ID="imgCategory" runat="server" style="height:35px; width:35px;" Visible="false"/>
                        </div>
                         <div class="form-group">
                            <asp:Label ID="Label4" runat="server" Text="Banner Image"></asp:Label>
                            <asp:FileUpload ID="fuBannerImage" CssClass="imageupload2" runat="server"/>
                            <asp:HiddenField ID="hdGuid1" runat="server" />
                            <span id="fuBanner" class="fuBannerImageError error-code"></span>
                            <p class="badge bg-light-blue">Best fit banner size is 1366 x 300 pixels.</p>
                            <asp:Image ID="imgBanner" runat="server" style="height:35px; width:35px;" Visible="false"/>
                        </div>
                        <div class="checkbox form-group">
                            <asp:CheckBox ID="chkIsActive" runat="server" Checked="true" CssClass="checkbox" Text="IsActive" />
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <asp:Button ID="btnSubmit" runat="server" class="btn btn-primary savebtn" Text="Submit" OnClick="btnSubmit_Click"/>
                        <asp:Button ID="btnClear" runat="server" class="btn btn-default" Text="Clear" OnClick="btnClear_Click"/>
                         <asp:Button ID="btnUpdateCancel" runat="server" class="btn btn-default" Text="Cancel Update" Visible="false" OnClick="btnUpdateCancel_Click"/>
                    </div>
                </div>
                <!-- /.box -->

            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">View Category</h3>                  
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <asp:GridView ID="gvViewCategory" runat="server" class="table table-hover" AutoGenerateColumns="false" GridLines="None"
                    OnRowCommand="gvViewCategory_RowCommand" OnRowDataBound="gvViewCategory_RowDataBound"
                    OnRowEditing="gvViewCategory_RowEditing"
                    OnRowDeleting="gvViewCategory_RowDeleting">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <div class="col-md-12">
                                        <div class="col-md-2 col-xs-2">
                                            <asp:Label ID="Label9" runat="server" Text="Image" />
                                        </div>
                                        <div class="col-md-2 col-xs-2">
                                            <asp:Label ID="Label10" runat="server" Text="Banner Image" />
                                        </div>
                                        <div class="col-md-2 col-xs-2">
                                            <asp:Label ID="Label5" runat="server" Text="Category Name" />
                                        </div>
                                           <div class="col-md-3 col-xs-3">
                                            <asp:Label ID="Label2" runat="server" Text="Description" />
                                        </div>
                                        <div class="col-md-1 col-xs-1">
                                            <asp:Label ID="Label6" runat="server" Text="Active" />
                                        </div>
                                        <div class="col-md-1 col-xs-1">
                                            <asp:Label ID="Label7" runat="server" Text="Edit" />
                                        </div>
                                        <div class="col-md-1 col-xs-1">
                                            <asp:Label ID="Label8" runat="server" Text="Delete" />
                                        </div>
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="col-md-12">
                                        <div class="col-md-2 col-xs-2">
                                            <asp:Label ID="lblID" runat="server" Text='<%#Eval("ID") %>' Visible="false"/>
                                            <asp:Image ID="imgCategory1" runat="server" style="height:35px; width:35px;" ImageUrl='<%#string.Format(ConfigurationManager.AppSettings["apiurl"]+"api/Image/{0}",Eval("ImageID")) %>' />
                                        </div>
                                        <div class="col-md-2 col-xs-2">
                                            <asp:Label ID="lblImgB" runat="server" Visible="false" Text='<%#Eval("BannerImageID") %>'></asp:Label>
                                            <asp:Image ID="imgBanner1" runat="server" style="height:35px; width:35px;" ImageUrl='<%#string.Format(ConfigurationManager.AppSettings["apiurl"]+"api/Image/{0}",Eval("BannerImageID")) %>' />
                                        </div>
                                        <div class="col-md-2 col-xs-2">
                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>' />
                                        </div>
                                          <div class="col-md-3 col-xs-3">
                                            <asp:Label ID="lblDesc" runat="server"  CssClass="text1" Text='<%#Eval("Description") %>' />
                                        </div>
                                        <div class="col-md-1 col-xs-1">
                                            <span><asp:Label ID="lblIsActive" runat="server" class="badge bg-green" Text='<%#Eval("IsActive") %>'/></span>
                                        </div>
                                        <div class="col-md-1 col-xs-1">
                                            <asp:ImageButton ID="imgEdit" runat="server" class="img-responsive sm-icon" CommandName="Edit"
                                                CommandArgument='<%#Eval("ID") %>' ImageUrl="~/Icons/edit_ic.png" data-toggle="tooltip"
                                                data-placement="top" title="Edit Record"/>
                                        </div>
                                        <div class="col-md-1 col-xs-1">
                                            <asp:ImageButton ID="imgDelete" runat="server" class="img-responsive sm-icon" OnClientClick="javascript:return ConfirmDelete();" CommandName="Delete"
                                                CommandArgument='<%#Eval("ID") %>' ImageUrl="~/Icons/delete_ic.png" data-toggle="tooltip"
                                                data-placement="top" title="Delete Record"/>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                        <ul style="text-align: center;">
                            <li class="list-group-item">No Records Found</li>
                        </ul>
                    </EmptyDataTemplate>
                    </asp:GridView>                  
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>
    </section>
</asp:Content>
