﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMasterPage.Master" AutoEventWireup="true" EnableEventValidation="false"
    CodeBehind="AddSubCategory.aspx.cs" Inherits="ApolloWebsite.Categories.AddSubCategory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="http://code.jquery.com/jquery-1.9.0.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>
     <style>
        .text1 {
            word-wrap: break-word;
        }
    </style>
    <script>
        function reset()
        {
            $(".subCategNameError").text("");
            $(".ddlerror").text("");
            $(".fuSubCategoryImageError").text("");
        }
        $(document).ready(function () {

            $(".subCategDesc").keyup(function () {
                var subCategDesc = $(".subCategDesc").val();               
                if (subCategDesc.length > 100)   //subCategDesc maximum length is 200 characters
                {
                    $(".subCategDescError").text("maximum 100 characters");
                    return false; 
                }
                else {
                    $(".subCategDescError").text("");
                }
            });

            $(".subCategName").keyup(function () {
                var subCategName = $(".subCategName").val();
                if (subCategName == "") {
                    $(".subCategNameError").text("Please Enter SubCategory Name.");
                    return false;
                }
                if (subCategName.length < 3)    //subCategName minimum length is 3 characters
                {
                    $(".subCategNameError").text("minimum 3 characters");
                    return false;
                }
                if (subCategName.length > 50)   //subCategName maximum length is 50 characters
                {
                    $(".subCategNameError").text("maximum 50 characters");
                    return false;
                }
                else {
                    $(".subCategNameError").text("");
                }
            });
            $(".ddl").change(function () {
                var ddl = $(".ddl").val();
                if (ddl == "Select") {
                    $(".ddlerror").text("Please Select Category");
                    return false;
                }
                else {
                    $(".ddlerror").text("");
                }
            });
            $(".imageupload1").change(function () {
                var btnText = $(".savebtn").val();
                var imageupload1 = $(".imageupload1").val();
                var pngimg = imageupload1.substring(imageupload1.lastIndexOf(".") + 1, imageupload1.length);
                
                if (btnText == "Submit") {
                    if (imageupload1 == "") {
                        $(".fuSubCategoryImageError").text("Please Choose Image");
                        return false;
                    }
                    if (pngimg.toUpperCase() != "PNG") {
                        $(".fuSubCategoryImageError").text("Please Choose PNG Format Only");
                        return false;
                    }
                }
                if (btnText == "Update") {
                    if (imageupload1 != "") {
                        if (pngimg.toUpperCase() != "PNG") {
                            $(".fuSubCategoryImageError").text("Please Choose PNG Format Only");
                            return false;
                        }
                        else {
                            $(".fuSubCategoryImageError").text("");
                        }
                    }
                }
                else {
                    $(".fuSubCategoryImageError").text("");
                }
            });
            $(".imageupload1").blur(function () {
                var imageupload1 = $(".imageupload1").val();
                var btnText = $(".savebtn").val();
                if (btnText == "Update") {
                    if (imageupload1 == "") {
                        $(".fuSubCategoryImageError").text("");
                    }
                }
            });
            $(".savebtn").click(function () {
                var count = 0;
                debugger;
                var btnText = $(".savebtn").val();
                var subCategName = $(".subCategName").val();
                var imageupload1 = $(".imageupload1").val();
                var pngimg = imageupload1.substring(imageupload1.lastIndexOf(".") + 1, imageupload1.length);
                var ddl = $(".ddl").val();
                var subCategDesc = $(".subCategDesc").val();
                if (subCategDesc.length > 100)   //subCategDesc maximum length is 200 characters
                {
                    $(".subCategDescError").text("maximum 100 characters");
                    count = count + 1;
                }

                if (subCategName.length < 3)    //subCategName minimum length is 3 characters
                {
                    $(".subCategNameError").text("minimum 3 characters");
                    count = count + 1;
                }
                if (subCategName.length > 50)   //subCategName maximum length is 50 characters
                {
                    $(".subCategNameError").text("maximum 50 characters");
                    count = count + 1;
                }
                if (subCategName == "") {
                    $(".subCategNameError").text("Please Enter SubCategory Name.");
                    count = count + 1;
                }
                if (btnText == "Submit") {
                    if (pngimg.toUpperCase() != "PNG") {
                        $(".fuSubCategoryImageError").text("Please Choose PNG Format Only");
                        count = count + 1;
                    }
                    if (imageupload1 == "") {
                        $(".fuSubCategoryImageError").text("Please Choose Image");
                        count = count + 1;
                    }
                }
                else if (btnText == "Update") {
                    if (imageupload1 != "") {
                        if ($('#fuCateg').text() == "Please Choose PNG Format Only") {
                            count = count + 1;
                        }
                    }
                }
                if (ddl == "Select") {
                    $(".ddlerror").text("Please Select Category");
                    count = count + 1;
                }
                if (count > 0) {
                    return false;
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content-header">
        <h1><asp:Label ID="lblPageHeading" runat="server" Text="SubCategory" /> <small>(Add / View)</small></h1>
        <ol class="breadcrumb">
            <li><a href="<%=Page.ResolveUrl("~/Home.aspx") %>"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a>Categories</a></li>
            <li class="active"><a href="javascript:void(0);">SubCategory</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-4">
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <asp:Label ID="Label4" runat="server" Text="Add Sub-Category" /></h3>
                    </div>
                    <!-- form start -->
                <%--     <asp:UpdatePanel ID="upd2" runat="server" UpdateMode="Always">
                            <ContentTemplate>--%>
                    <div class="box-body body-min">
                       
                                 <div class="form-group">
                            <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="Label1" runat="server" Text="Category"></asp:Label>
                            <asp:DropDownList ID="ddlCategory" runat="server" class="form-control ddl" DataTextField="Name" DataValueField="ID" AppendDataBoundItems="true">
                                <asp:ListItem>Select</asp:ListItem>
                            </asp:DropDownList>
                            <span id="ddlErr" runat="server" class="ddlerror error-code"></span>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label2" runat="server" Text="SubCateory Name"></asp:Label>
                            <asp:TextBox ID="txtSubCategoryName" runat="server"  class="form-control subCategName" placeholder="Enter SubCategoryName" />
                            <span id="subcategError" runat="server" class="subCategNameError error-code"></span>
                        </div>
                         <div class="form-group">
                            <asp:Label ID="lblDesc" runat="server" Text="Description"></asp:Label>
                            <asp:TextBox ID="txtSubCategDesc" runat="server"  class="form-control subCategDesc" placeholder="Enter Description"/>
                             <span class="subCategDescError error-code"></span>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label3" runat="server" Text="Image"></asp:Label>
                            <asp:FileUpload ID="fuSubCategoryImage" runat="server" CssClass="imageupload1"/>
                            <asp:HiddenField ID="hdGuid" runat="server" />
                            <span id="fuCateg" runat="server" class="fuSubCategoryImageError error-code"></span>
                            <p class="badge bg-light-blue">Best fit icon size is 100 x 100 pixels.</p>
                            <asp:Image ID="imgCategory" runat="server" style="height:35px; width:35px;" Visible="false"/>
                        </div>
                        <div class="checkbox form-group">
                            <asp:CheckBox ID="chkIsActive" runat="server" Checked="true" CssClass="checkbox" Text="IsActive" />
                        </div>
                       
                    </div>
                    <!-- /.box-body -->
                            <div class="box-footer">
                                <asp:Button ID="btnSubmit" runat="server" class="btn btn-primary savebtn" Text="Submit"  OnClick="btnSubmit_Click" />
                                <asp:Button ID="btnClear" runat="server" class="btn btn-default" OnClientClick="javascript:return reset();" Text="Clear" OnClick="btnClear_Click" />
                            <asp:Button ID="btnUpdateCancel" runat="server" class="btn btn-default" Text="Cancel Update" OnClientClick="javascript:return reset();"  Visible="false" OnClick="btnUpdateCancel_Click"/>
                            </div>
                    <%-- </ContentTemplate>
                         <Triggers>
                             <asp:PostBackTrigger ControlID="btnSubmit"/>
                             <asp:PostBackTrigger ControlID="btnClear"/>
                             <asp:PostBackTrigger ControlID="btnUpdateCancel"/>
                         </Triggers>
                        </asp:UpdatePanel>--%>
                </div>
                <!-- /.box -->
            </div>
            <div class="col-xs-8">
              <div class="box">
                  <div class="box-header with-border">
                        <h3 class="box-title">
                            <asp:Label ID="Label10" runat="server" Text="View SubCategory" /></h3>
                    </div>
                <div class="box-body body-min-1">
                 <%--   <asp:UpdatePanel ID="upd1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>--%>
                            <div class="form-group">
                                <asp:Label ID="Label11" runat="server" Text="Category"></asp:Label>
                                <asp:DropDownList ID="ddlViewCategory" runat="server" class="form-control" DataTextField="Name"
                                    DataValueField="ID" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlViewCategory_SelectedIndexChanged">
                                    <asp:ListItem>Select</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group table-responsive no-padding">
                                <asp:GridView ID="gvViewSubCategory" runat="server" class="table table-hover" AutoGenerateColumns="false" GridLines="None"
                                    OnRowCommand="gvViewSubCategory_RowCommand" OnRowDataBound="gvViewSubCategory_RowDataBound"
                                    OnRowEditing="gvViewSubCategory_RowEditing"
                                    OnRowDeleting="gvViewSubCategory_RowDeleting">
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <div class="col-md-12">
                                                    <div class="col-md-3 col-xs-3">
                                                        <asp:Label ID="Label9" runat="server" Text="Image" />
                                                    </div>
                                                    <div class="col-md-3 col-xs-3">
                                                        <asp:Label ID="Label5" runat="server" Text="SubCategory Name" />
                                                    </div>
                                                    <div class="col-md-3 col-xs-3">
                                                        <asp:Label ID="Label2" runat="server" Text="Description" />
                                                    </div>
                                                    <div class="col-md-1 col-xs-1">
                                                        <asp:Label ID="Label6" runat="server" Text="Active" />
                                                    </div>
                                                    <div class="col-md-1 col-xs-1">
                                                        <asp:Label ID="Label7" runat="server" Text="Edit" />
                                                    </div>
                                                    <div class="col-md-1 col-xs-1">
                                                        <asp:Label ID="Label8" runat="server" Text="Delete" />
                                                    </div>
                                                </div>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <div class="col-md-12 col-xs-12">
                                                    <div class="col-md-3 col-xs-3">
                                                        <asp:Label ID="lblID" runat="server" Text='<%#Eval("ID") %>' Visible="false" />
                                                        <asp:Image ID="imgCategory" runat="server" Style="height: 35px; width: 35px;" ImageUrl='<%#string.Format(ConfigurationManager.AppSettings["apiurl"]+"api/Image/{0}",Eval("ImageID")) %>' />
                                                    </div>
                                                    <div class="col-md-3 col-xs-3">
                                                        <asp:Label ID="lblName" runat="server" CssClass="text1" Text='<%#Eval("Name") %>' />
                                                    </div>
                                                     <div class="col-md-3 col-xs-3">
                                                        <asp:Label ID="lblDesc" runat="server" CssClass="text1" Text='<%#Eval("Description") %>' />
                                                    </div>
                                                    <div class="col-md-1 col-xs-1">
                                                        <span>
                                                            <asp:Label ID="lblIsActive" runat="server" class="badge bg-green" Text='<%#Eval("IsActive") %>' /></span>
                                                    </div>
                                                    <div class="col-md-1 col-xs-1">
                                                        <asp:ImageButton ID="imgEdit" runat="server" class="img-responsive sm-icon" CommandName="Edit"
                                                            CommandArgument='<%#Eval("ID") %>' ImageUrl="~/Icons/edit_ic.png" data-toggle="tooltip"
                                                            data-placement="top" title="Edit Record" />
                                                    </div>
                                                    <div class="col-md-1 col-xs-1">
                                                        <asp:ImageButton ID="imgDelete" runat="server" class="img-responsive sm-icon" OnClientClick="javascript:return ConfirmDelete();"
                                                            CommandName="Delete"
                                                            CommandArgument='<%#Eval("ID") %>' ImageUrl="~/Icons/delete_ic.png" data-toggle="tooltip"
                                                            data-placement="top" title="Delete Record" />
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <ul style="text-align: center;">
                                            <li class="list-group-item">No Records Found</li>
                                        </ul>
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                       <%-- </ContentTemplate>
                    </asp:UpdatePanel>--%>
                </div>
                <!-- /.box-body -->
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</asp:Content>
