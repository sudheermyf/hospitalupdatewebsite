﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMasterPage.Master" AutoEventWireup="true" CodeBehind="AddDoctors.aspx.cs" Inherits="ApolloWebsite.Team.AddDoctors" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="http://code.jquery.com/jquery-1.9.0.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>
    <style>
        .text1 {
            word-wrap: break-word;
        }
    </style>
    <script>
        function reset() {
            $(".nameError").text("");
            $(".subCategNameError").text("");
            $(".ddlerror").text("");
            $(".fuDoctorImageError").text("");
            $(".noyError").text("");
            $(".operError").text("");
        }
        $(document).ready(function () {
            debugger;
            $(".subCategDesc").keyup(function () {
                var subCategDesc = $(".subCategDesc").val();
                if (subCategDesc.length > 200)   //subCategDesc maximum length is 200 characters
                {
                    $(".subCategDescError").text("maximum 200 characters");
                    return false;
                }
                else {
                    $(".subCategDescError").text("");
                }
            });

            $(".name").keyup(function () {
                var name = $(".name").val();
                if (name == "") {
                    $(".nameError").text("Please Enter Doctor Name.");
                    return false;
                }
                if (name.length < 3)    //subCategName minimum length is 3 characters
                {
                    $(".nameError").text("minimum 3 characters");
                    return false;
                }
                if (name.length > 50)   //subCategName maximum length is 50 characters
                {
                    $(".nameError").text("maximum 50 characters");
                    return false;
                }
                else {
                    $(".nameError").text("");
                }
            });
            $(".noy").keyup(function () {
                var noy = $(".noy").val();
                var expression = /^[0-9\b]+$/;
                var result = expression.test(noy);

                if (noy.length == 0) {
                    $(".noyError").text("");
                }
                else if (!result) {
                    $(".noyError").text("Please Enter Number of Years.");
                    return false;
                }
                else {
                    $(".noyError").text("");
                }
            });
            $(".oper").keyup(function () {
                var oper = $(".oper").val();
                var expression = /^[0-9\b]+$/;
                var result = expression.test(oper);

                if (oper.length == 0) {
                    $(".operError").text("");
                }
                else if (!result) {
                    $(".operError").text("Please Enter Number of Years.");
                    return false;
                }
                else {
                    $(".operError").text("");
                }
            });

            $(".ddl").change(function () {
                var ddl = $(".ddl").val();
                if (ddl == "Select") {
                    $(".ddlerror").text("Please Select Department");
                    return false;
                }
                else {
                    $(".ddlerror").text("");
                }
            });
            $(".imageupload1").change(function () {
                var btnText = $(".savebtn").val();
                var imageupload1 = $(".imageupload1").val();
                var pngimg = imageupload1.substring(imageupload1.lastIndexOf(".") + 1, imageupload1.length);

                if (btnText == "Submit") {
                    if (imageupload1 == "") {
                        $(".fuDoctorImageError").text("Please Choose Image");
                        return false;
                    }
                    if (pngimg.toUpperCase() != "PNG") {
                        $(".fuDoctorImageError").text("Please Choose PNG Format Only");
                        return false;
                    }
                }
                if (btnText == "Update") {
                    if (imageupload1 != "") {
                        if (pngimg.toUpperCase() != "PNG") {
                            $(".fuDoctorImageError").text("Please Choose PNG Format Only");
                            return false;
                        }
                        else {
                            $(".fuDoctorImageError").text("");
                        }
                    }
                }
                else {
                    $(".fuDoctorImageError").text("");
                }
            });
            $(".imageupload1").blur(function () {
                var imageupload1 = $(".imageupload1").val();
                var btnText = $(".savebtn").val();
                if (btnText == "Update") {
                    if (imageupload1 == "") {
                        $(".fuDoctorImageError").text("");
                    }
                }
            });
            $(".savebtn").click(function () {
                var count = 0;
                var btnText = $(".savebtn").val();
                var name = $(".name").val();
                var imageupload1 = $(".imageupload1").val();
                var pngimg = imageupload1.substring(imageupload1.lastIndexOf(".") + 1, imageupload1.length);
                var ddl = $(".ddl").val();
                if (name.length < 3)    //subCategName minimum length is 3 characters
                {
                    $(".nameError").text("minimum 3 characters");
                    count = count + 1;
                }
                if (name.length > 50)   //subCategName maximum length is 50 characters
                {
                    $(".nameError").text("maximum 50 characters");
                    count = count + 1;
                }
                if (name == "") {
                    $(".nameError").text("Please Enter Doctor Name.");
                    count = count + 1;
                }
                if (btnText == "Submit") {
                    if (pngimg.toUpperCase() != "PNG") {
                        $(".fuDoctorImageError").text("Please Choose PNG Format Only");
                        count = count + 1;
                    }
                    if (imageupload1 == "") {
                        $(".fuDoctorImageError").text("Please Choose Image");
                        count = count + 1;
                    }
                }
                else if (btnText == "Update") {
                    if (imageupload1 != "") {
                        if ($('#fuDoctor').text() == "Please Choose PNG Format Only") {
                            count = count + 1;
                        }
                    }
                }
                if (ddl == "Select") {
                    $(".ddlerror").text("Please Select Department");
                    count = count + 1;
                }
                if (count > 0) {
                    return false;
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content-header">
        <h1>
            <asp:Label ID="lblPageHeading" runat="server" Text="Doctors" />
            <small>(Add / View)</small></h1>
        <ol class="breadcrumb">
            <li><a href="<%=Page.ResolveUrl("~/Home.aspx") %>"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a>Our Team</a></li>
            <li class="active"><a href="javascript:void(0);">Doctors</a></li>
        </ol>
    </section>
    <section class="content">
        <asp:UpdatePanel ID="updModal" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="modal fade" id="myModalDoctor" tabindex="-1" role="dialog"  data-backdrop="static" aria-labelledby="myModalLabel1" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">

                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title">
                                    <asp:Label ID="lblhead" runat="server" Text="Doctor Details"></asp:Label></h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="row form-group" style="text-align: center;">
                                            <div class="col-md-12 col-sm-12">
                                                <asp:Image ID="Image1" runat="server" AlternateText="~/Icons/test1.png" CssClass="img-thumbnail" Height="150px" Width="150px" />
                                                <asp:HiddenField ID="HiddenField1" runat="server" />
                                            </div>
                                        </div>
                                        <div class="row form-group" style="text-align: center;">
                                            <div class="col-md-12 col-sm-12">
                                                <asp:Label ID="lblDocName" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                        <hr />
                                        <ul class="list-group">
                                            <li class="list-group-item" id="lidept" runat="server">
                                                <div class="row">
                                                    <div class="col-md-6 col-xs-6">
                                                        <asp:Label ID="lblSNO" runat="server" Text="SNO" Visible="false"></asp:Label>
                                                        <asp:Label ID="lblDept" runat="server" Text="Department"></asp:Label>
                                                    </div>
                                                    <div class="col-md-6 col-xs-6">
                                                        <asp:Label ID="lblDepartment" runat="server"></asp:Label>
                                                        <span class="ddlerror error-code"></span>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="list-group-item" id="lispec" runat="server">
                                                <div class="row">
                                                    <div class="col-md-6 col-xs-6">
                                                        <asp:Label ID="lblSpecialization" runat="server" Text="Specialization"></asp:Label>
                                                    </div>
                                                    <div class="col-md-6 col-xs-6">
                                                        <asp:Label ID="lblSpec1" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="list-group-item" id="liexp" runat="server">
                                                <div class="row">
                                                    <div class="col-md-6 col-xs-6">
                                                        <asp:Label ID="lblexperience" runat="server" Text="No:Of Years"></asp:Label>
                                                    </div>
                                                    <div class="col-md-6 col-xs-6">
                                                        <asp:Label ID="lblexp" runat="server"></asp:Label>
                                                    </div>
                                                </div>

                                            </li>
                                            <li class="list-group-item" id="lioperation" runat="server">
                                                <div class="row">
                                                    <div class="col-md-6 col-xs-6">
                                                        <asp:Label ID="lbloperation" runat="server" Text=" Operations Done"></asp:Label>
                                                    </div>
                                                    <div class="col-md-6 col-xs-6">
                                                        <asp:Label ID="txtOperation" runat="server" />
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="list-group-item" id="lidesc" runat="server">
                                                <div class="row">
                                                    <div class="col-md-6 col-xs-6">
                                                        <asp:Label ID="lblDesc" runat="server" Text="Description"></asp:Label>
                                                    </div>
                                                    <div class="col-md-6 col-xs-6">
                                                        <asp:Label ID="txtDesc1" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

        <div class="row">
            <!-- left column -->
            <div class="col-md-4 col-xs-12 col-sm-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- /.box-header -->

                    <!-- form start -->
                    <%--<asp:UpdatePanel ID="upd2" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>--%>
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <asp:Label ID="lblHeads" runat="server" Text="Add Doctor" /></h3>
                    </div>
                    <div class="box-body body-min">

                        <div class="form-group">
                            <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="Label1" runat="server" Text="Department"></asp:Label>
                            <asp:DropDownList ID="ddlDepartment" runat="server" class="form-control ddl" DataTextField="Name" DataValueField="ID" AppendDataBoundItems="true">
                                <asp:ListItem>Select</asp:ListItem>
                            </asp:DropDownList>
                            <span class="ddlerror error-code"></span>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label2" runat="server" Text="Doctor Name"></asp:Label>
                            <asp:TextBox ID="txtDoctorName" runat="server" class="form-control name" placeholder="Enter Doctor Name" />
                            <span id="nameError" runat="server" class="nameError error-code"></span>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="lblSpec" runat="server" Text="Specialization"></asp:Label>
                            <asp:TextBox ID="txtSpecialization" runat="server" class="form-control " placeholder="Enter Specialization" />
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label12" runat="server" Text="No:Of Years"></asp:Label>
                            <asp:TextBox ID="txtNoYears" runat="server" class="form-control noy" placeholder="Enter No: Of Years" />
                            <span class="noyError error-code"></span>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label14" runat="server" Text="Operations Done"></asp:Label>
                            <asp:TextBox ID="txtOperationsDone" runat="server" class="form-control oper" placeholder="Enter Operations Done" />
                            <span class="operError error-code"></span>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label13" runat="server" Text="Description"></asp:Label>
                            <asp:TextBox ID="txtdesc" runat="server" class="form-control subCategDesc" placeholder="Enter Description" />
                            <span class="subCategDescError error-code"></span>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label3" runat="server" Text="Image"></asp:Label>
                            <asp:FileUpload ID="fuDoctorImage" runat="server" CssClass="imageupload1" />
                            <asp:HiddenField ID="hdGuid" runat="server" />
                            <span id="fuDoctor" class="fuDoctorImageError error-code"></span>
                            <p class="badge bg-light-blue">Best fit icon size is 220 x 220 pixels.</p>
                            <asp:Image ID="imgDoctor" runat="server" Style="height: 35px; width: 35px;" Visible="false" />
                        </div>
                        <div class="checkbox form-group">
                            <asp:CheckBox ID="chkIsActive" runat="server" Checked="true" CssClass="checkbox" Text="IsActive" />
                        </div>

                    </div>

                    <!-- /.box-body -->
                    <div class="box-footer">
                        <asp:Button ID="btnSubmit" runat="server" class="btn btn-primary savebtn" Text="Submit" OnClick="btnSubmit_Click" />
                        <asp:Button ID="btnClear" runat="server" class="btn btn-default" OnClientClick="javascript:return reset();" Text="Clear" OnClick="btnClear_Click" />
                        <asp:Button ID="btnUpdateCancel" runat="server" class="btn btn-default" Text="Cancel Update" OnClientClick="javascript:return reset();" Visible="false" OnClick="btnUpdateCancel_Click" />
                    </div>
                    <%--</ContentTemplate>
                          <Triggers>
                              <asp:PostBackTrigger ControlID="btnSubmit" />
                              <asp:PostBackTrigger ControlID="btnClear" />
                              <asp:PostBackTrigger ControlID="btnUpdateCancel" />
                          </Triggers>
                        </asp:UpdatePanel>--%>
                </div>
                <!-- /.box -->
            </div>
            <div class="col-xs-12 col-md-8 col-sm-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <asp:Label ID="Label10" runat="server" Text="View Doctors" /></h3>
                    </div>
                    <div class="box-body body-min-1">
                   <%--     <asp:UpdatePanel ID="upd1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>--%>
                                <div class="form-group">
                                    <asp:Label ID="Label11" runat="server" Text="Department"></asp:Label>
                                    <asp:DropDownList ID="ddlViewDepartments" runat="server" class="form-control" DataTextField="Name"
                                        DataValueField="ID" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlViewDepartments_SelectedIndexChanged">
                                        <asp:ListItem>Select</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group table-responsive no-padding">
                                    <asp:GridView ID="gvViewDoctors" runat="server" class="table table-hover" AutoGenerateColumns="false" GridLines="None"
                                        OnRowCommand="gvViewDoctors_RowCommand" OnRowDataBound="gvViewDoctors_RowDataBound"
                                        OnRowEditing="gvViewDoctors_RowEditing"
                                        OnRowDeleting="gvViewDoctors_RowDeleting">
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <div class="col-md-12">
                                                        <div class="col-md-3 col-xs-3 col-sm-3">
                                                            <asp:Label ID="Label9" runat="server" Text="Image" />
                                                        </div>
                                                        <div class="col-md-3 col-xs-3 col-sm-3">
                                                            <asp:Label ID="Label5" runat="server" Text="Doctor Name" />
                                                        </div>
                                                        <div class="col-md-1 col-xs-1 col-sm-1">
                                                            <asp:Label ID="Label6" runat="server" Text="Active" />
                                                        </div>
                                                        <div class="col-md-1 col-xs-1 col-sm-1">
                                                            <asp:Label ID="Label7" runat="server" Text="Edit" />
                                                        </div>
                                                        <div class="col-md-1 col-xs-1 col-sm-1">
                                                            <asp:Label ID="Label8" runat="server" Text="Delete" />
                                                        </div>
                                                        <div class="col-md-1 col-xs-1 col-sm-1">
                                                            <asp:Label ID="Label15" runat="server" Text="View" />
                                                        </div>
                                                    </div>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <div class="col-md-12 col-xs-12">
                                                        <div class="col-md-3 col-xs-3 col-sm-3">
                                                            <asp:Label ID="lblID" runat="server" Text='<%#Eval("ID") %>' Visible="false" />
                                                            <asp:Image ID="imgCategory" runat="server" Style="height: 35px; width: 35px;" ImageUrl='<%#string.Format(ConfigurationManager.AppSettings["apiurl"]+"api/Image/{0}",Eval("ImageID")) %>' />
                                                        </div>
                                                        <div class="col-md-3 col-xs-3 col-sm-3">
                                                            <asp:Label ID="lblName" runat="server" CssClass="text1" Text='<%#Eval("Name") %>' />
                                                        </div>
                                                        <div class="col-md-1 col-xs-1 col-sm-1">
                                                            <span>
                                                                <asp:Label ID="lblIsActive" runat="server" class="badge bg-green" Text='<%#Eval("IsActive") %>' /></span>
                                                        </div>
                                                        <div class="col-md-1 col-xs-1 col-sm-1">
                                                            <asp:ImageButton ID="imgEdit" runat="server" class="sm-icon" CommandName="Edit"
                                                                CommandArgument='<%#Eval("ID") %>' ImageUrl="~/Icons/edit_ic.png" data-toggle="tooltip"
                                                                data-placement="top" title="Edit Record" />
                                                        </div>
                                                        <div class="col-md-1 col-xs-1 col-sm-1">
                                                            <asp:ImageButton ID="imgDelete" runat="server" class="sm-icon" OnClientClick="javascript:return ConfirmDelete();"
                                                                CommandName="Delete"
                                                                CommandArgument='<%#Eval("ID") %>' ImageUrl="~/Icons/delete_ic.png" data-toggle="tooltip"
                                                                data-placement="top" title="Delete Record" />
                                                        </div>
                                                        <div class="col-md-1 col-xs-1 col-sm-1">
                                                            <asp:ImageButton ID="imgView" runat="server" class="sm-icon"
                                                                CommandName="View"
                                                                CommandArgument='<%#Eval("ID") %>' ImageUrl="~/Icons/view.png" data-toggle="tooltip"
                                                                data-placement="top" title="View Record" />
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <ul style="text-align: center;">
                                                <li class="list-group-item">No Records Found</li>
                                            </ul>
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                           <%-- </ContentTemplate>
                        </asp:UpdatePanel>--%>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
</asp:Content>
