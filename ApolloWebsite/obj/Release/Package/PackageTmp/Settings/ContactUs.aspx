﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMasterPage.Master" AutoEventWireup="true" CodeBehind="ContactUs.aspx.cs" Inherits="ApolloWebsite.Settings.ContactUs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
       <script src="http://code.jquery.com/jquery-1.9.0.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>
     <style>
        .text1 {
            word-wrap: break-word;
        }
    </style>
    <script>
        function reset()
        {
            $(".nameError").text("");
            $(".latError").text("");
            $(".longError").text("");
        }
        $(document).ready(function () {

            $(".name").keyup(function () {
                var name = $(".name").val();
                if (name == "") {
                    $(".nameError").text("Please Enter Contact Name.");
                    return false;
                }
                else {
                    $(".nameError").text("");
                }
            });
            $(".lat").keyup(function () {
                var lat = $(".lat").val();
                if (lat == "") {
                    $(".latError").text("Please Enter Latitude.");
                    return false;
                }
                else {
                    $(".latError").text("");
                }
            });
            $(".long").keyup(function () {
                var long = $(".long").val();
                if (long == "") {
                    $(".longError").text("Please Enter Longitude.");
                    return false;
                }
                else {
                    $(".longError").text("");
                }
            });
           
            $(".savebtn").click(function () {
                var count = 0;
                var name = $(".name").val();
                var lat = $(".lat").val();
                var long = $(".long").val();
                if (name == "") {
                    $(".nameError").text("Please Enter Contact Name.");
                    count = count + 1;
                }
                if (lat == "") {
                    $(".latError").text("Please Enter Latitude.");
                    count = count + 1;
                }
                if (long == "") {
                    $(".longError").text("Please Enter Longitude.");
                    count = count + 1;
                }
                if (count > 0) {
                    return false;
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <section class="content-header">
        <h1><asp:Label ID="lblPageHeading" runat="server" Text="Contact Us" /> <small>(Add / View)</small></h1>
        <ol class="breadcrumb">
            <li><a href="<%=Page.ResolveUrl("~/Home.aspx") %>"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a>Settings</a></li>
            <li class="active"><a href="javascript:void(0);">Contact Us</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12 col-xs-12 col-sm-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- /.box-header -->

                    <!-- form start -->
                    <asp:UpdatePanel ID="upd2" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="box-header with-border">
                                <h3 class="box-title">
                                    <asp:Label ID="lblHead" runat="server" Text="Add Contact Details" /></h3>
                            </div>
                            <div class="box-body body-min">
                                <div class="col-xs-12 col-md-12">
                                    <div class="col-xs-6 col-md-6">
                                        <div class="form-group">
                                            <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="Label1" runat="server" Text="Contact Name"></asp:Label>
                                            <asp:TextBox ID="txtName" runat="server" class="form-control name" placeholder="Enter Name" />
                                            <span class="nameError error-code"></span>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label12" runat="server" Text="City"></asp:Label>
                                            <asp:TextBox ID="txtCity" runat="server" class="form-control" placeholder="Enter City" />
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label16" runat="server" Text="State"></asp:Label>
                                            <asp:TextBox ID="txtState" runat="server" class="form-control" placeholder="Enter State" />
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label17" runat="server" Text="Country"></asp:Label>
                                            <asp:TextBox ID="txtCountry" runat="server" class="form-control" placeholder="Enter Country" />
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label14" runat="server" Text="ZipCode"></asp:Label>
                                            <asp:TextBox ID="txtZipCode" runat="server" class="form-control" placeholder="Enter ZipCode" />
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label23" runat="server" Text="Latitude"></asp:Label>
                                            <asp:TextBox ID="txtLatitude" runat="server" class="form-control lat groupOfTexbox" placeholder="Enter Latitude" />
                                             <span class="latError error-code"></span>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label13" runat="server" Text="Longitude"></asp:Label>
                                            <asp:TextBox ID="txtLogitude" runat="server" class="form-control long groupOfTexbox" placeholder="Enter Logitude" />
                                             <span class="longError error-code"></span>
                                        </div>
                                        <div class="form-group">
                                            <asp:CheckBox ID="chkIsActive" runat="server" Checked="true" CssClass="checkbox" Text="IsActive" />
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6">
                                        <div class="form-group">
                                            <asp:Label ID="Label2" runat="server" Text="Address1"></asp:Label>
                                            <asp:TextBox ID="txtAddress1" runat="server" class="form-control" placeholder="Enter Address1" />
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="lblSpec" runat="server" Text="Address2"></asp:Label>
                                            <asp:TextBox ID="txtAddress2" runat="server" class="form-control " placeholder="Enter Address2" />
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label22" runat="server" Text="Email"></asp:Label>
                                            <asp:TextBox ID="txtEmail" runat="server" class="form-control" placeholder="Enter Email" />
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label20" runat="server" Text="Call Center No"></asp:Label>
                                            <asp:TextBox ID="txtMobile" runat="server" class="form-control" placeholder="Enter CallCenter No" />
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label18" runat="server" Text="Telephone No"></asp:Label>
                                            <asp:TextBox ID="txtTelephone" runat="server" class="form-control" placeholder="Enter Telephone No" />
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label19" runat="server" Text="Toll Free No"></asp:Label>
                                            <asp:TextBox ID="txttollFree" runat="server" class="form-control oper" placeholder="Enter Toll Free No" />
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label21" runat="server" Text="Fax"></asp:Label>
                                            <asp:TextBox ID="txtFax" runat="server" class="form-control" placeholder="Enter Fax" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- /.box-body -->
                            <div class="box-footer">
                                <asp:Button ID="btnSubmit" runat="server" class="btn btn-primary savebtn" Text="Submit" OnClick="btnSubmit_Click" />
                                <asp:Button ID="btnClear" runat="server" class="btn btn-default" OnClientClick="javascript:return reset();" Text="Clear" OnClick="btnClear_Click" />
                                <asp:Button ID="btnUpdateCancel" runat="server" class="btn btn-default" Text="Cancel Update" OnClientClick="javascript:return reset();" Visible="false" OnClick="btnUpdateCancel_Click" />
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnSubmit" />
                            <asp:PostBackTrigger ControlID="btnUpdateCancel" />
                            <asp:PostBackTrigger ControlID="btnClear" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <!-- /.box -->
            </div>
            <div class="col-xs-12 col-md-12 col-sm-12">
              <div class="box">
                  <div class="box-header with-border">
                        <h3 class="box-title">
                            <asp:Label ID="Label10" runat="server" Text="View Contact Details" /></h3>
                    </div>
                <div class="box-body body-min-1">
                    <asp:UpdatePanel ID="upd1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="form-group table-responsive no-padding">
                                <asp:GridView ID="gvViewContact" runat="server" class="table table-hover" AutoGenerateColumns="false" GridLines="None"
                                    OnRowCommand="gvViewContact_RowCommand" 
                                    OnRowEditing="gvViewContact_RowEditing"
                                    OnRowDeleting="gvViewContact_RowDeleting">
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <div class="col-md-12">
                                                    <div class="col-md-6 col-xs-6 col-sm-6">
                                                        <asp:Label ID="Label5" runat="server" Text="Contact Name" />
                                                    </div>
                                                    <div class="col-md-2 col-xs-2 col-sm-2">
                                                        <asp:Label ID="Label3" runat="server" Text="IsActive" />
                                                    </div>
                                                    <div class="col-md-2 col-xs-2 col-sm-2">
                                                        <asp:Label ID="Label7" runat="server" Text="Edit" />
                                                    </div>
                                                    <div class="col-md-2 col-xs-2 col-sm-2">
                                                        <asp:Label ID="Label8" runat="server" Text="Delete" />
                                                    </div>
                                                </div>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <div class="col-md-12 col-xs-12">
                                                    <div class="col-md-6 col-xs-6 col-sm-6">
                                                        <asp:Label ID="lblName" runat="server" CssClass="text1" Text='<%#Eval("Name") %>' />
                                                    </div>
                                                      <div class="col-md-2 col-xs-2 col-sm-2">
                                                             <span><asp:Label ID="lblIsActive" runat="server" class="badge bg-green" Text='<%#Eval("IsActive") %>'/></span>
                                                   </div>
                                                    <div class="col-md-2 col-xs-2 col-sm-2">
                                                        <asp:ImageButton ID="imgEdit" runat="server" class="sm-icon" CommandName="Edit"
                                                            CommandArgument='<%#Eval("ID") %>' ImageUrl="~/Icons/edit_ic.png" data-toggle="tooltip"
                                                            data-placement="top" title="Edit Record" />
                                                    </div>
                                                    <div class="col-md-2 col-xs-2 col-sm-2">
                                                        <asp:ImageButton ID="imgDelete" runat="server" class="sm-icon" OnClientClick="javascript:return ConfirmDelete();"
                                                            CommandName="Delete"
                                                            CommandArgument='<%#Eval("ID") %>' ImageUrl="~/Icons/delete_ic.png" data-toggle="tooltip"
                                                            data-placement="top" title="Delete Record" />
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <ul style="text-align: center;">
                                            <li class="list-group-item">No Records Found</li>
                                        </ul>
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <!-- /.box-body -->
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</asp:Content>
