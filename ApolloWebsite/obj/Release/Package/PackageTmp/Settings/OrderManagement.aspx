﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMasterPage.Master" AutoEventWireup="true" CodeBehind="OrderManagement.aspx.cs" Inherits="ApolloWebsite.Settings.OrderManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">    
    <style>
        select option {
            padding: 5px 3px;
            border-bottom: 1px solid #EEE;
        }

        .info-box-content {
            margin-left: 70px;
        }
        .info-box-text-1{word-wrap: break-word; white-space:normal;}
        .highlight-div {
            -webkit-box-shadow: 0px 0px 0px 2px rgba(245,0,0,0.5);
            -moz-box-shadow: 0px 0px 0px 2px rgba(245,0,0,0.5);
            box-shadow: 0px 0px 0px 2px rgba(245,0,0,0.5);
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content-header">
        <h1>Screen Order</h1>
        <ol class="breadcrumb">
            <li><a href="<%=Page.ResolveUrl("~/Home.aspx") %>"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a>Settings</a></li>
            <li class="active"><a href="javascript:void(0);">Screen Order</a></li>
        </ol>
    </section>

    <section class="content">
        <asp:UpdatePanel ID="updatePanel" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-3">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            <!-- /.box-header -->
                            <!-- form start -->
                            <div class="box-body">
                                <asp:ListBox ID="lstbCategory" runat="server" CssClass="form-control"
                                    Style="min-height: 400px;" AutoPostBack="true"
                                    OnSelectedIndexChanged="lstbCategory_SelectedIndexChanged"
                                    DataTextField="Name" DataValueField="SNo"></asp:ListBox>
                                <asp:Label ID="lblListMessage" runat="server" CssClass="label label-danger" />
                            </div>
                            <div class="box-footer">
                                <asp:Button ID="btnClear" runat="server" CssClass="btn btn-info btn-block" Text="Clear Order" OnClick="btnClear_Click" Enabled="false" />
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <div class="col-md-9">
                        <!-- general form elements -->
                        <div class="box box-info">
                            <!-- /.box-header -->
                            <!-- form start -->
                            <div class="box-body">
                                <table class="table">
                                    <tr>
                                        <td>
                                            <div id="div11" runat="server" class="info-box" style="min-height: 65px;">
                                                <span class="info-box-icon bg-aqua" style="height: 65px; width: 65px; line-height: 65px;">
                                                    <asp:LinkButton ID="lnkCol1Ord1" runat="server" data-toggle="tooltip"
                                                        data-placement="top" title="Save Order" OnClick="lnkColumnOrder_Click">
                                            <i class="fa fa-save" style="color:#FFF;"></i></asp:LinkButton>
                                                </span>
                                                <div class="info-box-content">
                                                    <span class="info-box-text">
                                                        <asp:Label ID="lblCategoryName11" runat="server" CssClass="info-box-text-1" Text='<%#Eval("Name") %>' /></span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div id="div21" runat="server" class="info-box" style="min-height: 65px;">
                                                <span class="info-box-icon bg-aqua" style="height: 65px; width: 65px; line-height: 65px;">
                                                    <asp:LinkButton ID="lnkCol2Ord1" runat="server" data-toggle="tooltip"
                                                        data-placement="top" title="Save Order" OnClick="lnkColumnOrder_Click">
                                            <i class="fa fa-save" style="color:#FFF;"></i></asp:LinkButton>
                                                </span>
                                                <div class="info-box-content">
                                                    <span class="info-box-text">
                                                        <asp:Label ID="lblCategoryName21" runat="server" CssClass="info-box-text-1" Text='<%#Eval("Name") %>' /></span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div id="div31" runat="server" class="info-box" style="min-height: 65px;">
                                                <span class="info-box-icon bg-aqua" style="height: 65px; width: 65px; line-height: 65px;">
                                                    <asp:LinkButton ID="lnkCol3Ord1" runat="server" data-toggle="tooltip"
                                                        data-placement="top" title="Save Order" OnClick="lnkColumnOrder_Click">
                                            <i class="fa fa-save" style="color:#FFF;"></i></asp:LinkButton>
                                                </span>
                                                <div class="info-box-content">
                                                    <span class="info-box-text">
                                                        <asp:Label ID="lblCategoryName31" runat="server" CssClass="info-box-text-1" Text='<%#Eval("Name") %>' /></span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div id="div41" runat="server" class="info-box" style="min-height: 65px;">
                                                <span class="info-box-icon bg-aqua" style="height: 65px; width: 65px; line-height: 65px;">
                                                    <asp:LinkButton ID="lnkCol4Ord1" runat="server" data-toggle="tooltip"
                                                        data-placement="top" title="Save Order" OnClick="lnkColumnOrder_Click">
                                            <i class="fa fa-save" style="color:#FFF;"></i></asp:LinkButton>
                                                </span>
                                                <div class="info-box-content">
                                                    <span class="info-box-text">
                                                        <asp:Label ID="lblCategoryName41" runat="server" CssClass="info-box-text-1" Text='<%#Eval("Name") %>' /></span>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div id="div12" runat="server" class="info-box" style="min-height: 65px;">
                                                <span class="info-box-icon bg-aqua" style="height: 65px; width: 65px; line-height: 65px;">
                                                    <asp:LinkButton ID="lnkCol1Ord2" runat="server" data-toggle="tooltip"
                                                        data-placement="top" title="Save Order" OnClick="lnkColumnOrder_Click">
                                            <i class="fa fa-save" style="color:#FFF;"></i></asp:LinkButton>
                                                </span>
                                                <div class="info-box-content">
                                                    <span class="info-box-text">
                                                        <asp:Label ID="lblCategoryName12" runat="server" CssClass="info-box-text-1" Text='<%#Eval("Name") %>' /></span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div id="div22" runat="server" class="info-box" style="min-height: 65px;">
                                                <span class="info-box-icon bg-aqua" style="height: 65px; width: 65px; line-height: 65px;">
                                                    <asp:LinkButton ID="lnkCol2Ord2" runat="server" data-toggle="tooltip"
                                                        data-placement="top" title="Save Order" OnClick="lnkColumnOrder_Click">
                                            <i class="fa fa-save" style="color:#FFF;"></i></asp:LinkButton>
                                                </span>
                                                <div class="info-box-content">
                                                    <span class="info-box-text">
                                                        <asp:Label ID="lblCategoryName22" runat="server" CssClass="info-box-text-1" Text='<%#Eval("Name") %>' /></span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div id="div32" runat="server" class="info-box" style="min-height: 65px;">
                                                <span class="info-box-icon bg-aqua" style="height: 65px; width: 65px; line-height: 65px;">
                                                    <asp:LinkButton ID="lnkCol3Ord2" runat="server" data-toggle="tooltip"
                                                        data-placement="top" title="Save Order" OnClick="lnkColumnOrder_Click">
                                            <i class="fa fa-save" style="color:#FFF;"></i></asp:LinkButton>
                                                </span>
                                                <div class="info-box-content">
                                                    <span class="info-box-text">
                                                        <asp:Label ID="lblCategoryName32" runat="server" CssClass="info-box-text-1" Text='<%#Eval("Name") %>' /></span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div id="div42" runat="server" class="info-box" style="min-height: 65px;background-color: rgba(204, 204, 204, 0.43);">
                                                <span class="info-box-icon bg-aqua" style="height: 65px; width: 65px; line-height: 65px;">
                                                    <asp:LinkButton ID="lnkCol4Ord2" runat="server" data-toggle="tooltip"
                                                        data-placement="top">
                                            <i class="fa fa-save" style="color:#ccc;"></i></asp:LinkButton>
                                                </span>
                                                <div class="info-box-content">
                                                    <span class="info-box-text">
                                                        <asp:Label ID="lblCategoryName42" runat="server" CssClass="info-box-text-1" Text="FEEDBACK" /></span>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div id="div13" runat="server" class="info-box" style="min-height: 65px;">
                                                <span class="info-box-icon bg-aqua" style="height: 65px; width: 65px; line-height: 65px;">
                                                    <asp:LinkButton ID="lnkCol1Ord3" runat="server" data-toggle="tooltip"
                                                        data-placement="top" title="Save Order" OnClick="lnkColumnOrder_Click">
                                            <i class="fa fa-save" style="color:#FFF;"></i></asp:LinkButton>
                                                </span>
                                                <div class="info-box-content">
                                                    <span class="info-box-text">
                                                        <asp:Label ID="lblCategoryName13" runat="server" CssClass="info-box-text-1" Text='<%#Eval("Name") %>' /></span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div id="div23" runat="server" class="info-box" style="min-height: 65px;">
                                                <span class="info-box-icon bg-aqua" style="height: 65px; width: 65px; line-height: 65px;">
                                                    <asp:LinkButton ID="lnkCol2Ord3" runat="server" data-toggle="tooltip"
                                                        data-placement="top" title="Save Order" OnClick="lnkColumnOrder_Click">
                                            <i class="fa fa-save" style="color:#FFF;"></i></asp:LinkButton>
                                                </span>
                                                <div class="info-box-content">
                                                    <span class="info-box-text">
                                                        <asp:Label ID="lblCategoryName23" runat="server" CssClass="info-box-text-1" Text='<%#Eval("Name") %>' /></span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div id="div33" runat="server" class="info-box" style="min-height: 65px;">
                                                <span class="info-box-icon bg-aqua" style="height: 65px; width: 65px; line-height: 65px;">
                                                    <asp:LinkButton ID="lnkCol3Ord3" runat="server" data-toggle="tooltip"
                                                        data-placement="top" title="Save Order" OnClick="lnkColumnOrder_Click">
                                            <i class="fa fa-save" style="color:#FFF;"></i></asp:LinkButton>
                                                </span>
                                                <div class="info-box-content">
                                                    <span class="info-box-text">
                                                        <asp:Label ID="lblCategoryName33" runat="server" CssClass="info-box-text-1" Text='<%#Eval("Name") %>' /></span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div id="div43" runat="server" class="info-box" style="min-height: 65px;background-color: rgba(204, 204, 204, 0.43);">
                                                <span class="info-box-icon bg-aqua" style="height: 65px; width: 65px; line-height: 65px;">
                                                    <asp:LinkButton ID="lnkCol4Ord3" runat="server" data-toggle="tooltip"
                                                        data-placement="top">
                                            <i class="fa fa-save" style="color:#ccc;"></i></asp:LinkButton>
                                                </span>
                                                <div class="info-box-content">
                                                    <span class="info-box-text">
                                                        <asp:Label ID="lblCategoryName43" runat="server" CssClass="info-box-text-1" Text="CALL FOR APPOINTMENT"/></span>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div id="div14" runat="server" class="info-box" style="min-height: 65px;">
                                                <span class="info-box-icon bg-aqua" style="height: 65px; width: 65px; line-height: 65px;">
                                                    <asp:LinkButton ID="lnkCol1Ord4" runat="server" data-toggle="tooltip"
                                                        data-placement="top" title="Save Order" OnClick="lnkColumnOrder_Click">
                                            <i class="fa fa-save" style="color:#FFF;"></i></asp:LinkButton>
                                                </span>
                                                <div class="info-box-content">
                                                    <span class="info-box-text">
                                                        <asp:Label ID="lblCategoryName14" runat="server" CssClass="info-box-text-1" Text='<%#Eval("Name") %>' /></span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div id="div24" runat="server" class="info-box" style="min-height: 65px;">
                                                <span class="info-box-icon bg-aqua" style="height: 65px; width: 65px; line-height: 65px;">
                                                    <asp:LinkButton ID="lnkCol2Ord4" runat="server" data-toggle="tooltip"
                                                        data-placement="top" title="Save Order" OnClick="lnkColumnOrder_Click">
                                            <i class="fa fa-save" style="color:#FFF;"></i></asp:LinkButton>
                                                </span>
                                                <div class="info-box-content">
                                                    <span class="info-box-text">
                                                        <asp:Label ID="lblCategoryName24" runat="server" CssClass="info-box-text-1" Text='<%#Eval("Name") %>' /></span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div id="div34" runat="server" class="info-box" style="min-height: 65px;">
                                                <span class="info-box-icon bg-aqua" style="height: 65px; width: 65px; line-height: 65px;">
                                                    <asp:LinkButton ID="lnkCol3Ord4" runat="server" data-toggle="tooltip"
                                                        data-placement="top" title="Save Order" OnClick="lnkColumnOrder_Click">
                                            <i class="fa fa-save" style="color:#FFF;"></i></asp:LinkButton>
                                                </span>
                                                <div class="info-box-content">
                                                    <span class="info-box-text">
                                                        <asp:Label ID="lblCategoryName34" runat="server" CssClass="info-box-text-1" Text='<%#Eval("Name") %>' /></span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div id="div44" runat="server" class="info-box" style="min-height: 65px;background-color: rgba(204, 204, 204, 0.43);">
                                                <span class="info-box-icon bg-aqua" style="height: 65px; width: 65px; line-height: 65px;">
                                                    <asp:LinkButton ID="lnkCol4Ord4" runat="server" data-toggle="tooltip"
                                                        data-placement="top">
                                            <i class="fa fa-save" style="color:#ccc;"></i></asp:LinkButton>
                                                </span>
                                                <div class="info-box-content">
                                                    <span class="info-box-text">
                                                        <asp:Label ID="lblCategoryName44" runat="server" CssClass="info-box-text-1" Text="APOLLO PRISM" /></span>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </section>
</asp:Content>
