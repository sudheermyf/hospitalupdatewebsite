﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMasterPage.Master" AutoEventWireup="true" CodeBehind="DoctorsOrder.aspx.cs" Inherits="ApolloWebsite.Settings.DoctorsOrder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        $(document).ready(function () {
            $(".gvLanguageClass").sortable({
                items: 'tr:not(tr:first-child)',
                cursor: 'pointer',
                axis: 'y',
                dropOnEmpty: false,
                start: function (e, ui) {
                    ui.item.addClass("selected");
                },
                stop: function (e, ui) {
                    ui.item.removeClass("selected");
                },
            });
            $(".ddl").change(function () {
                var ddl = $(".ddl").val();
                if (ddl == "Select") {
                    $(".ddlerror").text("Please Select Department");
                    return false;
                }
                else {
                    $(".ddlerror").text("");
                }
            });
            $(".savebtn").click(function () {
                var count = 0;
                var ddl = $(".ddl").val();
                if (ddl == "Select") {
                    $(".ddlerror").text("Please Select Department");
                    count = count + 1;
                }
                if (count > 0) {
                    return false;
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content-header">
        <h1>
            <asp:Label ID="lblPageHeading" runat="server" Text="Doctor Order" />
            <small>(Add / View)</small></h1>
        <ol class="breadcrumb">
            <li><a href="<%=Page.ResolveUrl("~/Home.aspx") %>"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a>Settings</a></li>
            <li class="active"><a href="javascript:void(0);">Doctors Order</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <asp:Label ID="Label4" runat="server" Text="Set Order" /></h3>
                    </div>
                    <!-- form start -->
                    <div class="box-body body-min">
                        <div class="form-group">
                            <asp:Label ID="Label11" runat="server" Text="Select Department"></asp:Label>
                            <asp:DropDownList ID="ddlDepartment" runat="server" class="form-control ddl" DataTextField="Name" DataValueField="ID" AutoPostBack="true" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged">
                                <asp:ListItem>Select</asp:ListItem>
                            </asp:DropDownList>
                            <span class="ddlerror error-code"></span>
                        </div>
                            <div class="form-group table-responsive">
                            <asp:GridView ID="gvViewDoctors" CssClass="gvLanguageClass table table-hover" runat="server" GridLines="None" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="SNo" ItemStyle-Width="50">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                            <input type="hidden" name="doctorid" value='<%# Eval("ID") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Image" ItemStyle-Width="50">
                                        <ItemTemplate>
                                            <asp:Image ID="imgCategory" runat="server" Style="height: 35px; width: 35px;" ImageUrl='<%#string.Format(ConfigurationManager.AppSettings["apiurl"]+"api/Image/{0}",Eval("ImageID")) %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Name" HeaderText="Doctor Name" ItemStyle-Width="160" />
                                    <asp:BoundField DataField="Order" HeaderText="Sort Order" ItemStyle-Width="50" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <asp:Button ID="btnUpdateSortOrder" runat="server" Text="Update Sort Order" CssClass="btn btn-primary savebtn"
                            OnClick="btnUpdateSortOrder_Click" />
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    <div>
    </div>
</asp:Content>
