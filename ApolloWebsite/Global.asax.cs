﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using NLog.Targets.Wrappers;
using NLog.Targets;
using NLog.Config;
using NLog;
using NLog.Common;
using System.IO;

namespace ApolloWebsite
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            //string str= AppDomain.CurrentDomain.BaseDirectory.ToString();
            //FileTarget fileTarget = new FileTarget();
            //fileTarget.FileName = "${basedir}/logfile.txt";

            //PostFilteringTargetWrapper postfilteringTarget = new PostFilteringTargetWrapper();

            //ASPNetBufferingTargetWrapper aspnetBufferingTarget = new ASPNetBufferingTargetWrapper();
            //aspnetBufferingTarget.WrappedTarget = postfilteringTarget;
            //postfilteringTarget.WrappedTarget = fileTarget;

            //postfilteringTarget.DefaultFilter = "level >= LogLevel.Info";
            //FilteringRule rule = new FilteringRule();
            //rule.Exists = "level >= LogLevel.Warn";
            //rule.Filter = "level >= LogLevel.Debug";
            //postfilteringTarget.Rules.Add(rule);

            //SimpleConfigurator.ConfigureForTargetLogging(aspnetBufferingTarget, LogLevel.Debug);
            //string nlogPath = Server.MapPath("nlog-web.log");
            //InternalLogger.LogFile = nlogPath;
            //InternalLogger.LogLevel = NLog.LogLevel.Trace;  
            //using (FileStream stream = new FileStream(nlogPath, FileMode.Open, FileAccess.ReadWrite, FileShare.Read))
            //{
            //    // Do your writing here.
            //}
        }
        protected void Session_Start(object sender, EventArgs e)
        {

        }
        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}