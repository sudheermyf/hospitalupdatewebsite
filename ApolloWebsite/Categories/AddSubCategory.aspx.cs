﻿using ApolloDB.DBFolder;
using ApolloDB.Models;
using ApolloWebsite.Logic;
using CMCAPI.Client.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ApolloWebsite.Categories
{
    public partial class AddSubCategory : System.Web.UI.Page
    {
        object lockTarget = new object();
        HttpClient client;
        string username = string.Empty, password = string.Empty;
        protected string adminType = string.Empty;
        long adminid = 0;
        SyncLogins syncLogin = new SyncLogins();
        private Logger Logger = LogManager.GetCurrentClassLogger();
        protected void Page_Load(object sender, EventArgs e)
        {
            DepartmentControl.bindRelatedData -= DepartmentControl_bindRelatedData;
            DepartmentControl.bindRelatedData += DepartmentControl_bindRelatedData;
            if (Session["adminloginid"] != null)
            {
                adminType = Convert.ToString(Session["adminType"],CultureInfo.CurrentCulture);
                if (Int64.TryParse(Convert.ToString(Session["adminloginid"], CultureInfo.CurrentCulture), out adminid) && adminid > 0)
                {

                }
                else
                {
                    Response.Redirect("../Default.aspx");
                }
                username = Convert.ToString(Session["adminusername"], CultureInfo.CurrentCulture);
                password = Convert.ToString(Session["adminpassword"], CultureInfo.CurrentCulture);
            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }
            client = Logic.SyncLogins.InitiateHttp(username, password);
            client.DefaultRequestHeaders.Add("AdminType", adminType);
            if (!IsPostBack)
            {
                BindSubCategories();
                LoadCategories();
            }
        }

        private void DepartmentControl_bindRelatedData(string abcd)
        {
            long id = 0;
            if (Int64.TryParse(Convert.ToString(lblID.Text, CultureInfo.CurrentCulture), out id) && id > 0)
            {
                GetEditedData(id);
            }
        }

        private void GetEditedData(long id)
        {
            try
            {
                lock (lockTarget)
                {
                    using (ApolloDBContext dbContext = new ApolloDBContext())
                    {
                        Logger.Info("Starts When Edit Button Clicks in AddSubCategory.aspx");
                        ListBox ddlDepartments = (ListBox)DepartmentControl.FindControl("ddlDepartments");
                        GroupDropDownList.GroupDropDownList ddlStores = (GroupDropDownList.GroupDropDownList)DepartmentControl.FindControl("ddlStores");
                        Label lblDepartments = (Label)DepartmentControl.FindControl("lblDepartments");
                        Label lblStores = (Label)DepartmentControl.FindControl("lblStores");
                        List<StoreDTO> lstStores = new List<StoreDTO>();
                        
                        SubCategory subCateg = dbContext.SUBCATEGORY.FirstOrDefault(c => c.ID.Equals(id));
                        if (subCateg != null)
                        {
                            lblID.Text = Convert.ToString(id,CultureInfo.CurrentCulture);
                            switch (adminType)
                            {
                                case "A":
                                    lblDepartments.Visible = true;
                                    lblStores.Visible = true;
                                    ddlDepartments.Visible = true;
                                    ddlStores.Visible = true;
                                    List<long> lstDepartments2 = new List<long>();
                                    lstDepartments2 = Array.ConvertAll(ddlDepartments.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                                    if (lstDepartments2 != null)
                                    {
                                        lstDepartments2.ForEach(c =>
                                        {
                                            ddlDepartments.Items.FindByValue(Convert.ToString(c,CultureInfo.CurrentCulture)).Selected = true;
                                            lstStores.AddRange(syncLogin.GetStoresUnderDepartment(c));
                                        });
                                        ddlStores.Items.Clear();
                                        ddlStores.DataSource = lstStores;
                                        ddlStores.DataGroupField = "DeptName";
                                        ddlStores.DataTextField = "StoreName";
                                        ddlStores.DataValueField = "SNO";
                                        ddlStores.DataBind();
                                        if (lstStores != null && lstStores.Count > 0)
                                        {
                                            lstStores.ForEach(t =>
                                            {
                                                if (subCateg.LstLocations.Contains(dbContext.LOCATIONID.FirstOrDefault(c => c.ID.Equals(t.SNO))))
                                                {
                                                    ddlStores.Items.FindByValue(Convert.ToString(t.SNO,CultureInfo.CurrentCulture)).Selected = true;
                                                }
                                            });
                                        }
                                    }
                                    break;
                                case "D":
                                    lblDepartments.Visible = false;
                                    ddlDepartments.Visible = false;
                                    ddlStores.Visible = true;
                                    lblStores.Visible = true;
                                    List<DepartmentID> lstDepartments = subCateg.LstDepartments.Where(c => c.ID.Equals(adminid)).ToList();
                                    if (lstDepartments != null)
                                    {
                                        lstDepartments.ForEach(c =>
                                        {
                                            lstStores.AddRange(syncLogin.GetStoresUnderDepartment(c.ID));
                                        });
                                        ddlStores.Items.Clear();
                                        ddlStores.DataSource = lstStores;
                                        ddlStores.DataGroupField = "DeptName";
                                        ddlStores.DataTextField = "StoreName";
                                        ddlStores.DataValueField = "SNO";
                                        ddlStores.DataBind();
                                        if (lstStores != null && lstStores.Count > 0)
                                        {
                                            lstStores.ForEach(t =>
                                            {
                                                if (subCateg.LstLocations.Contains(dbContext.LOCATIONID.FirstOrDefault(c => c.ID.Equals(t.SNO))))
                                                {
                                                    ddlStores.Items.FindByValue(Convert.ToString(t.SNO,CultureInfo.CurrentCulture)).Selected = true;
                                                }
                                            });
                                        }
                                    }
                                    break;
                                case "S":
                                    lblStores.Visible = false;
                                    ddlStores.Visible = false;
                                    lblDepartments.Visible = false;
                                    ddlDepartments.Visible = false;
                                    break;
                                default:
                                    break;
                            }
                        }
                        Logger.Info("Ends When Edit Button Clicks in AddSubCategory.aspx");
                    }
                }
            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(GetType(), "notifier3", "errorAlert('" + ex.Message + "');", true);
            }
        }
        private void LoadCategories()
        {
            lock(lockTarget)
            {
                ddlCategory.DataSource = null;
                ddlCategory.DataBind();
                ddlViewCategory.DataSource = null;
                ddlViewCategory.DataBind();
                Logger.Info("Starts LoadCategories() in AddSubCategory.aspx");
                using (ApolloDBContext dbContext = new ApolloDBContext())
                {
                    if (dbContext.CATEGORY.Count() > 0)
                    {
                        switch (adminType)
                        {
                            case "A":
                                ddlCategory.DataSource = dbContext.CATEGORY.Where(c => c.RegAppID.Equals(adminid)).ToList();
                                ddlCategory.DataBind();
                                ddlViewCategory.DataSource = dbContext.CATEGORY.Where(c => c.RegAppID.Equals(adminid) && c.LstSubCategories.Count > 0).ToList();
                                ddlViewCategory.DataBind();
                                break;
                            case "D":
                                ddlCategory.DataSource = dbContext.CATEGORY.Where(c => c.LstDepartments.Any(d => d.ID.Equals(adminid))).ToList();
                                ddlCategory.DataBind();
                                ddlViewCategory.DataSource = dbContext.CATEGORY.Where(c => c.LstDepartments.Any(d => d.ID.Equals(adminid) && c.LstSubCategories.Count > 0)).ToList();
                                ddlViewCategory.DataBind();
                                break;
                            case "S":
                                ddlCategory.DataSource = dbContext.CATEGORY.Where(c => c.LstLocations.Any(d => d.ID.Equals(adminid))).ToList();
                                ddlCategory.DataBind();
                                ddlViewCategory.DataSource = dbContext.CATEGORY.Where(c => c.LstLocations.Any(d => d.ID.Equals(adminid) && c.LstSubCategories.Count > 0)).ToList();
                                ddlViewCategory.DataBind();
                                break;
                            default:
                                break;
                        }
                    }
                }
                Logger.Info("Ends LoadCategories() in AddSubCategory.aspx");
            }
        }
        private void BindSubCategories()
        {
            gvViewSubCategory.DataSource = null;
            gvViewSubCategory.DataBind();
            if (ddlViewCategory.SelectedIndex > 0)
            {
                long categId = 0;
                if (Int64.TryParse(ddlViewCategory.SelectedValue, out categId) && categId > 0)
                {
                    using (ApolloDBContext dbContext = new ApolloDBContext())
                    {
                        Category categ = dbContext.CATEGORY.FirstOrDefault(c => c.ID.Equals(categId));
                        if (categ != null && categ.LstSubCategories != null && categ.LstSubCategories.Count > 0)
                        {
                            gvViewSubCategory.DataSource = categ.LstSubCategories;
                            gvViewSubCategory.DataBind();
                        }
                    }
                }
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                lock (lockTarget)
                {
                    using (ApolloDBContext dbContext = new ApolloDBContext())
                    {
                        Logger.Info("Starts When Submit Button Clicks in AddSubCategory.aspx");
                        string path = MainResource.ServerPath;
                        string folder = path + @"\CategoryImages";
                        if (!Directory.Exists(folder))
                        {
                            Directory.CreateDirectory(folder);
                        }

                        if (btnSubmit.Text.Equals(MainResource.Submit))
                        {
                            try
                            {
                                if (CheckNameExists())
                                {
                                    SubCategory subcateg = new SubCategory();
                                    string strCategName = ddlCategory.SelectedItem.Text;
                                    Category categ = dbContext.CATEGORY.FirstOrDefault(c => c.Name.Equals(strCategName));
                                    subcateg.Category = categ;
                                    if (dbContext.SUBCATEGORY.ToList().Count > 0)
                                    {
                                        subcateg.ID = dbContext.SUBCATEGORY.Max(c => c.ID) + 1;
                                    }
                                    else
                                    {
                                        subcateg.ID = 1;
                                    }
                                    subcateg.Description = txtSubCategDesc.Text;
                                    foreach (HttpPostedFile Img in fuSubCategoryImage.PostedFiles)
                                    {
                                        lock (lockTarget)
                                        {
                                            string fileName = Path.GetFileName(Img.FileName);
                                            string strRandomNumber = new Random().Next(1000, 100000).ToString() + txtSubCategoryName.Text.Trim().ToUpper() + Path.GetExtension(fileName);
                                            Img.SaveAs(Path.Combine(folder, strRandomNumber));
                                            ImageInformation imgInfo = new ImageInformation();
                                            imgInfo.Name = strRandomNumber;
                                            imgInfo.Content = File.ReadAllBytes(folder + "\\" + strRandomNumber);
                                            imgInfo.ContentType = enums.Image.ToString();
                                            imgInfo.ContentExtension = Path.GetExtension(Img.FileName);
                                            subcateg.ImageExtension = Path.GetExtension(Img.FileName);
                                            MongoOperation mongo = new MongoOperation();
                                            if (mongo.InsertObjectToMongoDB(imgInfo))
                                            {
                                                subcateg.ImageID = imgInfo.IMGID;
                                            }
                                            File.Delete(folder + "\\" + strRandomNumber);
                                            subcateg.Name = txtSubCategoryName.Text;
                                            subcateg.LastUpdatedTime = DateTime.Now;
                                            if (chkIsActive.Checked)
                                            {
                                                subcateg.IsActive = true;
                                            }
                                            List<long> lstDepartments = new List<long>();
                                            List<long> lstStores = new List<long>();
                                            ListBox ddlDepartments = (ListBox)DepartmentControl.FindControl("ddlDepartments");
                                            GroupDropDownList.GroupDropDownList ddlStores = (GroupDropDownList.GroupDropDownList)DepartmentControl.FindControl("ddlStores");
                                            switch (adminType)
                                            {
                                                case "A":
                                                    lstDepartments = Array.ConvertAll(ddlDepartments.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                                                    subcateg.LstDepartments = dbContext.DEPARTMENTID.Where(c => lstDepartments.Contains(c.ID)).ToList();
                                                    lstStores = Array.ConvertAll(ddlStores.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                                                    subcateg.LstLocations = dbContext.LOCATIONID.Where(c => lstStores.Contains(c.ID)).ToList();
                                                    subcateg.RegAppID =adminid;
                                                    break;
                                                case "D":
                                                    subcateg.LstDepartments = dbContext.DEPARTMENTID.Where(c => c.ID.Equals(adminid)).ToList();
                                                    lstStores = Array.ConvertAll(ddlStores.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                                                    subcateg.LstLocations = dbContext.LOCATIONID.Where(c => lstStores.Contains(c.ID)).ToList();
                                                    DepartmentDTO dept = syncLogin.Getdepartment();
                                                    subcateg.RegAppID = dept.RegAppID;
                                                    break;
                                                case "S":
                                                    subcateg.LstLocations = dbContext.LOCATIONID.Where(c => c.ID.Equals(adminid)).ToList();
                                                    StoreDTO store = new StoreDTO();
                                                    store = syncLogin.GetStoreById();
                                                    subcateg.LstDepartments = dbContext.DEPARTMENTID.Where(c => c.ID.Equals(store.DeptID)).ToList();
                                                    subcateg.RegAppID = store.RegAppID;
                                                    break;
                                                default:
                                                    break;
                                            }
                                            dbContext.SUBCATEGORY.Add(subcateg);
                                            dbContext.SaveChanges();
                                            ClientScript.RegisterStartupScript(GetType(), "notifier15", "successAlert('SubCategory Added Successfully');", true);
                                            ClearAll();

                                            BindSubCategories();
                                            LoadCategories();
                                        }
                                    }
                                }
                                Logger.Info("Ends When Submit Button Clicks in AddSubCategory.aspx");
                            }
                            catch (Exception ex)
                            {
                                ClientScript.RegisterStartupScript(GetType(), "notifier15", "errorAlert('" + ex.Message + "');", true);
                            }
                        }
                        else if (btnSubmit.Text.Equals(MainResource.Update))
                        {
                            try
                            {
                                Logger.Info("Starts When Update() button Clicks() in AddSubCategory.aspx");
                                long id = 0;
                                if (Int64.TryParse(lblID.Text, out id) && id > 0)
                                {
                                    SubCategory subcateg = dbContext.SUBCATEGORY.FirstOrDefault(c => c.ID.Equals(id));
                                    if (!subcateg.Name.Equals(txtSubCategoryName.Text))
                                    {
                                        CheckNameExists();
                                    }
                                    subcateg.Name = txtSubCategoryName.Text;
                                    subcateg.Order = 0;
                                    subcateg.ColumnNbr = 0;
                                    subcateg.Description = txtSubCategDesc.Text;
                                    if (fuSubCategoryImage.HasFile)
                                    {
                                        foreach (HttpPostedFile Img in fuSubCategoryImage.PostedFiles)
                                        {
                                            lock (lockTarget)
                                            {
                                                string fileName = Path.GetFileName(Img.FileName);
                                                string strRandomNumber = new Random().Next(1000, 100000).ToString() + txtSubCategoryName.Text.Trim().ToUpper() + Path.GetExtension(fileName);
                                                Img.SaveAs(Path.Combine(folder, strRandomNumber));
                                                ImageInformation imgInfo = new ImageInformation();
                                                imgInfo.Name = strRandomNumber;
                                                imgInfo.Content = File.ReadAllBytes(folder + "\\" + strRandomNumber);
                                                imgInfo.ContentType = enums.Image.ToString();
                                                imgInfo.ContentExtension = Path.GetExtension(Img.FileName);
                                                subcateg.ImageExtension = Path.GetExtension(Img.FileName);
                                                MongoOperation mongo = new MongoOperation();
                                                if (mongo.InsertObjectToMongoDB(imgInfo))
                                                {
                                                    subcateg.ImageID = imgInfo.IMGID;
                                                }
                                                File.Delete(folder + "\\" + strRandomNumber);
                                            }
                                        }
                                    }
                                    subcateg.LastUpdatedTime = DateTime.Now;
                                    if (chkIsActive.Checked)
                                    {
                                        subcateg.IsActive = true;
                                    }
                                    else
                                    {
                                        subcateg.IsActive = false;
                                    }
                                    List<long> lstDepts = new List<long>();
                                    List<long> lstStores = new List<long>();
                                    ListBox ddlDepartments = (ListBox)DepartmentControl.FindControl("ddlDepartments");
                                    GroupDropDownList.GroupDropDownList ddlStores = (GroupDropDownList.GroupDropDownList)DepartmentControl.FindControl("ddlStores");

                                    switch (adminType)
                                    {
                                        case "A":
                                            #region Department operations
                                            lstDepts = Array.ConvertAll(ddlDepartments.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                                            List<long> RemoveLstDepartments = subcateg.LstDepartments.Where(c => !lstDepts.Contains(c.ID)).Select(c => c.ID).ToList();
                                            if (RemoveLstDepartments != null && RemoveLstDepartments.Count > 0)
                                            {
                                                RemoveLstDepartments.ForEach(d =>
                                                {
                                                    DepartmentID RemoveDeptDto = subcateg.LstDepartments.FirstOrDefault(c => c.ID.Equals(d));
                                                    if (RemoveDeptDto != null)
                                                    {
                                                        subcateg.LstDepartments.Remove(RemoveDeptDto);
                                                    }
                                                });
                                            }
                                            lock (lockTarget)
                                            {
                                                List<DepartmentID> dpt = dbContext.DEPARTMENTID.Where(c => lstDepts.Contains(c.ID)).ToList();
                                                subcateg.LstDepartments = dpt;
                                            }
                                            #endregion
                                            #region Store Operation
                                            List<long> RemoveLstStores = subcateg.LstLocations.Where(c => !lstStores.Contains(c.ID)).Select(c => c.ID).ToList();
                                            RemoveLstStores.ToList().ForEach(d =>
                                            {
                                                LocationID RemovelocationDto = subcateg.LstLocations.ToList().Where(c => c.ID.Equals(d)).FirstOrDefault();
                                                subcateg.LstLocations.Remove(RemovelocationDto);
                                            });
                                            lstStores = Array.ConvertAll(ddlStores.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                                            lock (lockTarget)
                                            {
                                                List<LocationID> location = dbContext.LOCATIONID.Where(c => lstStores.Contains(c.ID)).ToList();
                                                subcateg.LstLocations = location;
                                            }
                                            #endregion
                                            break;
                                        case "D":
                                            #region Store Operation
                                            List<long> RemoveLstStores1 = subcateg.LstLocations.Where(c => !lstStores.Contains(c.ID)).Select(c => c.ID).ToList();
                                            RemoveLstStores1.ToList().ForEach(d =>
                                            {
                                                LocationID RemovelocationDto = subcateg.LstLocations.ToList().Where(c => c.ID.Equals(d)).FirstOrDefault();
                                                subcateg.LstLocations.Remove(RemovelocationDto);
                                            });
                                            lstStores = Array.ConvertAll(ddlStores.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                                            lock (lockTarget)
                                            {
                                                List<LocationID> location = dbContext.LOCATIONID.Where(c => lstStores.Contains(c.ID)).ToList();
                                                subcateg.LstLocations = location;
                                            }
                                            #endregion
                                            break;
                                        case "S":
                                            break;
                                        default:
                                            break;
                                    }
                                    dbContext.SaveChanges();
                                    ClientScript.RegisterStartupScript(GetType(), "notifier15", "successAlert('SubCategory Updated Successfully');", true);
                                    CancelUpdate();
                                    BindSubCategories();
                                    Logger.Info("Ends When Update() button Clicks() in AddSubCategory.aspx");
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.Error(ex, ex.Message);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                ClientScript.RegisterStartupScript(GetType(), "notifier3", "errorAlert('" + ex.Message + "');", true);
            }
        }
        private bool CheckNameExists()
        {
            string subcategName = txtSubCategoryName.Text;
            using (ApolloDBContext dbContext = new ApolloDBContext())
            {
                long categID = 0;
                if (Int64.TryParse(ddlCategory.SelectedValue, out categID) && categID > 0)
                {
                    string categname = ddlCategory.SelectedItem.Text;
                    long sid = 0;
                    switch (adminType)
                    {
                        case "A":
                            sid = adminid;
                            break;
                        case "D":
                            DepartmentDTO dept = syncLogin.Getdepartment();
                            sid = dept.RegAppID;
                            break;
                        case "S":
                            StoreDTO store = new StoreDTO();
                            store = syncLogin.GetStoreById();
                            sid = store.RegAppID;
                            break;
                        default:
                            break;
                    }
                    Category categ = dbContext.CATEGORY.FirstOrDefault(c => c.Name.Equals(categname)  && c.RegAppID.Equals(sid));
                    if (categ!=null && categ.LstSubCategories != null && categ.LstSubCategories.Count > 0)
                    {
                        SubCategory subcateg = categ.LstSubCategories.FirstOrDefault(c => c.Name.Equals(subcategName));
                        if (btnSubmit.Text.Equals(MainResource.Submit))
                        {
                            if (subcateg == null)
                            {
                                subcategError.InnerText = string.Empty;
                                return true;
                            }
                        }
                        if (btnSubmit.Text.Equals(MainResource.Update))
                        {
                            long id = 0;
                            if (Int64.TryParse(lblID.Text, out id) && id > 0)
                            {
                                if (subcateg == null || subcateg.ID.Equals(id))
                                {
                                    subcategError.InnerText = string.Empty;
                                    return true;
                                }
                            }
                        }
                    }
                    else
                    {
                        return true;
                    }
                    subcategError.InnerText = "Sub-Category name already exists";
                    ClientScript.RegisterStartupScript(GetType(), "notifier16", "errorAlert('Sub-Category name already exists');", true);
                }
            }
            return false;
        }

        private void ClearAll()
        {
            txtSubCategoryName.Text = string.Empty;
            txtSubCategDesc.Text = string.Empty;
            ddlCategory.ClearSelection();
            chkIsActive.Checked = true;
            subcategError.InnerText = string.Empty;
            fuCateg.InnerText=string.Empty;
            ddlErr.InnerText=string.Empty;
            subcategError.InnerText = string.Empty;
            ListBox ddlDepartments = (ListBox)DepartmentControl.FindControl("ddlDepartments");
            GroupDropDownList.GroupDropDownList ddlStores = (GroupDropDownList.GroupDropDownList)DepartmentControl.FindControl("ddlStores");
            ddlDepartments.ClearSelection();
            ddlStores.ClearSelection();
            if (adminType.Equals("A"))
            {
                ddlStores.Items.Clear();
            }
        }

        protected void gvViewSubCategory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblIsActive = ((Label)e.Row.FindControl("lblIsActive"));
                if (lblIsActive.Text.Equals(MainResource.False))
                {
                    lblIsActive.Attributes.Add("class", "badge bg-red");
                    lblIsActive.Attributes["data-toggle"] = "tooltip";
                    lblIsActive.Attributes["data-placement"] = "top";
                    lblIsActive.Attributes["title"] = "Please Edit to enable.";
                }
            }
        }
        protected void gvViewSubCategory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            long id = 0;
            if (Int64.TryParse(e.CommandArgument.ToString(), out id) && id > 0)
            {
                if (e.CommandName.Equals(MainResource.Edit))
                {
                    GetEditData(id);
                }
                else if (e.CommandName.Equals(MainResource.Delete))
                {
                    lock (lockTarget)
                    {
                        Logger.Info("Starts When Delete Button Clicks in AddSubCategory.aspx");
                        using (ApolloDBContext dbContext = new ApolloDBContext())
                        {
                            SubCategory subCateg = dbContext.SUBCATEGORY.FirstOrDefault(c => c.ID.Equals(id));
                            if (subCateg != null)
                            {
                                if (subCateg.LstDocuments != null && subCateg.LstDocuments.Count > 0)
                                {
                                    ClientScript.RegisterStartupScript(GetType(), "notifier15", "errorAlert('Please delete Documents under this SubCategory');", true);
                                }
                                else
                                {
                                    dbContext.SUBCATEGORY.Remove(subCateg);
                                    dbContext.SaveChanges();
                                    ClientScript.RegisterStartupScript(GetType(), "notifier15", "successAlert('SubCategory Deleted Successfully');", true);
                                    CancelUpdate();
                                }
                            }
                        }
                        BindSubCategories();
                        Logger.Info("Ends When Delete Button Clicks in AddSubCategory.aspx");
                    }
                }
            }
        }

        private void GetEditData(long id)
        {
            try
            {
                lock (lockTarget)
                {
                    using (ApolloDBContext dbContext = new ApolloDBContext())
                    {
                        Logger.Info("Starts When Edit Button Clicks in AddSubCategory.aspx");
                        ListBox ddlDepartments = (ListBox)DepartmentControl.FindControl("ddlDepartments");
                        GroupDropDownList.GroupDropDownList ddlStores = (GroupDropDownList.GroupDropDownList)DepartmentControl.FindControl("ddlStores");
                        Label lblDepartments = (Label)DepartmentControl.FindControl("lblDepartments");
                        ddlDepartments.ClearSelection();
                        Label lblStores = (Label)DepartmentControl.FindControl("lblStores");
                        List<StoreDTO> lstStores = new List<StoreDTO>();
                        SubCategory subCateg = dbContext.SUBCATEGORY.FirstOrDefault(c => c.ID.Equals(id));
                        if (subCateg != null)
                        {
                            lblID.Text = Convert.ToString(id,CultureInfo.CurrentCulture);
                            txtSubCategoryName.Text = subCateg.Name;
                            txtSubCategDesc.Text = subCateg.Description;
                            ddlCategory.ClearSelection();
                            string strSelectedCateg = subCateg.Category.Name;
                            ddlCategory.Items.FindByText(strSelectedCateg).Selected = true;
                            if (subCateg.IsActive)
                            {
                                chkIsActive.Checked = true;
                            }
                            else
                            {
                                chkIsActive.Checked = false;
                            }
                            switch (adminType)
                            {
                                case "A":
                                    lblDepartments.Visible = true;
                                    lblStores.Visible = true;
                                    ddlDepartments.Visible = true;
                                    ddlStores.Visible = true;
                                    if (subCateg.LstDepartments != null)
                                    {
                                        subCateg.LstDepartments.ForEach(c =>
                                        {
                                            ddlDepartments.Items.FindByValue(Convert.ToString(c.ID,CultureInfo.CurrentCulture)).Selected = true;
                                            lstStores.AddRange(syncLogin.GetStoresUnderDepartment(c.ID));
                                        });
                                        ddlStores.Items.Clear();
                                        ddlStores.DataSource = lstStores;
                                        ddlStores.DataGroupField = "DeptName";
                                        ddlStores.DataTextField = "StoreName";
                                        ddlStores.DataValueField = "SNO";
                                        ddlStores.DataBind();
                                        if (lstStores != null && lstStores.Count > 0)
                                        {
                                            lstStores.ForEach(t =>
                                            {
                                                if (subCateg.LstLocations.Contains(dbContext.LOCATIONID.FirstOrDefault(c => c.ID.Equals(t.SNO))))
                                                {
                                                    ddlStores.Items.FindByValue(Convert.ToString(t.SNO,CultureInfo.CurrentCulture)).Selected = true;
                                                }
                                            });
                                        }
                                    }
                                    break;
                                case "D":
                                    lblDepartments.Visible = false;
                                    ddlDepartments.Visible = false;
                                    ddlStores.Visible = true;
                                    lblStores.Visible = true;
                                    List<DepartmentID> lstDepartments = subCateg.LstDepartments.Where(c => c.ID.Equals(adminid)).ToList();
                                    if (lstDepartments != null)
                                    {
                                        lstDepartments.ForEach(c =>
                                        {
                                            lstStores.AddRange(syncLogin.GetStoresUnderDepartment(c.ID));
                                        });
                                        ddlStores.Items.Clear();
                                        ddlStores.DataSource = lstStores;
                                        ddlStores.DataGroupField = "DeptName";
                                        ddlStores.DataTextField = "StoreName";
                                        ddlStores.DataValueField = "SNO";
                                        ddlStores.DataBind();
                                        if (lstStores != null && lstStores.Count > 0)
                                        {
                                            lstStores.ForEach(t =>
                                            {
                                                if (subCateg.LstLocations.Contains(dbContext.LOCATIONID.FirstOrDefault(c => c.ID.Equals(t.SNO))))
                                                {
                                                    ddlStores.Items.FindByValue(Convert.ToString(t.SNO,CultureInfo.CurrentCulture)).Selected = true;
                                                }
                                            });
                                        }
                                    }
                                    break;
                                case "S":
                                    lblStores.Visible = false;
                                    ddlStores.Visible = false;
                                    lblDepartments.Visible = false;
                                    ddlDepartments.Visible = false;
                                    break;
                                default:
                                    break;
                            }
                            imgCategory.ImageUrl = ConfigurationManager.AppSettings["apiurl"] + "api/Image/" + subCateg.ImageID;
                            imgCategory.Visible = true;
                            btnSubmit.Text = MainResource.Update;
                            btnClear.Text = MainResource.Reset;
                            btnUpdateCancel.Visible = true;
                            lblPageHeading.Text = MainResource.EditSubCategory;
                        }
                        Logger.Info("Ends When Edit Button Clicks in AddSubCategory.aspx");
                    }
                }
            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(GetType(), "notifier3", "errorAlert('" + ex.Message + "');", true);
            }
        }

        protected void gvViewSubCategory_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }
        protected void gvViewSubCategory_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            if (btnClear.Text.Equals(MainResource.Clear))
            {
                ClearAll();
            }
            else
            {
                Logger.Info("Starts Reset Button in AddSubCategory Page");
                long id = 0;
                if (Int64.TryParse(lblID.Text, out id) && id > 0)
                {
                    GetEditData(id);
                    Logger.Info("Ends Reset Button in AddSubCategory Page");
                }
            }
        }

        protected void btnUpdateCancel_Click(object sender, EventArgs e)
        {
            CancelUpdate();
        }
        private void CancelUpdate()
        {
            lblPageHeading.Text = MainResource.SubCategory;
            btnSubmit.Text = MainResource.Submit;
            btnClear.Text = MainResource.Clear;
            btnUpdateCancel.Visible = false;
            lblID.Text = string.Empty;
            imgCategory.Visible = false;
            ClearAll();
        }

        protected void ddlViewCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            gvViewSubCategory.DataSource = null;
            gvViewSubCategory.DataBind();
            if (ddlViewCategory.SelectedIndex > 0)
            {
                BindSubCategories();
            }
        }
    }
}