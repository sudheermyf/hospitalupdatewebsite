﻿using ApolloDB.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ApolloWebsite.Logic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Net;
using CMCAPI.Client.Models;
using ApolloDB.DBFolder;
using System.Data;
using NLog;
using System.Globalization;

namespace ApolloWebsite.Categories
{
    public partial class AddCategory : System.Web.UI.Page
    {
        object lockTarget = new object();
        HttpClient client;
        string username = string.Empty, password = string.Empty;
        protected string adminType = string.Empty;
        long adminid=0;
        SyncLogins syncLogin = new SyncLogins();
        private Logger Logger = LogManager.GetCurrentClassLogger();
        DataTable dt;
        DataRow dr;
        protected void Page_Load(object sender, EventArgs e)
        {
            DepartmentControl.bindRelatedData -= DepartmentControl_bindRelatedData;
            DepartmentControl.bindRelatedData += DepartmentControl_bindRelatedData;
            if (Session["adminloginid"] != null)
            {
                adminType = Convert.ToString(Session["adminType"],CultureInfo.CurrentCulture);
                if (Int64.TryParse(Convert.ToString(Session["adminloginid"], CultureInfo.CurrentCulture), out adminid) && adminid > 0)
                {

                }
                else
                {
                    Response.Redirect("../Default.aspx");
                }
                username = Convert.ToString(Session["adminusername"],CultureInfo.CurrentCulture);
                password = Convert.ToString(Session["adminpassword"],CultureInfo.CurrentCulture);
            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }
            client = Logic.SyncLogins.InitiateHttp(username, password);
            client.DefaultRequestHeaders.Add("AdminType", adminType);
            if (!IsPostBack)
            {
              
                BindCategories();
            }
        }

        private void DepartmentControl_bindRelatedData(string abcd)
        {
            long id=0;
            if (Int64.TryParse(Convert.ToString(lblID.Text, CultureInfo.CurrentCulture), out id) && id > 0)
            {
                GetEditedData(id);
            }
        }

        private void BindCategories()
        {
            lock (lockTarget)
            {
                gvViewCategory.DataSource = null;
                gvViewCategory.DataBind();
                using (ApolloDBContext dbContext = new ApolloDBContext())
                {
                    Logger.Info("Starts BindCategories() in AddCategory.aspx");
                    if (dbContext.CATEGORY.Count() > 0)
                    {
                        GetTotalData();
                    }
                }
                Logger.Info("Ends BindCategories() in AddCategory.aspx");
            }
        }

        private void GetTotalData()
        {
            using (ApolloDBContext dbContext = new ApolloDBContext())
            {
                dt = new DataTable();
                dt.Locale = CultureInfo.InvariantCulture;
                dr = null;
                dt.Columns.Add(new DataColumn("strSno", typeof(long)));
                dt.Columns.Add(new DataColumn("ImageID", typeof(string)));
                dt.Columns.Add(new DataColumn("BannerImageID", typeof(string)));
                dt.Columns.Add(new DataColumn("Name", typeof(string)));
                //dt.Columns.Add(new DataColumn("Department", typeof(string)));
                dt.Columns.Add(new DataColumn("strIsActive", typeof(string)));
                List<Category> lstCategories = new List<Category>();
                switch (adminType)
                {
                    case "A":
                        lstCategories = dbContext.CATEGORY.Where(c => c.RegAppID.Equals(adminid)).ToList();
                        break;
                    case "D":
                        lstCategories = dbContext.CATEGORY.Where(c => c.LstDepartments.Any(d => d.ID.Equals(adminid))).ToList();
                        break;
                    case "S":
                        lstCategories = dbContext.CATEGORY.Where(c => c.LstLocations.Any(d => d.ID.Equals(adminid))).ToList();
                        break;
                    default:
                        break;
                }
                lstCategories.ForEach(c =>
                {
                    long strSno = c.SNo;
                    dr = dt.NewRow();
                    dr["strSno"] = strSno;
                    dr["ImageID"] = c.ImageID;
                    dr["BannerImageID"] = c.BannerImageID;
                    dr["Name"] = c.Name;
                    dr["strIsActive"] = c.IsActive;
                    dt.Rows.Add(dr);
                    ViewState["CurrentTable"] = dt;
                });
                gvViewCategory.DataSource = dt;
                gvViewCategory.DataBind();
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            lock (lockTarget)
            {
                string path = MainResource.ServerPath;
                string folder = path + @"\CategoryImages";
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
                if (btnSubmit.Text.Equals(MainResource.Submit))
                {
                    if (CheckNameExists())
                    {
                        using (ApolloDBContext dbContext = new ApolloDBContext())
                        {
                            try
                            {
                                Logger.Info("Starts When Submit() button Clicks() in AddCategory.aspx");
                                Category categ = new Category();
                                categ.ID = dbContext.CATEGORY.Count() > 0 ? dbContext.CATEGORY.Max(c => c.ID) + 1 : 1;
                                categ.Description = txtCategDesc.Text;
                                foreach (HttpPostedFile Img in fuCategoryImage.PostedFiles)
                                {
                                    lock (lockTarget)
                                    {
                                        string fileName = Path.GetFileName(Img.FileName);
                                        string strRandomNumber = new Random().Next(1000, 100000).ToString() + txtCategoryName.Text.Trim().ToUpper() + Path.GetExtension(fileName);
                                        Img.SaveAs(Path.Combine(folder, strRandomNumber));
                                        ImageInformation imgInfo = new ImageInformation();
                                        imgInfo.Name = strRandomNumber;
                                        imgInfo.Content = File.ReadAllBytes(folder + "\\" + strRandomNumber);
                                        imgInfo.ContentType = enums.Image.ToString();
                                        imgInfo.ContentExtension = Path.GetExtension(Img.FileName);
                                        categ.ImageExtension = Path.GetExtension(Img.FileName);
                                        MongoOperation mongo = new MongoOperation();
                                        if (mongo.InsertObjectToMongoDB(imgInfo))
                                        {
                                            categ.ImageID = imgInfo.IMGID;
                                        }
                                        File.Delete(folder + "\\" + strRandomNumber);
                                    }
                                }
                                if (fuBannerImage.HasFile)
                                {
                                    foreach (HttpPostedFile Img in fuBannerImage.PostedFiles)
                                    {
                                        string fileName = Path.GetFileName(Img.FileName);
                                        string strRandomNumber = new Random().Next(1000, 100000).ToString() + txtCategoryName.Text.Trim().ToUpper() + Path.GetExtension(fileName);
                                        Img.SaveAs(Path.Combine(folder, strRandomNumber));
                                        ImageInformation imgInfo = new ImageInformation();
                                        imgInfo.Name = strRandomNumber;
                                        imgInfo.Content = File.ReadAllBytes(folder + "\\" + strRandomNumber);
                                        imgInfo.ContentType = enums.Image.ToString();
                                        imgInfo.ContentExtension = Path.GetExtension(Img.FileName);
                                        MongoOperation mongo = new MongoOperation();
                                        if (mongo.InsertObjectToMongoDB(imgInfo))
                                        {
                                            categ.BannerImageID = imgInfo.IMGID;
                                        }
                                        File.Delete(folder + "\\" + strRandomNumber);
                                    }
                                }
                                categ.Name = txtCategoryName.Text;
                                categ.Order = 0;
                                categ.ColumnNbr = 0;
                                categ.LastUpdatedTime = DateTime.Now;
                                if (chkIsActive.Checked)
                                {
                                    categ.IsActive = true;
                                }
                                List<long> lstDepartments = new List<long>();
                                List<long> lstStores = new List<long>();
                                ListBox ddlDepartments = (ListBox)DepartmentControl.FindControl("ddlDepartments");
                                GroupDropDownList.GroupDropDownList ddlStores = (GroupDropDownList.GroupDropDownList)DepartmentControl.FindControl("ddlStores");
                                switch (adminType)
                                {
                                    case "A":
                                        lstDepartments = Array.ConvertAll(ddlDepartments.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                                        categ.LstDepartments = dbContext.DEPARTMENTID.Where(c => lstDepartments.Contains(c.ID)).ToList();
                                        lstStores = Array.ConvertAll(ddlStores.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                                        categ.LstLocations = dbContext.LOCATIONID.Where(c => lstStores.Contains(c.ID)).ToList();
                                        categ.RegAppID = adminid;
                                        break;
                                    case "D":

                                        categ.LstDepartments=dbContext.DEPARTMENTID.Where(c => c.ID.Equals(adminid)).ToList();
                                        lstStores = Array.ConvertAll(ddlStores.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                                        categ.LstLocations = dbContext.LOCATIONID.Where(c => lstStores.Contains(c.ID)).ToList();
                                        DepartmentDTO dept = syncLogin.Getdepartment();
                                        categ.RegAppID = dept.RegAppID;

                                        break;
                                    case "S":
                                        categ.LstLocations=dbContext.LOCATIONID.Where(c => c.ID.Equals(adminid)).ToList();
                                        StoreDTO store = new StoreDTO();
                                        store = syncLogin.GetStoreById();
                                        categ.LstDepartments=dbContext.DEPARTMENTID.Where(c => c.ID.Equals(store.DeptID)).ToList();
                                        categ.RegAppID = store.RegAppID;

                                        break;
                                    default:
                                        break;
                                }
                                dbContext.CATEGORY.Add(categ);
                                dbContext.SaveChanges();
                                ClientScript.RegisterStartupScript(GetType(), "notifier15", "successAlert('Category Added Successfully');", true);
                                ClearAll();
                                BindCategories();
                                Logger.Info("Ends When Submit() button Clicks() in AddCategory.aspx");
                            }
                            catch (Exception ex)
                            {
                                Logger.Error(ex, ex.Message);
                            }
                        }
                    }
                }
                else if (btnSubmit.Text.Equals(MainResource.Update))
                {
                    try
                    {
                        using (ApolloDBContext dbContext = new ApolloDBContext())
                        {
                            Logger.Info("Starts When Update() button Clicks() in AddCategory.aspx");
                            long id = 0;
                            if (Int64.TryParse(lblID.Text, out id) && id > 0)
                            {
                                Category categ = dbContext.CATEGORY.FirstOrDefault(c => c.SNo.Equals(id));
                                if(!categ.Name.Equals(txtCategoryName.Text))
                                {
                                    CheckNameExists();
                                }
                                categ.Name = txtCategoryName.Text;
                                categ.Description = txtCategDesc.Text;
                                if (fuCategoryImage.HasFile)
                                {
                                    foreach (HttpPostedFile Img in fuCategoryImage.PostedFiles)
                                    {
                                        string fileName = Path.GetFileName(Img.FileName);
                                        string strRandomNumber = new Random().Next(1000, 100000).ToString() + txtCategoryName.Text.Trim().ToUpper() + Path.GetExtension(fileName);
                                        Img.SaveAs(Path.Combine(folder, strRandomNumber));
                                        ImageInformation imgInfo = new ImageInformation();
                                        imgInfo.Name = strRandomNumber;
                                        imgInfo.Content = File.ReadAllBytes(folder + "\\" + strRandomNumber);
                                        imgInfo.ContentType = enums.Image.ToString();
                                        imgInfo.ContentExtension = Path.GetExtension(Img.FileName);
                                        categ.ImageExtension = Path.GetExtension(Img.FileName);
                                        MongoOperation mongo = new MongoOperation();
                                        if (mongo.InsertObjectToMongoDB(imgInfo))
                                        {
                                            categ.ImageID = imgInfo.IMGID;
                                        }
                                        File.Delete(folder + "\\" + strRandomNumber);
                                    }
                                }
                                if (fuBannerImage.HasFile)
                                {
                                    foreach (HttpPostedFile Img in fuBannerImage.PostedFiles)
                                    {
                                        string fileName = Path.GetFileName(Img.FileName);
                                        string strRandomNumber = new Random().Next(1000, 100000).ToString() + txtCategoryName.Text.Trim().ToUpper() + Path.GetExtension(fileName);
                                        Img.SaveAs(Path.Combine(folder, strRandomNumber));
                                        ImageInformation imgInfo = new ImageInformation();
                                        imgInfo.Name = strRandomNumber;
                                        imgInfo.Content = File.ReadAllBytes(folder + "\\" + strRandomNumber);
                                        imgInfo.ContentType = enums.Image.ToString();
                                        imgInfo.ContentExtension = Path.GetExtension(Img.FileName);
                                        MongoOperation mongo = new MongoOperation();
                                        if (mongo.InsertObjectToMongoDB(imgInfo))
                                        {
                                            categ.BannerImageID = imgInfo.IMGID;
                                        }
                                        File.Delete(folder + "\\" + strRandomNumber);
                                    }
                                }
                                categ.LastUpdatedTime = DateTime.Now;
                                if (chkIsActive.Checked)
                                {
                                    categ.IsActive = true;
                                }
                                else
                                {
                                    categ.IsActive = false;
                                }
                                List<long> lstDepts = new List<long>();
                                List<long> lstStores = new List<long>();
                                ListBox ddlDepartments = (ListBox)DepartmentControl.FindControl("ddlDepartments");
                                GroupDropDownList.GroupDropDownList ddlStores = (GroupDropDownList.GroupDropDownList)DepartmentControl.FindControl("ddlStores");

                                switch (adminType)
                                {
                                    case "A":
                                        #region Department operations
                                        lstDepts = Array.ConvertAll(ddlDepartments.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                                        List<long> RemoveLstDepartments = categ.LstDepartments.Where(c => !lstDepts.Contains(c.ID)).Select(c => c.ID).ToList();
                                        if (RemoveLstDepartments != null && RemoveLstDepartments.Count > 0)
                                        {
                                            RemoveLstDepartments.ForEach(d =>
                                            {
                                                DepartmentID RemoveDeptDto = categ.LstDepartments.FirstOrDefault(c => c.ID.Equals(d));
                                                if (RemoveDeptDto != null)
                                                {
                                                    categ.LstDepartments.Remove(RemoveDeptDto);
                                                }
                                            });
                                        }
                                        lock (lockTarget)
                                        {
                                            List<DepartmentID> dpt = dbContext.DEPARTMENTID.Where(c => lstDepts.Contains(c.ID)).ToList();
                                            categ.LstDepartments = dpt;
                                        }
                                        #endregion
                                        #region Store Operation
                                        List<long> RemoveLstStores = categ.LstLocations.Where(c => !lstStores.Contains(c.ID)).Select(c => c.ID).ToList();
                                        RemoveLstStores.ToList().ForEach(d =>
                                        {
                                            LocationID RemovelocationDto = categ.LstLocations.ToList().Where(c => c.ID.Equals(d)).FirstOrDefault();
                                            categ.LstLocations.Remove(RemovelocationDto);
                                        });
                                        lstStores = Array.ConvertAll(ddlStores.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                                        lock (lockTarget)
                                        {
                                            List<LocationID> location = dbContext.LOCATIONID.Where(c => lstStores.Contains(c.ID)).ToList();
                                            categ.LstLocations = location;
                                        }
                                        #endregion
                                        break;
                                    case "D":
                                        #region Store Operation
                                        List<long> RemoveLstStores1 = categ.LstLocations.Where(c => !lstStores.Contains(c.ID)).Select(c => c.ID).ToList();
                                        RemoveLstStores1.ToList().ForEach(d =>
                                        {
                                            LocationID RemovelocationDto = categ.LstLocations.ToList().Where(c => c.ID.Equals(d)).FirstOrDefault();
                                            categ.LstLocations.Remove(RemovelocationDto);
                                        });
                                        lstStores = Array.ConvertAll(ddlStores.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                                        lock (lockTarget)
                                        {
                                            List<LocationID> location = dbContext.LOCATIONID.Where(c => lstStores.Contains(c.ID)).ToList();
                                            categ.LstLocations = location;
                                        }
                                        #endregion
                                        break;
                                    case "S":
                                        break;
                                    default:
                                        break;
                                }
                                dbContext.SaveChanges();
                                ClientScript.RegisterStartupScript(GetType(), "notifier15", "successAlert('Category Updated Successfully');", true);
                                BindCategories();
                                CancelUpdate();
                                Logger.Info("Ends When Update() button Clicks() in AddCategory.aspx");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex, ex.Message);
                    }
                }
            }
        }

        private bool CheckNameExists()
        {
            string categName = txtCategoryName.Text;
            using (ApolloDBContext dbContext = new ApolloDBContext())
            {
                long sid = 0;
                switch (adminType)
                {
                    case "A":
                        sid = adminid;
                        break;
                    case "D":
                        DepartmentDTO dept = syncLogin.Getdepartment();
                        sid = dept.RegAppID;
                        break;
                    case "S":
                        StoreDTO store = new StoreDTO();
                        store = syncLogin.GetStoreById();
                        sid = store.RegAppID;
                        break;
                    default:
                        break;
                }
                Category categ = dbContext.CATEGORY.FirstOrDefault(c => c.Name.Equals(categName) && c.RegAppID.Equals(sid));
                if (btnSubmit.Text.Equals(MainResource.Submit))
                {
                    if (categ == null)
                    {
                        categError.InnerText = string.Empty;
                        return true;
                    }
                }
                if (btnSubmit.Text.Equals(MainResource.Update))
                {
                    long id = 0;
                    if (Int64.TryParse(lblID.Text, out id) && id > 0)
                    {
                        if (categ == null || categ.ID.Equals(id))
                        {
                            categError.InnerText = string.Empty;
                            return true;
                        }
                    }
                }
            }
            categError.InnerText = "Category name already exists";
            ClientScript.RegisterStartupScript(GetType(), "notifier16", "errorAlert('Category name already exists');", true);
            return false;
        }
        private void ClearAll()
        {
            txtCategoryName.Text = string.Empty;
            chkIsActive.Checked = true;
            categError.InnerText = string.Empty;
            txtCategDesc.Text = string.Empty;
            ListBox ddlDepartments = (ListBox)DepartmentControl.FindControl("ddlDepartments");
            GroupDropDownList.GroupDropDownList ddlStores = (GroupDropDownList.GroupDropDownList)DepartmentControl.FindControl("ddlStores");
            ddlDepartments.ClearSelection();
            ddlStores.ClearSelection();
            if(adminType.Equals("A"))
            {
                ddlStores.Items.Clear();
            }
        }
        protected void gvViewCategory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            long id = 0;
            if (Int64.TryParse(e.CommandArgument.ToString(), out id) && id > 0)
            {
                if (e.CommandName.Equals(MainResource.Edit))
                {
                    GetEditData(id);
                }
                else if (e.CommandName.Equals(MainResource.Delete))
                {
                    try
                    {
                        using (ApolloDBContext dbContext = new ApolloDBContext())
                        {
                            lock (lockTarget)
                            {
                                Logger.Info("Starts Delete Button in AddCategory.aspx");
                                Category categ = dbContext.CATEGORY.FirstOrDefault(c => c.SNo.Equals(id));
                                if (categ != null)
                                {
                                    if (categ.LstSubCategories != null && categ.LstSubCategories.Count > 0)
                                    {
                                        ClientScript.RegisterStartupScript(GetType(), "notifier15", "errorAlert('Please delete Sub-Categories under this category.');", true);
                                    }
                                    else if (categ.LstDocuments != null && categ.LstDocuments.Count > 0)
                                    {
                                        ClientScript.RegisterStartupScript(GetType(), "notifier16", "errorAlert('Please delete Documents under this category.');", true);
                                    }
                                    else if (categ.LstTrackPageHits != null && categ.LstTrackPageHits.Count > 0)
                                    {

                                    }
                                    else
                                    {
                                        dbContext.CATEGORY.Remove(categ);
                                        dbContext.SaveChanges();
                                        ClientScript.RegisterStartupScript(GetType(), "notifier15", "successAlert('Category Deleted Successfully.');", true);
                                        CancelUpdate();
                                    }
                                }
                                BindCategories();
                                Logger.Info("Ends Delete Button in AddCategory.aspx");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex, ex.Message);
                    }
                }
            }
        }

        private void GetEditData(long id)
        {
            try
            {
                using (ApolloDBContext dbContext = new ApolloDBContext())
                {
                    ListBox ddlDepartments = (ListBox)DepartmentControl.FindControl("ddlDepartments");
                    GroupDropDownList.GroupDropDownList ddlStores = (GroupDropDownList.GroupDropDownList)DepartmentControl.FindControl("ddlStores");
                    Label lblDepartments = (Label)DepartmentControl.FindControl("lblDepartments");
                    ddlDepartments.ClearSelection();
                    Label lblStores = (Label)DepartmentControl.FindControl("lblStores");
                    List<StoreDTO> lstStores = new List<StoreDTO>();
                    lock (lockTarget)
                    {
                        Logger.Info("Starts When Edit Button Clicks in AddCategory.aspx");
                        Category categ = dbContext.CATEGORY.FirstOrDefault(c => c.SNo.Equals(id));
                        if (categ != null)
                        {
                            lblID.Text = Convert.ToString(id, CultureInfo.CurrentCulture);
                            txtCategoryName.Text = categ.Name;
                            txtCategDesc.Text = categ.Description;
                            if (categ.IsActive)
                            {
                                chkIsActive.Checked = true;
                            }
                            else
                            {
                                chkIsActive.Checked = false;
                            }
                            imgCategory.ImageUrl = ConfigurationManager.AppSettings["apiurl"] + "api/Image/" + categ.ImageID;
                            btnUpdateCancel.Visible = true;
                            imgBanner.ImageUrl = ConfigurationManager.AppSettings["apiurl"] + "api/Image/" + categ.BannerImageID;
                            if (string.IsNullOrEmpty(categ.BannerImageID))
                            {
                                imgBanner.Visible = false;
                            }
                            else
                            {
                                imgBanner.Visible = true;
                            }
                            switch (adminType)
                            {
                                case "A":
                                    lblDepartments.Visible = true;
                                    lblStores.Visible = true;
                                    ddlDepartments.Visible = true;
                                    ddlStores.Visible = true;
                                    if (categ.LstDepartments != null)
                                    {
                                        categ.LstDepartments.ForEach(c =>
                                        {
                                            ddlDepartments.Items.FindByValue(Convert.ToString(c.ID,CultureInfo.CurrentCulture)).Selected = true;
                                            lstStores.AddRange(syncLogin.GetStoresUnderDepartment(c.ID));
                                        });
                                        ddlStores.Items.Clear();
                                        ddlStores.DataSource = lstStores;
                                        ddlStores.DataGroupField = "DeptName";
                                        ddlStores.DataTextField = "StoreName";
                                        ddlStores.DataValueField = "SNO";
                                        ddlStores.DataBind();
                                        if (lstStores != null && lstStores.Count > 0)
                                        {
                                            lstStores.ForEach(t =>
                                            {
                                                if (categ.LstLocations.Contains(dbContext.LOCATIONID.FirstOrDefault(c => c.ID.Equals(t.SNO))))
                                                {
                                                    ddlStores.Items.FindByValue(Convert.ToString(t.SNO,CultureInfo.CurrentCulture)).Selected = true;
                                                }
                                            });
                                        }
                                    }
                                    break;
                                case "D":
                                    lblDepartments.Visible = false;
                                    ddlDepartments.Visible = false;
                                    ddlStores.Visible = true;
                                    lblStores.Visible = true;
                                    List<DepartmentID> lstDepartments = categ.LstDepartments.Where(c => c.ID.Equals(adminid)).ToList();
                                    if (lstDepartments != null)
                                    {
                                        lstDepartments.ForEach(c =>
                                        {
                                            lstStores.AddRange(syncLogin.GetStoresUnderDepartment(c.ID));
                                        });
                                        ddlStores.Items.Clear();
                                        ddlStores.DataSource = lstStores;
                                        ddlStores.DataGroupField = "DeptName";
                                        ddlStores.DataTextField = "StoreName";
                                        ddlStores.DataValueField = "SNO";
                                        ddlStores.DataBind();
                                        if (lstStores != null && lstStores.Count > 0)
                                        {
                                            lstStores.ForEach(t =>
                                            {
                                                if (categ.LstLocations.Contains(dbContext.LOCATIONID.FirstOrDefault(c => c.ID.Equals(t.SNO))))
                                                {
                                                    ddlStores.Items.FindByValue(Convert.ToString(t.SNO,CultureInfo.CurrentCulture)).Selected = true;
                                                }
                                            });
                                        }
                                    }
                                    break;
                                case "S":
                                    lblStores.Visible = false;
                                    ddlStores.Visible = false;
                                    lblDepartments.Visible = false;
                                    ddlDepartments.Visible = false;
                                    break;
                                default:
                                    break;
                            }
                            imgCategory.Visible = true;
                            btnSubmit.Text = MainResource.Update;
                            btnClear.Text = MainResource.Reset;
                            lblPageHeading.Text = MainResource.EditCategory;
                        }
                        Logger.Info("Ends When Edit Button Clicks in AddCategory.aspx");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }
        private void GetEditedData(long id)
        {
            try
            {
                using (ApolloDBContext dbContext = new ApolloDBContext())
                {
                    ListBox ddlDepartments = (ListBox)DepartmentControl.FindControl("ddlDepartments");
                    GroupDropDownList.GroupDropDownList ddlStores = (GroupDropDownList.GroupDropDownList)DepartmentControl.FindControl("ddlStores");
                    List<long> lstnewDepartments = new List<long>();
                    lstnewDepartments = Array.ConvertAll(ddlDepartments.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                    Label lblDepartments = (Label)DepartmentControl.FindControl("lblDepartments");
                    Label lblStores = (Label)DepartmentControl.FindControl("lblStores");
                    List<StoreDTO> lstStores = new List<StoreDTO>();
                    lock (lockTarget)
                    {
                        Logger.Info("Starts When Edit Button Clicks in AddCategory.aspx");
                        Category categ = dbContext.CATEGORY.FirstOrDefault(c => c.SNo.Equals(id));
                        if (categ != null)
                        {
                            lblID.Text = Convert.ToString(id, CultureInfo.CurrentCulture);
                            switch (adminType)
                            {
                                case "A":
                                    lblDepartments.Visible = true;
                                    lblStores.Visible = true;
                                    ddlDepartments.Visible = true;
                                    ddlStores.Visible = true;
                                    if (lstnewDepartments != null)
                                    {
                                        lstnewDepartments.ForEach(c =>
                                        {
                                            ddlDepartments.Items.FindByValue(Convert.ToString(c,CultureInfo.CurrentCulture)).Selected = true;
                                            lstStores.AddRange(syncLogin.GetStoresUnderDepartment(c));
                                        });
                                        ddlStores.Items.Clear();
                                        ddlStores.DataSource = lstStores;
                                        ddlStores.DataGroupField = "DeptName";
                                        ddlStores.DataTextField = "StoreName";
                                        ddlStores.DataValueField = "SNO";
                                        ddlStores.DataBind();
                                        if (lstStores != null && lstStores.Count > 0)
                                        {
                                            lstStores.ForEach(t =>
                                            {
                                                if (categ.LstLocations.Contains(dbContext.LOCATIONID.FirstOrDefault(c => c.ID.Equals(t.SNO))))
                                                {
                                                    ddlStores.Items.FindByValue(Convert.ToString(t.SNO,CultureInfo.CurrentCulture)).Selected = true;
                                                }
                                            });
                                        }
                                    }
                                    break;
                                case "D":
                                    lblDepartments.Visible = false;
                                    ddlDepartments.Visible = false;
                                    ddlStores.Visible = true;
                                    lblStores.Visible = true;
                                    List<DepartmentID> lstDepartments = categ.LstDepartments.Where(c => c.ID.Equals(adminid)).ToList();
                                    if (lstDepartments != null)
                                    {
                                        lstDepartments.ForEach(c =>
                                        {
                                            lstStores.AddRange(syncLogin.GetStoresUnderDepartment(c.ID));
                                        });
                                        ddlStores.Items.Clear();
                                        ddlStores.DataSource = lstStores;
                                        ddlStores.DataGroupField = "DeptName";
                                        ddlStores.DataTextField = "StoreName";
                                        ddlStores.DataValueField = "SNO";
                                        ddlStores.DataBind();
                                        if (lstStores != null && lstStores.Count > 0)
                                        {
                                            lstStores.ForEach(t =>
                                            {
                                                if (categ.LstLocations.Contains(dbContext.LOCATIONID.FirstOrDefault(c => c.ID.Equals(t.SNO))))
                                                {
                                                    ddlStores.Items.FindByValue(Convert.ToString(t.SNO,CultureInfo.CurrentCulture)).Selected = true;
                                                }
                                            });
                                        }
                                    }
                                    break;
                                case "S":
                                    lblStores.Visible = false;
                                    ddlStores.Visible = false;
                                    lblDepartments.Visible = false;
                                    ddlDepartments.Visible = false;
                                    break;
                                default:
                                    break;
                            }
                        }
                        Logger.Info("Ends When Edit Button Clicks in AddCategory.aspx");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }

    
        protected void gvViewCategory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblIsActive = ((Label)e.Row.FindControl("lblIsActive"));
                if (lblIsActive.Text.Equals(MainResource.False))
                {
                    lblIsActive.Attributes.Add("class", "badge bg-red");
                    lblIsActive.Attributes["data-toggle"] = "tooltip";
                    lblIsActive.Attributes["data-placement"] = "top";
                    lblIsActive.Attributes["title"] = "Please Edit to enable.";
                }
                Label lblImgBanner = ((Label)e.Row.FindControl("lblImgB"));
                Image img1 = ((Image)e.Row.FindControl("imgBanner1"));
                if (string.IsNullOrEmpty(lblImgBanner.Text))
                {
                    img1.Visible = false;
                }
                else
                {
                    img1.Visible = true;
                }
            }
        }
        protected void gvViewCategory_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }
        protected void gvViewCategory_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            if (btnClear.Text.Equals(MainResource.Clear))
            {
                ClearAll();
            }
            else if (btnClear.Text.Equals(MainResource.Reset))
            {
                long id = 0;
                if (Int64.TryParse(lblID.Text, out id) && id > 0)
                {
                    GetEditData(id);
                }
            }
        }

        protected void btnUpdateCancel_Click(object sender, EventArgs e)
        {
            CancelUpdate();
        }
        private void CancelUpdate()
        {
            btnSubmit.Text = MainResource.Submit;
            btnClear.Text = MainResource.Clear;
            lblPageHeading.Text = MainResource.Category;
            btnUpdateCancel.Visible = false;
            lblID.Text = string.Empty;
            imgCategory.Visible = false;
            imgBanner.Visible = false;
            categError.InnerText = string.Empty;
            ClearAll();
        }
    }
}