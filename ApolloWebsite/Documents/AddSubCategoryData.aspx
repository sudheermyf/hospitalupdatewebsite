﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMasterPage.Master" AutoEventWireup="true" CodeBehind="AddSubCategoryData.aspx.cs" Inherits="ApolloWebsite.Documents.AddSubCategoryData" %>
<%@ Register TagPrefix="customControl" Namespace="GroupDropDownList" Assembly="GroupDropDownList" %>
<%@ Register Src="~/DepartmentControl.ascx" TagPrefix="uc1" TagName="DepartmentControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="http://code.jquery.com/jquery-1.9.0.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>
     <style>
        .text1 {
            word-wrap: break-word;
        }
            .showdept{
            display:block;
        }
        .hidedept{
            display:none;
        }
    </style>
    <script>

        function reset() {
            $(".fileName").text("");
        }

        $(document).ready(function () {
            $(".ddlDocType").change(function () {                
                var value = $(".ddlDocType").val();
                if (value == "Select") {
                    $(".ddlDocTypeError").text("Please Select Document Type");
                    return false;
                }
                else {
                    $(".ddlDocTypeError").text("");
                }
                if (checkfileuploadtype()) {
                    return true;
                }
                else {
                    return false;
                }
            });
            $(".ddlCateg").change(function () {
                var value = $(".ddlCateg").val();
                if (value == "Select") {
                    $(".ddlCategError").text("Please Select Category");
                    return false;
                }
                else {
                    $(".ddlCategError").text("");
                }
            });
            $(".ddlSubCateg").change(function () {
                var value = $(".ddlSubCateg").val();
                if (value == "Select") {
                    $(".ddlSubCategError").text("Please Select SubCategory");
                    return false;
                }
                else {
                    $(".ddlSubCategError").text("");
                }
            });
            $(".fileName").keyup(function () {
                var value = $(".fileName").val();
                if (value == "") {
                    $(".fileNameError").text("Please Enter File Name");
                    return false;
                }
                else {
                    $(".fileNameError").text("");
                }
            });
            $(".imageupload1").change(function () {
                if (checkfileuploadtype()) {
                    return true;
                }
                else {
                    return false;
                }
            });

            function checkfileuploadtype() {
                debugger;
                var ddlSelectedValue;
                var ddlDocType = $(".ddlDocType").val();
                switch (ddlDocType) {
                    case "Select": ddlSelectedValue = "Select"; break;
                    case "Image": ddlSelectedValue = "Image"; break;
                    case "Video": ddlSelectedValue = "Video"; break;
                    case "PPT": ddlSelectedValue = "PPT"; break;
                    case "PDF": ddlSelectedValue = "PDF"; break;
                    default: return false; break;
                }
                var imageupload1 = $(".imageupload1").val();
                var fuExtensionValue = imageupload1.substring(imageupload1.lastIndexOf(".") + 1, imageupload1.length);

                switch (ddlSelectedValue) {
                    case "Select":
                        $(".ddlDocTypeError").text("Please Select Document Type");
                        $(".fuImageError").text("");
                        return false;
                        break;
                    case "Image":
                        if (fuExtensionValue.toUpperCase() != "PNG") {
                            $(".fuImageError").text("Please Choose PNG Format Only");
                            return false;
                        }
                        else {
                            $(".fuImageError").text(""); return true;
                        }
                        break;
                    case "Video":
                        if (fuExtensionValue.toUpperCase() != "MP4") {
                            $(".fuImageError").text("Please Choose mp4 Format Only");
                            return false;
                        }
                        else {
                            $(".fuImageError").text(""); return true;
                        }
                        break;
                    case "PPT":
                        switch (fuExtensionValue.toUpperCase()) {
                            case "PPT":
                                $(".fuImageError").text("");
                                return true;
                            case "PPTX":
                                $(".fuImageError").text("");
                                return true;
                            default:
                                $(".fuImageError").text("Please Choose ppt or pptx Format Only");
                                return false;
                                break;
                        }
                    case "PDF":
                        if (fuExtensionValue.toUpperCase() != "PDF") {
                            $(".fuImageError").text("Please Choose PDF Format Only");
                            return false;
                        }
                        else {
                            $(".fuImageError").text(""); return true;
                        }
                        break;
                    default: return false; break;
                }
            }

            $(".savebtn").click(function () {
                var count = 0;
                var value = $(".fileName").val();
                var imageupload1 = $(".imageupload1").val();
                var ddlCateg = $(".ddlCateg").val();
                var ddlSubCateg = $(".ddlSubCateg").val();
                var ddlDocType = $(".ddlDocType").val();
                var pngimg = imageupload1.substring(imageupload1.lastIndexOf(".") + 1, imageupload1.length);
                if (value == "") {
                    $(".fileNameError").text("Please Enter File Name.");
                    count = count + 1;
                }
                if (!checkfileuploadtype()) {
                    count = count + 1;
                }
                if (ddlCateg == "Select") {
                    $(".ddlCategError").text("Please Select Category");
                    count = count + 1;
                }
                if (ddlSubCateg == "Select") {
                    $(".ddlSubCategError").text("Please Select SubCategory");
                    count = count + 1;
                }
                if (ddlDocType == "Select") {
                    $(".ddlDocTypeError").text("Please Select Document Type");
                    count = count + 1;
                }
                var admintypedata = "<%=this.adminType%>";
                if (admintypedata == "A") {
                    var Listoptions = document.getElementById("<%=DepartmentControl.FindControl("ddlDepartments").ClientID%>").options;
                    var Selectedcount = 0;
                    if (Listoptions.count > 0) {
                        for (var i = 0; i < Listoptions.length; i++) {
                            if (Listoptions[i].selected == true) {
                                Selectedcount++;
                            }
                        }
                        if (Selectedcount < 1) {
                            $(".lstDeptError").text("Please select atleast one Department");
                            count = count + 1;
                        }
                    }
                }
                if (admintypedata == "A" || admintypedata == "D") {
                    var Listoptions1 = document.getElementById("<%=DepartmentControl.FindControl("ddlStores").ClientID%>").options;
                    var Selectedcount1 = 0;
                    for (var i = 0; i < Listoptions1.length; i++) {
                        if (Listoptions1[i].selected == true) {
                            Selectedcount1++;
                        }
                    }
                    if (Selectedcount1 < 1) {
                        $(".lstStoreError").text("Please select atleast one Location");
                        count = count + 1;
                    }
                }
                if (count > 0) {
                    return false;
                }
            });
        });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <section class="content-header">
        <h1>Add SubCategory Data</h1>
        <ol class="breadcrumb">
            <li><a href="<%=Page.ResolveUrl("~/Home.aspx") %>"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a>Documents</a></li>
            <li class="active"><a href="javascript:void(0);">SubCategory Data</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body body-min">
                        <div class="form-group">
                            <asp:Label ID="Label1" runat="server" Text="Category"></asp:Label>
                            <asp:DropDownList ID="ddlCategory" runat="server" class="form-control ddlCateg" DataTextField="Name" 
                                DataValueField="ID" AutoPostBack="true" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" AppendDataBoundItems="true">
                                <asp:ListItem>Select</asp:ListItem>
                            </asp:DropDownList>
                            <span class="ddlCategError error-code"></span>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label4" runat="server" Text="SubCategory"></asp:Label>
                            <asp:DropDownList ID="ddlSubCategory" runat="server" class="form-control ddlSubCateg" DataTextField="Name" DataValueField="ID" AppendDataBoundItems="true">
                                <asp:ListItem>Select</asp:ListItem>
                            </asp:DropDownList>
                            <span class="ddlSubCategError error-code"></span>
                        </div>
                        <div class="form-group">
                            <uc1:DepartmentControl runat="server" ID="DepartmentControl" />
                             <div class="col-md-12">
                                    <div class="col-md-6">
                                        <span class="lstDeptError error-code"></span>
                                    </div>
                                    <div class="col-md-6">
                                        <span class="lstStoreError error-code"></span>
                                    </div>
                                </div>
                            </div>
                        <div class="form-group col-md-12">
                            <asp:Label ID="Label6" runat="server" Text="Document Type"></asp:Label>
                            <asp:DropDownList ID="ddlDocumentType" runat="server" class="form-control ddlDocType" AppendDataBoundItems="true">
                                <asp:ListItem>Select</asp:ListItem>
                                <asp:ListItem>Image</asp:ListItem>
                                <asp:ListItem>Video</asp:ListItem>
                                <asp:ListItem>PPT</asp:ListItem>
                                <asp:ListItem>PDF</asp:ListItem>
                            </asp:DropDownList>
                            <span class="ddlDocTypeError error-code"></span>
                        </div>
                        <div class="form-group col-md-12">
                            <asp:Label ID="Label2" runat="server" Text="File Name"></asp:Label>
                            <asp:TextBox ID="txtFileName" runat="server" class="form-control fileName" placeholder="Enter FileName" />
                            <span class="fileNameError error-code"></span>
                        </div>
                        <div class="form-group col-md-12">
                            <asp:Label ID="Label3" runat="server" Text="Select File"></asp:Label>
                            <asp:FileUpload ID="fuSubCategoryImage" runat="server" CssClass="imageupload1" />
                            <span class="fuImageError error-code"></span>
                        </div>
                        <div class="checkbox form-group col-md-12">
                            <asp:CheckBox ID="chkIsActive" runat="server" Checked="true" CssClass="checkbox" Text="IsActive" />
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <asp:Button ID="btnSubmit" runat="server" class="btn btn-primary savebtn" Text="Submit" OnClick="btnSubmit_Click" />
                        <asp:Button ID="btnClear" runat="server" class="btn btn-default" OnClientClick="javascript:return reset();" OnClick="btnClear_Click" Text="Clear" />
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-info">
                    <!-- /.box-header -->
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <asp:Label ID="Label5" runat="server" Text="View Sub-Category Data" /></h3>
                    </div>
                    <!-- form start -->
                    <div class="box-body table-responsive body-min">
                        <asp:UpdatePanel ID="upd1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="form-group">
                                    <asp:Label ID="Label10" runat="server" Text="Category"></asp:Label>
                                    <asp:DropDownList ID="ddlViewCategory" runat="server" class="form-control"
                                        DataTextField="Name" DataValueField="ID" AppendDataBoundItems="true" AutoPostBack="true"
                                        OnSelectedIndexChanged="ddlViewCategory_SelectedIndexChanged">
                                        <asp:ListItem>Select</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label8" runat="server" Text="Sub-Category"></asp:Label>
                                    <asp:DropDownList ID="ddlViewSubCateg" runat="server" class="form-control ddlViewCateg"
                                        DataTextField="Name" DataValueField="ID" AppendDataBoundItems="true" AutoPostBack="true"
                                        OnSelectedIndexChanged="ddlViewSubCateg_SelectedIndexChanged">
                                        <asp:ListItem>Select</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group scroll-1">
                                    <asp:GridView ID="gvViewSubCategoryData" runat="server" class="table table-hover" AutoGenerateColumns="false" GridLines="None"
                                        OnRowCommand="gvViewSubCategoryData_RowCommand" OnRowDataBound="gvViewSubCategoryData_RowDataBound"
                                        OnRowEditing="gvViewSubCategoryData_RowEditing"
                                        OnRowDeleting="gvViewSubCategoryData_RowDeleting">
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <div class="col-md-12">
                                                        <div class="col-md-3 col-xs-3">
                                                            <asp:Label ID="Label9" runat="server" Text="Document Type" />
                                                        </div>
                                                        <div class="col-md-5 col-xs-5">
                                                            <asp:Label ID="Label5" runat="server" Text="File Name" />
                                                        </div>
                                                        <div class="col-md-2 col-xs-2">
                                                            <asp:Label ID="Label10" runat="server" Text="IsActive" />
                                                        </div>
                                                        <div class="col-md-2 col-xs-2">
                                                            <asp:Label ID="Label7" runat="server" Text="Delete" />
                                                        </div>
                                                    </div>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <div class="col-md-12">
                                                        <div class="col-md-3 col-xs-3">
                                                            <asp:Label ID="lblID" runat="server" Text='<%#Eval("ID") %>' Visible="false" />
                                                            <asp:Label ID="Label2" runat="server" Text='<%#Eval("DocumentType") %>' />
                                                            <%--<asp:Image ID="imgCategory" runat="server" ImageUrl='<%#string.Format(ConfigurationManager.AppSettings["apiurl"]+"api/Image/{0}",Eval("WebImageLocation")) %>' />--%>
                                                        </div>
                                                        <div class="col-md-5 col-xs-5">
                                                            <asp:Label ID="lblName" runat="server" CssClass="text1" Text='<%#Eval("Name") %>' />
                                                        </div>
                                                        <div class="col-md-2 col-xs-2">
                                                            <span>
                                                                <asp:Label ID="lblIsActive" runat="server" class="badge bg-green" Text='<%# Eval("IsActive") %>' /></span>
                                                        </div>
                                                        <div class="col-md-2 col-xs-2">
                                                            <asp:ImageButton ID="imgDelete" runat="server" class="img-responsive sm-icon" OnClientClick="javascript:return ConfirmDelete();" CommandName="Delete"
                                                                CommandArgument='<%#Eval("ID") %>' ImageUrl="~/Icons/delete_ic.png" data-toggle="tooltip" Style="float: left;"
                                                                data-placement="top" title="Delete Record" />
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <ul style="text-align: center;">
                                                <li class="list-group-item">No Records Found</li>
                                            </ul>
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
</asp:Content>
