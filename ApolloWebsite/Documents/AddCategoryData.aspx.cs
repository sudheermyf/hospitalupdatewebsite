﻿using ApolloDB.Models;
using Spire.Presentation;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PdfToImage;
using iTextSharp.text.pdf;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using MongoDB.Bson;
using System.Runtime.InteropServices;
using ApolloWebsite.Logic;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Net.Http;
using System.Configuration;
using CMCAPI.Client.Models;
using ApolloDB.DBFolder;
using NLog;
using System.Globalization;


namespace ApolloWebsite.Documents
{
    public partial class AddCategoryData : System.Web.UI.Page
    {
        object lockTarget = new object();
        HttpClient client;
        string username = string.Empty, password = string.Empty;
        protected string adminType = string.Empty;
        long adminid = 0;
        SyncLogins syncLogin = new SyncLogins();
        private Logger Logger = LogManager.GetCurrentClassLogger();
        protected void Page_Load(object sender, EventArgs e)
        {
            DepartmentControl.bindRelatedData -= DepartmentControl_bindRelatedData;
            DepartmentControl.bindRelatedData += DepartmentControl_bindRelatedData;
            if (Session["adminloginid"] != null)
            {
                adminType = Convert.ToString(Session["adminType"], CultureInfo.CurrentCulture);
                if (Int64.TryParse(Convert.ToString(Session["adminloginid"], CultureInfo.CurrentCulture), out adminid) && adminid > 0)
                {

                }
                else
                {
                    Response.Redirect("../Default.aspx");
                }
                username = Convert.ToString(Session["adminusername"], CultureInfo.CurrentCulture);
                password = Convert.ToString(Session["adminpassword"], CultureInfo.CurrentCulture);
            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }
            client = Logic.SyncLogins.InitiateHttp(username, password);
            client.DefaultRequestHeaders.Add("AdminType", adminType);
            if(!IsPostBack)
            {
                BindCategory();
            }
        }

        private void DepartmentControl_bindRelatedData(string abcd)
        {
           
        }
        private void BindCategoryData()
        {
            gvViewCategoryData.DataSource = null;
            gvViewCategoryData.DataBind();
            if (ddlViewCateg.SelectedIndex > 0)
            {
                long categId = 0;
                if(Int64.TryParse(ddlViewCateg.SelectedValue,out categId) && categId>0)
                {
                    using (ApolloDBContext dbContext = new ApolloDBContext())
                    {
                        Category categ = dbContext.CATEGORY.FirstOrDefault(c => c.ID.Equals(categId));
                        if (categ != null && categ.LstDocuments != null && categ.LstDocuments.Count > 0)
                        {
                            gvViewCategoryData.DataSource = categ.LstDocuments.OrderByDescending(c => c.LastUpdatedTime);
                            gvViewCategoryData.DataBind();
                        }
                    }
                }
            }
        }
        private void BindCategory()
        {
            lock(lockTarget)
            {
                ddlCategory.DataSource = null;
                ddlCategory.DataBind();
                ddlViewCateg.DataSource = null;
                ddlViewCateg.DataBind();
                Logger.Info("Starts BindCategory() in AdCategoryData.aspx");
                using (ApolloDBContext dbContext = new ApolloDBContext())
                {
                    if (dbContext.CATEGORY.Count() > 0)
                    {

                        switch (adminType)
                        {
                            case "A":
                                ddlCategory.DataSource = dbContext.CATEGORY.Where(c => c.RegAppID.Equals(adminid) && c.LstSubCategories.Count == 0).ToList();
                                ddlCategory.DataBind();
                                ddlViewCateg.DataSource = dbContext.CATEGORY.Where(c =>c.RegAppID.Equals(adminid) &&  c.LstDocuments.Count>0).ToList();
                                ddlViewCateg.DataBind();
                                break;
                            case "D":
                                ddlCategory.DataSource = dbContext.CATEGORY.Where(c =>c.LstDepartments.Any(d => d.ID.Equals(adminid) && c.LstSubCategories.Count == 0)).ToList();
                                ddlCategory.DataBind();
                                ddlViewCateg.DataSource = dbContext.CATEGORY.Where(c =>c.LstDepartments.Any(d => d.ID.Equals(adminid) && c.LstDocuments.Count > 0)).ToList();
                                ddlViewCateg.DataBind();
                                break;
                            case "S":
                                ddlCategory.DataSource = dbContext.CATEGORY.Where(c =>c.LstLocations.Any(d => d.ID.Equals(adminid) && c.LstSubCategories.Count == 0)).ToList();
                                ddlCategory.DataBind();
                                ddlViewCateg.DataSource = dbContext.CATEGORY.Where(c =>c.LstLocations.Any(d => d.ID.Equals(adminid) && c.LstDocuments.Count > 0)).ToList();
                                ddlViewCateg.DataBind();
                                break;
                            default:
                                break;
                        }
                    }
                }
                Logger.Info("Ends BindCategory() in AdCategoryData.aspx");
            }
        }
        public ApolloDB.Models.Document doc;
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Logger.Info("Starts When Submit Button Clicks in AddCategoryData.aspx");
            using (ApolloDBContext dbContext = new ApolloDBContext())
            {
                doc = new ApolloDB.Models.Document();
                doc.ID = dbContext.DOCUMENT.Count() > 0 ? dbContext.DOCUMENT.Max(c => c.ID) + 1 : 1;
                doc.Name = txtFileName.Text;
                doc.DocumentType = ddlDocumentType.SelectedItem.Text;
                doc.LastUpdatedTime = DateTime.Now;
                List<long> lstDepartments = new List<long>();
                List<long> lstStores = new List<long>();
                ListBox ddlDepartments = (ListBox)DepartmentControl.FindControl("ddlDepartments");
                GroupDropDownList.GroupDropDownList ddlStores = (GroupDropDownList.GroupDropDownList)DepartmentControl.FindControl("ddlStores");
                switch (adminType)
                {
                    case "A":
                        lstDepartments = Array.ConvertAll(ddlDepartments.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                        doc.LstDepartments = dbContext.DEPARTMENTID.Where(c => lstDepartments.Contains(c.ID)).ToList();
                        lstStores = Array.ConvertAll(ddlStores.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                        doc.LstLocations = dbContext.LOCATIONID.Where(c => lstStores.Contains(c.ID)).ToList();
                        doc.RegAppID = adminid;
                        break;
                    case "D":
                        doc.LstDepartments = dbContext.DEPARTMENTID.Where(c => c.ID.Equals(adminid)).ToList();
                        lstStores = Array.ConvertAll(ddlStores.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                        doc.LstLocations = dbContext.LOCATIONID.Where(c => lstStores.Contains(c.ID)).ToList();
                        DepartmentDTO dept = syncLogin.Getdepartment();
                        doc.RegAppID = dept.RegAppID;
                        break;
                    case "S":
                        doc.LstLocations = dbContext.LOCATIONID.Where(c => c.ID.Equals(adminid)).ToList();
                        StoreDTO store = new StoreDTO();
                        store = syncLogin.GetStoreById();
                        doc.LstDepartments = dbContext.DEPARTMENTID.Where(c => c.ID.Equals(store.DeptID)).ToList();
                        doc.RegAppID = store.RegAppID;
                        break;
                    default:
                        break;
                }
                long categid = 0;
                if (Int64.TryParse(ddlCategory.SelectedValue, out categid) && categid > 0)
                {
                    Category categ = dbContext.CATEGORY.FirstOrDefault(c => c.ID.Equals(categid));
                    List<DocumentDetails> lstDocDetails = new List<DocumentDetails>();
                    long count = 1;
                    switch (ddlDocumentType.SelectedItem.Text)
                    {
                        case "PPT":
                            Logger.Info("Starts When Document Type is PPT");
                            foreach (HttpPostedFile img in fuCategoryDataImage.PostedFiles)
                            {
                                string folder = MainResource.ServerPath + @"\DataPPT";
                                if (!Directory.Exists(folder))
                                {
                                    Directory.CreateDirectory(folder);
                                }
                                img.SaveAs(folder + "\\" + img.FileName);
                                using (Presentation presentation = new Presentation())
                                {
                                    //Presentation presentation = new Presentation();
                                    //load PPT file from disk

                                    presentation.LoadFromFile(folder + "\\" + img.FileName);

                                    //save PPT document to images

                                    for (int i = 0; i < presentation.Slides.Count; i++)
                                    {
                                        ApolloDB.Models.DocumentDetails docDetails = new ApolloDB.Models.DocumentDetails();
                                        string strRandomNumber = new Random().Next(1000, 100000) + txtFileName.Text.Trim().ToUpper();
                                        string fileName = String.Format(strRandomNumber + "result-img-{0}.png", i);
                                        System.Drawing.Image image = presentation.Slides[i].SaveAsImage(1440, 1080);
                                        image.Save(folder + "\\" + fileName, ImageFormat.Png);
                                        ImageInformation imgInfo = new ImageInformation();
                                        imgInfo.Name = strRandomNumber;
                                        imgInfo.Content = File.ReadAllBytes(folder + "\\" + fileName);
                                        imgInfo.ContentType = enums.Image.ToString();
                                        imgInfo.ContentExtension = ".png";
                                        docDetails.ImageExtension = ".png";
                                        MongoOperation mongo = new MongoOperation();
                                        if (mongo.InsertObjectToMongoDB(imgInfo))
                                        {
                                            docDetails.ImageID = imgInfo.IMGID;
                                        }
                                        File.Delete(folder + "\\" + strRandomNumber);
                                        docDetails.SNo = dbContext.DOCUMENTDETAILS.Count() > 0 ? dbContext.DOCUMENTDETAILS.Count() + 1 : 1;
                                        docDetails.ID = dbContext.DOCUMENTDETAILS.Count() > 0 ? dbContext.DOCUMENTDETAILS.Count() + 1 : 1;
                                        if (chkIsActive.Checked)
                                        {
                                            docDetails.IsActive = true;
                                        }
                                        else
                                        {
                                            docDetails.IsActive = false;
                                        }
                                        docDetails.LastUpdatedTime = DateTime.Now;
                                        docDetails.Name = fileName;
                                        lstDocDetails.Add(docDetails);
                                        count++;
                                    }
                                }
                            }
                            Logger.Info("Ends When Document Type is PPT");
                            break;
                        case "Image":
                            Logger.Info("Starts When Document Type is Image");
                            foreach (HttpPostedFile img in fuCategoryDataImage.PostedFiles)
                            {
                                ApolloDB.Models.DocumentDetails docDetails = new ApolloDB.Models.DocumentDetails();
                                string extention = Path.GetExtension(img.FileName);

                                string folder = MainResource.ServerPath + @"\DataImages";
                                if (!Directory.Exists(folder))
                                {
                                    Directory.CreateDirectory(folder);
                                }
                                string strRandomNumber = new Random().Next(1000, 100000) + txtFileName.Text.Trim().ToUpper() + extention;
                                img.SaveAs(folder + "\\" + strRandomNumber);

                                ImageInformation imgInfo = new ImageInformation();
                                imgInfo.Name = strRandomNumber;
                                imgInfo.Content = File.ReadAllBytes(folder + "\\" + strRandomNumber);
                                imgInfo.ContentType = enums.Image.ToString();
                                imgInfo.ContentExtension = Path.GetExtension(img.FileName);
                                docDetails.ImageExtension = Path.GetExtension(img.FileName);
                                MongoOperation mongo = new MongoOperation();
                                if (mongo.InsertObjectToMongoDB(imgInfo))
                                {
                                    docDetails.ImageID = imgInfo.IMGID;
                                }
                                File.Delete(folder + "\\" + strRandomNumber);
                                docDetails.Name = txtFileName.Text;
                                docDetails.LastUpdatedTime = DateTime.Now;
                                docDetails.SNo = dbContext.DOCUMENTDETAILS.Count() > 0 ? dbContext.DOCUMENTDETAILS.Count() + 1 : 1;
                                docDetails.ID = dbContext.DOCUMENTDETAILS.Count() > 0 ? dbContext.DOCUMENTDETAILS.Count() + 1 : 1;
                                if (chkIsActive.Checked)
                                {
                                    docDetails.IsActive = true;
                                }
                                else
                                {
                                    docDetails.IsActive = false;
                                }
                                lstDocDetails.Add(docDetails);
                            }
                            Logger.Info("Ends When Document Type is Image");
                            break;
                        case "PDF":
                            Logger.Info("Starts When Document Type is PDF");
                            foreach (HttpPostedFile img in fuCategoryDataImage.PostedFiles)
                            {
                                string folder = MainResource.ServerPath + @"\DataPDF";
                                if (!Directory.Exists(folder))
                                {
                                    Directory.CreateDirectory(folder);
                                }
                                string strPath = txtFileName.Text + Path.GetFileNameWithoutExtension(img.FileName);
                                img.SaveAs(folder + "\\" + txtFileName.Text + img.FileName);
                                string Path1 = folder + "\\" + strPath + Path.GetExtension(img.FileName);

                                PDFConvert Pconverter = new PDFConvert();

                                bool Converted = false;

                                Pconverter.OutputFormat = "jpeg";
                                Pconverter.OutputToMultipleFile = true;
                                Pconverter.JPEGQuality = 12;
                                Pconverter.ResolutionX = 250;
                                Pconverter.ResolutionY = 180;
                                System.IO.FileInfo input = new FileInfo(Path1);
                                string[] strpath1 = input.Name.Split('.');
                                string str = strpath1[0].ToString();

                                string output = string.Format(CultureInfo.CurrentCulture,"{0}\\{1}{2}", folder, str, ".jpeg");

                                Converted = Pconverter.Convert(input.FullName, output);

                                using (PdfReader reader = new PdfReader(Path1))
                                {

                                    for (int pagenumber = 1; pagenumber <= reader.NumberOfPages; pagenumber++)
                                    {
                                        ApolloDB.Models.DocumentDetails docDetails = new ApolloDB.Models.DocumentDetails();
                                        string strRandomNumber = str + pagenumber + ".jpeg";
                                        //string strRandomNumber = pagenumber + ".jpeg";
                                        ImageInformation imgInfo = new ImageInformation();
                                        imgInfo.Name = strRandomNumber;
                                        imgInfo.Content = File.ReadAllBytes(folder + "\\" + strRandomNumber);
                                        imgInfo.ContentType = enums.Image.ToString();
                                        imgInfo.ContentExtension = ".jpeg";
                                        docDetails.ImageExtension = ".jpeg";
                                        MongoOperation mongo = new MongoOperation();
                                        if (mongo.InsertObjectToMongoDB(imgInfo))
                                        {
                                            docDetails.ImageID = imgInfo.IMGID;
                                        }
                                        File.Delete(folder + "\\" + strRandomNumber);
                                        docDetails.SNo = dbContext.DOCUMENTDETAILS.Count() > 0 ? dbContext.DOCUMENTDETAILS.Count() + 1 : 1;
                                        docDetails.ID = dbContext.DOCUMENTDETAILS.Count() > 0 ? dbContext.DOCUMENTDETAILS.Count() + 1 : 1;
                                        docDetails.LastUpdatedTime = DateTime.Now;
                                        docDetails.Name = strRandomNumber;
                                        if (chkIsActive.Checked)
                                        {
                                            docDetails.IsActive = true;
                                        }
                                        else
                                        {
                                            docDetails.IsActive = false;
                                        }
                                        lstDocDetails.Add(docDetails);
                                        count++;
                                    }
                                }
                            }
                            Logger.Info("Ends When Document Type is PDF");
                            break;
                        case "Video":
                            Logger.Info("Starts When Document Type is Video");

                            foreach (HttpPostedFile img in fuCategoryDataImage.PostedFiles)
                            {
                                ApolloDB.Models.DocumentDetails docDetails = new ApolloDB.Models.DocumentDetails();
                                string extention = Path.GetExtension(img.FileName);
                                if (extention.Equals(".mp4", StringComparison.OrdinalIgnoreCase))
                                {
                                    string folder = MainResource.ServerPath + @"\DataVideos";
                                    if (!Directory.Exists(folder))
                                    {
                                        Directory.CreateDirectory(folder);
                                    }
                                    string strRandomNumber = new Random().Next(1000, 100000) + txtFileName.Text.Trim().ToUpper() + extention;
                                    img.SaveAs(folder + "\\" + strRandomNumber);

                                    ImageInformation imgInfo = new ImageInformation();
                                    imgInfo.Name = strRandomNumber;
                                    imgInfo.Content = File.ReadAllBytes(folder + "\\" + strRandomNumber);
                                    imgInfo.ContentType = enums.Video.ToString();
                                    imgInfo.ContentExtension = Path.GetExtension(img.FileName);
                                    docDetails.ImageExtension = Path.GetExtension(img.FileName);
                                    MongoOperation mongo = new MongoOperation();
                                    if (mongo.InsertObjectToMongoDB(imgInfo))
                                    {
                                        docDetails.ImageID = imgInfo.IMGID;
                                    }
                                    File.Delete(folder + "\\" + strRandomNumber);
                                    docDetails.Name = txtFileName.Text;
                                    docDetails.LastUpdatedTime = DateTime.Now;
                                    docDetails.SNo = dbContext.DOCUMENTDETAILS.Count() > 0 ? dbContext.DOCUMENTDETAILS.Count() + 1 : 1;
                                    docDetails.ID = dbContext.DOCUMENTDETAILS.Count() > 0 ? dbContext.DOCUMENTDETAILS.Count() + 1 : 1;
                                    if (chkIsActive.Checked)
                                    {
                                        docDetails.IsActive = true;
                                    }
                                    else
                                    {
                                        docDetails.IsActive = false;
                                    }
                                    lstDocDetails.Add(docDetails);
                                }
                            }
                            Logger.Info("Ends When Document Type is Video");
                            break;
                        default:
                            break;
                    }
                    if (doc.LstDocDetails == null)
                    {
                        doc.LstDocDetails = lstDocDetails;
                    }
                    if (categ.LstDocuments == null)
                    {
                        categ.LstDocuments = new List<Document>();
                    }
                    categ.LstDocuments.Add(doc);
                    doc.IsActive = true;
                    dbContext.DOCUMENT.Add(doc);
                    dbContext.SaveChanges();
                    Logger.Info("Ends When Submit Button Clicks in AddCategoryData.aspx");
                    
                    ClientScript.RegisterStartupScript(GetType(), "notifier15", "successAlert('Document Uploaded Successfully');setInterval(function(){location.href='AddCategoryData.aspx';},2000);", true);
                }
            }
        }

        private void ClearAll()
        {
            txtFileName.Text = string.Empty;
            ddlCategory.ClearSelection();
            ddlDocumentType.ClearSelection();
            chkIsActive.Checked = true;
            ListBox ddlDepartments = (ListBox)DepartmentControl.FindControl("ddlDepartments");
            GroupDropDownList.GroupDropDownList ddlStores = (GroupDropDownList.GroupDropDownList)DepartmentControl.FindControl("ddlStores");
            ddlDepartments.ClearSelection();
            ddlStores.ClearSelection();
            if (adminType.Equals("A"))
            {
                ddlStores.Items.Clear();
            }
           
        }

        protected void gvViewCategoryData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            long ID = 0;
            if (Int64.TryParse(e.CommandArgument.ToString(), out ID) && ID > 0)
            {
                if (e.CommandName.Equals(MainResource.Delete))
                {
                    lock (lockTarget)
                    {
                        using (ApolloDBContext dbContext = new ApolloDBContext())
                        {
                            Logger.Info("Starts When Delete Button Clicks in AddCategoryData.aspx");
                            ApolloDB.Models.Document doc = dbContext.DOCUMENT.FirstOrDefault(c => c.ID.Equals(ID));
                            if (doc != null && doc.LstDocDetails != null && doc.LstDocDetails.ToList().Count > 0)
                            {
                                doc.LstDocDetails.ToList().ForEach(c =>
                                {
                                    dbContext.DOCUMENTDETAILS.Remove(c);
                                });
                                dbContext.DOCUMENT.Remove(doc);
                                dbContext.SaveChanges();
                                ClientScript.RegisterStartupScript(GetType(), "notifier15", "successAlert('Deleted Successfully');", true);
                                BindCategoryData();
                            }
                        }
                        Logger.Info("Ends When Delete Button Clicks in AddCategoryData.aspx");
                    }
                }
            }
        }

        protected void gvViewCategoryData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblIsActive = ((Label)e.Row.FindControl("lblIsActive"));
                if (lblIsActive.Text == MainResource.False)
                {
                    lblIsActive.Attributes.Add("class", "badge bg-red");
                    lblIsActive.Attributes["data-toggle"] = "tooltip";
                    lblIsActive.Attributes["data-placement"] = "top";
                    lblIsActive.Attributes["title"] = "Please Edit to enable.";
                }
            }
        }

        protected void gvViewCategoryData_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void gvViewCategoryData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        protected void ddlViewCateg_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(ddlViewCateg.SelectedItem.Equals("Select"))
            {
                gvViewCategoryData.DataSource = null;
                gvViewCategoryData.DataBind();
            }
            else
            {
                BindCategoryData();
            }
            
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            ClearAll();
        }
    }
}