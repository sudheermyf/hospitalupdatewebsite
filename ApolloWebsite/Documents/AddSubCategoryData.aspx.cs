﻿using ApolloDB.DBFolder;
using ApolloDB.Models;
using ApolloWebsite.Logic;
using CMCAPI.Client.Models;
using iTextSharp.text.pdf;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using NLog;
using PdfToImage;
using Spire.Presentation;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ApolloWebsite.Documents
{
    public partial class AddSubCategoryData : System.Web.UI.Page
    {
        object lockTarget = new object();
        HttpClient client;
        string username = string.Empty, password = string.Empty;
        protected string adminType = string.Empty;
        long adminid = 0;
        SyncLogins syncLogin = new SyncLogins();
        private Logger Logger = LogManager.GetCurrentClassLogger();
        protected void Page_Load(object sender, EventArgs e)
        {
            DepartmentControl.bindRelatedData -= DepartmentControl_bindRelatedData;
            DepartmentControl.bindRelatedData += DepartmentControl_bindRelatedData;
            if (Session["adminloginid"] != null)
            {
                adminType = Convert.ToString(Session["adminType"], CultureInfo.CurrentCulture);
                if (Int64.TryParse(Convert.ToString(Session["adminloginid"], CultureInfo.CurrentCulture), out adminid) && adminid > 0)
                {

                }
                else
                {
                    Response.Redirect("../Default.aspx");
                }
                username = Convert.ToString(Session["adminusername"], CultureInfo.CurrentCulture);
                password = Convert.ToString(Session["adminpassword"], CultureInfo.CurrentCulture);
            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }
            client = Logic.SyncLogins.InitiateHttp(username, password);
            client.DefaultRequestHeaders.Add("AdminType", adminType);
            if(!IsPostBack)
            {
                BindCategories();
                BindSubCategoryData();
            }
        }

        private void DepartmentControl_bindRelatedData(string abcd)
        {

        }
        private void BindSubCategoryData()
        {
            gvViewSubCategoryData.DataSource = null;
            gvViewSubCategoryData.DataBind();
            if (ddlViewSubCateg.SelectedIndex > 0)
            {
                long categId = 0;
                if (Int64.TryParse(ddlViewSubCateg.SelectedValue, out categId) && categId > 0)
                {
                    using (ApolloDBContext dbContext = new ApolloDBContext())
                    {
                        SubCategory subcateg = dbContext.SUBCATEGORY.FirstOrDefault(c => c.ID.Equals(categId));
                        if (subcateg != null && subcateg.LstDocuments != null && subcateg.LstDocuments.Count > 0)
                        {
                            gvViewSubCategoryData.DataSource = subcateg.LstDocuments.OrderByDescending(c => c.LastUpdatedTime);
                            gvViewSubCategoryData.DataBind();
                        }
                    }
                }
            }
        }

        private void BindCategories()
        {
            lock (lockTarget)
            {
                Logger.Info("Starts BindCategories() in AddSubCategoryData.aspx");
                ddlCategory.DataSource = null;
                ddlCategory.DataBind();
                ddlViewCategory.DataSource = null;
                ddlViewCategory.DataBind();
                using (ApolloDBContext dbContext = new ApolloDBContext())
                {
                    if (dbContext.CATEGORY.Count() > 0)
                    {
                        switch (adminType)
                        {
                            case "A":
                                ddlCategory.DataSource = dbContext.CATEGORY.Where(c => c.RegAppID.Equals(adminid) && c.LstSubCategories.Count > 0).ToList();
                                ddlCategory.DataBind();
                                ddlViewCategory.DataSource = dbContext.CATEGORY.Where(c => c.RegAppID.Equals(adminid) && c.LstSubCategories.Count > 0).ToList();
                                ddlViewCategory.DataBind();
                                break;
                            case "D":
                                ddlCategory.DataSource = dbContext.CATEGORY.Where(c => c.LstDepartments.Any(d => d.ID.Equals(adminid) && c.LstSubCategories.Count > 0)).ToList();
                                ddlCategory.DataBind();
                                ddlViewCategory.DataSource = dbContext.CATEGORY.Where(c => c.LstDepartments.Any(d => d.ID.Equals(adminid) && c.LstSubCategories.Count > 0)).ToList();
                                ddlViewCategory.DataBind();
                                break;
                            case "S":
                                ddlCategory.DataSource = dbContext.CATEGORY.Where(c => c.LstLocations.Any(d => d.ID.Equals(adminid) && c.LstSubCategories.Count > 0)).ToList();
                                ddlCategory.DataBind();
                                ddlViewCategory.DataSource = dbContext.CATEGORY.Where(c => c.LstLocations.Any(d => d.ID.Equals(adminid) && c.LstSubCategories.Count > 0)).ToList();
                                ddlViewCategory.DataBind();
                                break;
                            default:
                                break;
                        }
                    }
                }
                Logger.Info("Ends BindCategories() in AddSubCategoryData.aspx");
            }
        }
        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearSubCategData();
            if (!ddlCategory.SelectedItem.Text.Equals("Select"))
            {
                Logger.Info("Starts ddlCategory_SelectedIndexChanged in AddSubCategoryData.aspx");
                long id = 0;
                if (Int64.TryParse(ddlCategory.SelectedValue, out id) && id > 0)
                {
                    using (ApolloDBContext dbContext = new ApolloDBContext())
                    {
                        Category categ = dbContext.CATEGORY.FirstOrDefault(c => c.ID.Equals(id));
                        if (categ != null && categ.LstSubCategories != null && categ.LstSubCategories.Count > 0)
                        {
                            ddlSubCategory.DataSource = categ.LstSubCategories;
                            ddlSubCategory.DataBind();
                        }
                    }
                }
                Logger.Info("Ends ddlCategory_SelectedIndexChanged in AddSubCategoryData.aspx");
            }
        }
        private void ClearSubCategData()
        {
            ddlSubCategory.Items.Clear();
            ddlSubCategory.Items.Insert(0, "Select");
            gvViewSubCategoryData.DataSource = null;
            gvViewSubCategoryData.DataBind();
        }
        public ApolloDB.Models.Document doc;
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            using (ApolloDBContext dbContext = new ApolloDBContext())
            {
                Logger.Info("Starts When Submit Button Clicks in AddSubCategoryData.aspx");
                string strFileName = fuSubCategoryImage.PostedFile.FileName;
                string extension = Path.GetExtension(strFileName);
                string path = Server.MapPath(fuSubCategoryImage.FileName);
                doc = new ApolloDB.Models.Document();
                doc.ID = dbContext.DOCUMENT.Count() > 0 ? dbContext.DOCUMENT.Max(c => c.ID) + 1 : 1;
                doc.Name = txtFileName.Text;
                doc.DocumentType = ddlDocumentType.SelectedItem.Text;
                doc.LastUpdatedTime = DateTime.Now;
                List<long> lstDepartments = new List<long>();
                List<long> lstStores = new List<long>();
                ListBox ddlDepartments = (ListBox)DepartmentControl.FindControl("ddlDepartments");
                GroupDropDownList.GroupDropDownList ddlStores = (GroupDropDownList.GroupDropDownList)DepartmentControl.FindControl("ddlStores");
                switch (adminType)
                {
                    case "A":
                        lstDepartments = Array.ConvertAll(ddlDepartments.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                        doc.LstDepartments = dbContext.DEPARTMENTID.Where(c => lstDepartments.Contains(c.ID)).ToList();
                        lstStores = Array.ConvertAll(ddlStores.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                        doc.LstLocations = dbContext.LOCATIONID.Where(c => lstStores.Contains(c.ID)).ToList();
                        doc.RegAppID = adminid;
                        break;
                    case "D":
                        doc.LstDepartments = dbContext.DEPARTMENTID.Where(c => c.ID.Equals(adminid)).ToList();
                        lstStores = Array.ConvertAll(ddlStores.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                        doc.LstLocations = dbContext.LOCATIONID.Where(c => lstStores.Contains(c.ID)).ToList();
                        DepartmentDTO dept = syncLogin.Getdepartment();
                        doc.RegAppID = dept.RegAppID;
                        break;
                    case "S":
                        doc.LstLocations = dbContext.LOCATIONID.Where(c => c.ID.Equals(adminid)).ToList();
                        StoreDTO store = new StoreDTO();
                        store = syncLogin.GetStoreById();
                        doc.LstDepartments = dbContext.DEPARTMENTID.Where(c => c.ID.Equals(store.DeptID)).ToList();
                        doc.RegAppID = store.RegAppID;
                        break;
                    default:
                        break;
                }
                long subcategid = 0;
                if (Int64.TryParse(ddlSubCategory.SelectedValue, out subcategid) && subcategid > 0)
                {
                    ApolloDB.Models.SubCategory subcateg = dbContext.SUBCATEGORY.FirstOrDefault(c => c.ID.Equals(subcategid));
                    List<DocumentDetails> lstDocDetails = new List<DocumentDetails>();
                    long count = 1;
                    switch (ddlDocumentType.SelectedItem.Text)
                    {
                        case "PPT":
                            Logger.Info("Starts When DocumentType is PPT");
                            foreach (HttpPostedFile img in fuSubCategoryImage.PostedFiles)
                            {
                                string folder = MainResource.ServerPath + @"\DataPPT";
                                if (!Directory.Exists(folder))
                                {
                                    Directory.CreateDirectory(folder);
                                }
                                img.SaveAs(folder + "\\" + img.FileName);
                                using (Presentation presentation = new Presentation())
                                {
                                    //load PPT file from disk

                                    presentation.LoadFromFile(folder + "\\" + img.FileName);

                                    //save PPT document to images

                                    for (int i = 0; i < presentation.Slides.Count; i++)
                                    {
                                        string strRandomNumber = new Random().Next(1000, 100000) + txtFileName.Text.Trim().ToUpper(CultureInfo.CurrentCulture);
                                        string fileName = String.Format(CultureInfo.CurrentCulture, strRandomNumber + "result-img-{0}.png", i);
                                        System.Drawing.Image image = presentation.Slides[i].SaveAsImage(1440, 1080);
                                        image.Save(folder + "\\" + fileName, ImageFormat.Png);
                                        ApolloDB.Models.DocumentDetails docDetails = new ApolloDB.Models.DocumentDetails();
                                        ImageInformation imgInfo = new ImageInformation();
                                        imgInfo.Name = strRandomNumber;
                                        imgInfo.Content = File.ReadAllBytes(folder + "\\" + fileName);
                                        imgInfo.ContentType = enums.Image.ToString();
                                        imgInfo.ContentExtension = ".png";
                                        docDetails.ImageExtension = ".png";
                                        MongoOperation mongo = new MongoOperation();
                                        if (mongo.InsertObjectToMongoDB(imgInfo))
                                        {
                                            docDetails.ImageID = imgInfo.IMGID;
                                        }
                                        File.Delete(folder + "\\" + strRandomNumber);
                                        if (dbContext.DOCUMENTDETAILS.Count() > 0)
                                        {

                                            docDetails.SNo = Convert.ToInt64(dbContext.DOCUMENTDETAILS.Count()) + count;
                                            docDetails.ID = Convert.ToInt64(dbContext.DOCUMENTDETAILS.Count()) + count;
                                        }
                                        else
                                        {
                                            docDetails.SNo = count;
                                            docDetails.ID = count;
                                        }
                                        docDetails.LastUpdatedTime = DateTime.Now;
                                        docDetails.Name = fileName;
                                        if (chkIsActive.Checked)
                                        {
                                            docDetails.IsActive = true;
                                        }
                                        else
                                        {
                                            docDetails.IsActive = false;
                                        }
                                        lstDocDetails.Add(docDetails);
                                        count++;
                                    }
                                }
                            }
                            Logger.Info("Ends When DocumentType is PPT");
                            break;
                        case "Image":
                            Logger.Info("Starts When DocumentType is Image");
                            foreach (HttpPostedFile img in fuSubCategoryImage.PostedFiles)
                            {
                                ApolloDB.Models.DocumentDetails docDetails = new ApolloDB.Models.DocumentDetails();
                                string extention = Path.GetExtension(img.FileName);

                                string folder = MainResource.ServerPath + @"\DataImages";
                                if (!Directory.Exists(folder))
                                {
                                    Directory.CreateDirectory(folder);
                                }
                                string strRandomNumber = new Random().Next(1000, 100000) + txtFileName.Text.Trim().ToUpper() + extention;
                                img.SaveAs(folder + "\\" + strRandomNumber);
                                ImageInformation imgInfo = new ImageInformation();
                                imgInfo.Name = strRandomNumber;
                                imgInfo.Content = File.ReadAllBytes(folder + "\\" + strRandomNumber);
                                imgInfo.ContentType = enums.Image.ToString();
                                imgInfo.ContentExtension = Path.GetExtension(img.FileName);
                                docDetails.ImageExtension = Path.GetExtension(img.FileName);
                                MongoOperation mongo = new MongoOperation();
                                if (mongo.InsertObjectToMongoDB(imgInfo))
                                {
                                    docDetails.ImageID = imgInfo.IMGID;
                                }
                                File.Delete(folder + "\\" + strRandomNumber);
                                docDetails.Name = strRandomNumber;
                                docDetails.LastUpdatedTime = DateTime.Now;
                                if (dbContext.DOCUMENTDETAILS.Count() > 0)
                                {
                                    docDetails.SNo = Convert.ToInt64(dbContext.DOCUMENTDETAILS.Count()) + 1;
                                    docDetails.ID = Convert.ToInt64(dbContext.DOCUMENTDETAILS.Count()) + 1;
                                }
                                else
                                {
                                    docDetails.SNo = 1;
                                    docDetails.ID = 1;
                                }
                                if (chkIsActive.Checked)
                                {
                                    docDetails.IsActive = true;
                                }
                                else
                                {
                                    docDetails.IsActive = false;
                                }
                                lstDocDetails.Add(docDetails);
                            }
                            Logger.Info("Ends When DocumentType is Image");
                            break;
                        case "PDF":
                            Logger.Info("Starts When DocumentType is PDF");
                            foreach (HttpPostedFile img in fuSubCategoryImage.PostedFiles)
                            {
                                string folder = MainResource.ServerPath + @"\DataPDF";
                                if (!Directory.Exists(folder))
                                {
                                    Directory.CreateDirectory(folder);
                                }
                                string strPath = txtFileName.Text + Path.GetFileNameWithoutExtension(img.FileName);
                                img.SaveAs(folder + "\\" + txtFileName.Text + img.FileName);
                                string Path1 = folder + "\\" + strPath + Path.GetExtension(img.FileName);

                                PDFConvert Pconverter = new PDFConvert();

                                bool Converted = false;

                                Pconverter.OutputFormat = "jpeg";

                                Pconverter.OutputToMultipleFile = true;
                                Pconverter.JPEGQuality = 12;
                                Pconverter.ResolutionX = 250;
                                Pconverter.ResolutionY = 180;
                                System.IO.FileInfo input = new FileInfo(Path1);
                                string[] strpath1 = input.Name.Split('.');
                                string str = strpath1[0].ToString();
                                string output = string.Format(CultureInfo.CurrentCulture,"{0}\\{1}{2}", folder, str, ".jpeg");

                                Converted = Pconverter.Convert(input.FullName, output);

                                using (PdfReader reader = new PdfReader(Path1))
                                {

                                    for (int pagenumber = 1; pagenumber <= reader.NumberOfPages; pagenumber++)
                                    {
                                        ApolloDB.Models.DocumentDetails docDetails = new ApolloDB.Models.DocumentDetails();
                                        string strRandomNumber = str + pagenumber + ".jpeg";
                                        ImageInformation imgInfo = new ImageInformation();
                                        imgInfo.Name = strRandomNumber;
                                        imgInfo.Content = File.ReadAllBytes(folder + "\\" + strRandomNumber);
                                        imgInfo.ContentType = enums.Image.ToString();
                                        imgInfo.ContentExtension = ".jpeg";
                                        docDetails.ImageExtension = ".jpeg";
                                        MongoOperation mongo = new MongoOperation();
                                        if (mongo.InsertObjectToMongoDB(imgInfo))
                                        {
                                            docDetails.ImageID = imgInfo.IMGID;
                                        }
                                        File.Delete(folder + "\\" + strRandomNumber);

                                        if (dbContext.DOCUMENTDETAILS.Count() > 0)
                                        {
                                            docDetails.SNo = Convert.ToInt64(dbContext.DOCUMENTDETAILS.Count()) + count;
                                            docDetails.ID = Convert.ToInt64(dbContext.DOCUMENTDETAILS.Count()) + count;
                                        }
                                        else
                                        {
                                            docDetails.SNo = count;
                                            docDetails.ID = count;
                                        }
                                        docDetails.LastUpdatedTime = DateTime.Now;
                                        docDetails.Name = strRandomNumber;
                                        if (chkIsActive.Checked)
                                        {
                                            docDetails.IsActive = true;
                                        }
                                        else
                                        {
                                            docDetails.IsActive = false;
                                        }
                                        lstDocDetails.Add(docDetails);
                                        count++;
                                    }
                                }
                            }
                            Logger.Info("Ends When DocumentType is PDF");
                            break;
                        case "Video":
                            Logger.Info("Starts When DocumentType is Video");
                            foreach (HttpPostedFile img in fuSubCategoryImage.PostedFiles)
                            {
                                ApolloDB.Models.DocumentDetails docDetails = new ApolloDB.Models.DocumentDetails();
                                string extention = Path.GetExtension(img.FileName);
                                if (extention.Equals(".mp4", StringComparison.OrdinalIgnoreCase))
                                {
                                    string folder = MainResource.ServerPath + @"\DataVideos";
                                    if (!Directory.Exists(folder))
                                    {
                                        Directory.CreateDirectory(folder);
                                    }
                                    string strRandomNumber = new Random().Next(1000, 100000) + txtFileName.Text.Trim().ToUpper() + extention;
                                    img.SaveAs(folder + "\\" + strRandomNumber);
                                    ImageInformation imgInfo = new ImageInformation();
                                    imgInfo.Name = strRandomNumber;
                                    imgInfo.Content = File.ReadAllBytes(folder + "\\" + strRandomNumber);
                                    imgInfo.ContentType = enums.Video.ToString();
                                    imgInfo.ContentExtension = Path.GetExtension(img.FileName);
                                    docDetails.ImageExtension = Path.GetExtension(img.FileName);
                                    MongoOperation mongo = new MongoOperation();
                                    if (mongo.InsertObjectToMongoDB(imgInfo))
                                    {
                                        docDetails.ImageID = imgInfo.IMGID;
                                    }
                                    File.Delete(folder + "\\" + strRandomNumber);
                                    docDetails.Name = strRandomNumber;
                                    docDetails.LastUpdatedTime = DateTime.Now;
                                    if (dbContext.DOCUMENTDETAILS.Count() > 0)
                                    {
                                        docDetails.SNo = Convert.ToInt64(dbContext.DOCUMENTDETAILS.Count()) + 1;
                                        docDetails.ID = Convert.ToInt64(dbContext.DOCUMENTDETAILS.Count()) + 1;
                                    }
                                    else
                                    {
                                        docDetails.SNo = 1;
                                        docDetails.ID = 1;
                                    }
                                    if (chkIsActive.Checked)
                                    {
                                        docDetails.IsActive = true;
                                    }
                                    else
                                    {
                                        docDetails.IsActive = false;
                                    }
                                    lstDocDetails.Add(docDetails);
                                }
                            }
                            Logger.Info("Ends When DocumentType is Video");
                            break;
                        default:
                            break;
                    }
                    if (doc.LstDocDetails == null)
                    {
                        doc.LstDocDetails = lstDocDetails;
                    }
                    if (subcateg.LstDocuments == null)
                    {
                        subcateg.LstDocuments = new List<Document>();
                    }
                    subcateg.LstDocuments.Add(doc);
                    doc.IsActive = true;
                    dbContext.DOCUMENT.Add(doc);
                    dbContext.SaveChanges();
                    Logger.Info("Ends When Submit Button Clicks in AddSubCategoryData.aspx");
                  
                    ClientScript.RegisterStartupScript(GetType(), "notifier15", "successAlert('Document Uploaded Successfully');setInterval(function(){location.href='AddSubCategoryData.aspx';},2000);", true);
                }
            }
        }
        private void ClearAll()
        {
            ddlCategory.ClearSelection();
            ddlDocumentType.ClearSelection();
            ddlSubCategory.Items.Clear();
            ddlSubCategory.Items.Insert(0, "Select");
            txtFileName.Text = string.Empty;
            chkIsActive.Checked = true;
            ListBox ddlDepartments = (ListBox)DepartmentControl.FindControl("ddlDepartments");
            GroupDropDownList.GroupDropDownList ddlStores = (GroupDropDownList.GroupDropDownList)DepartmentControl.FindControl("ddlStores");
            ddlDepartments.ClearSelection();
            ddlStores.ClearSelection();
            if (adminType.Equals("A"))
            {
                ddlStores.Items.Clear();
            }
        }
        protected void gvViewSubCategoryData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            long ID = 0;
            if (Int64.TryParse(e.CommandArgument.ToString(), out ID) && ID > 0)
            {
                if (e.CommandName.Equals(MainResource.Delete))
                {
                    lock (lockTarget)
                    {
                        Logger.Info("Starts When Delete Button Clicks in AddSubCategoryData.aspx");
                        using (ApolloDBContext dbContext = new ApolloDBContext())
                        {
                            ApolloDB.Models.Document doc1 = dbContext.DOCUMENT.FirstOrDefault(c => c.ID.Equals(ID));
                            if (doc1 != null && doc1.LstDocDetails != null && doc1.LstDocDetails.ToList().Count > 0)
                            {
                                doc1.LstDocDetails.ToList().ForEach(c =>
                                {
                                    dbContext.DOCUMENTDETAILS.Remove(c);
                                });
                                dbContext.DOCUMENT.Remove(doc1);
                                dbContext.SaveChanges();
                                ScriptManager.RegisterStartupScript(upd1, upd1.GetType(), "notifier15", "successAlert('Deleted Successfully');", true);
                                BindSubCategoryData();
                            }
                        }
                        Logger.Info("Ends When Delete Button Clicks in AddSubCategoryData.aspx");
                    }
                }
            }
        }


        protected void gvViewSubCategoryData_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void gvViewSubCategoryData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        protected void gvViewSubCategoryData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblIsActive = ((Label)e.Row.FindControl("lblIsActive"));
                if (lblIsActive.Text.Equals(MainResource.False))
                {
                    lblIsActive.Attributes.Add("class", "badge bg-red");
                    lblIsActive.Attributes["data-toggle"] = "tooltip";
                    lblIsActive.Attributes["data-placement"] = "top";
                    lblIsActive.Attributes["title"] = "Please Edit to enable.";
                }
            }
        }

        protected void ddlViewSubCateg_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindSubCategoryData();
        }

        protected void ddlViewCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearViewSubCategData();
            if (!ddlViewCategory.SelectedItem.Text.Equals("Select"))
            {
                Logger.Info("Starts ddlViewCategory in AddSubCategoryData.aspx");
                long ID = 0;
                if (Int64.TryParse(ddlViewCategory.SelectedValue, out ID) && ID > 0)
                {
                    using (ApolloDBContext dbContext = new ApolloDBContext())
                    {
                        Category categ = dbContext.CATEGORY.FirstOrDefault(c => c.ID.Equals(ID));
                        ddlViewSubCateg.DataSource = null;
                        ddlViewSubCateg.DataBind();
                        if (categ != null && categ.LstSubCategories != null && categ.LstSubCategories.Count > 0)
                        {
                            ddlViewSubCateg.DataSource = categ.LstSubCategories;
                            ddlViewSubCateg.DataBind();
                        }
                    }
                }
                Logger.Info("Ends ddlViewCategory_SelectedIndexChanged in AddSubCategoryData.aspx");
            }
        }

        private void ClearViewSubCategData()
        {
            ddlViewSubCateg.Items.Clear();
            ddlViewSubCateg.Items.Insert(0, "Select");
            gvViewSubCategoryData.DataSource = null;
            gvViewSubCategoryData.DataBind();
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            ClearAll();
        }
    }
}