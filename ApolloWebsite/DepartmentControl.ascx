﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DepartmentControl.ascx.cs" Inherits="ApolloWebsite.DepartmentControl" %>
<%@ Register TagPrefix="customControl" Namespace="GroupDropDownList" Assembly="GroupDropDownList" %>
 <script>        
     $(document).ready(function () {
         $(".lstDeptbox").change(function () {
             clearLabel();
         });
         $(".lstStoreBox").change(function () {
             debugger;
             clearLocationLabel();
         });
     });
     function changeControl() {
         clearLocationLabel();
     }
     </script>
<asp:UpdatePanel ID="updatePanelProducts" runat="server" UpdateMode="Conditional" RenderMode="Block">
    <ContentTemplate>
        <div class="col-md-6" id="departments" runat="server">
            <div class="col-md-12">
                <asp:Label ID="lblDepartments" runat="server" Text="Departments:" class="control-label"></asp:Label>
            </div>
            <div class="col-md-12">
                <asp:ListBox ID="ddlDepartments" runat="server" AppendDataBoundItems="true" AutoPostBack="true"
                    DataTextField="Name" OnSelectedIndexChanged="ddlDepartments_SelectedIndexChanged" Height="200px"
                    DataValueField="SNo" SelectionMode="Multiple" class="form-control lstDeptbox"></asp:ListBox>
                <asp:UpdateProgress ID="uppg1" runat="server" AssociatedUpdatePanelID="updatePanelProducts" ClientIDMode="Static">
                    <ProgressTemplate>
                        <asp:Image ID="imgload" runat="server" ImageUrl="~/Icons/loading.gif" />
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </div>
        </div>
        <div class="col-md-6" id="stores">
            <div class="col-md-12">
                <asp:Label ID="lblStores" runat="server" Text="Locations:" class="control-label"></asp:Label>
            </div>
            <div class="col-md-12">
                <customControl:GroupDropDownList ID="ddlStores" runat="server" SelectionMode="Multiple"
                    AppendDataBoundItems="true" Height="200px" onchange="changeControl()"
                    multiple="multiple" Style="min-width: 100%;" class="form-control lstStoreBox">
                </customControl:GroupDropDownList>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
