﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ApolloWebsite.Logic
{
    public class MongoOperation
    {
        MongoClient client = null;
        MongoServer server = null;
        MongoDatabase database = null;
        MongoCollection collection = null;
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public MongoOperation()
        {
            client = new MongoClient(App.MongoServerUrl);
            server = client.GetServer();
            database = server.GetDatabase(App.MongoDB);
            collection = database.GetCollection(App.MongoCollection);
        }
        public bool InsertObjectToMongoDB(ImageInformation imageInfo)
        {
            bool isSuccess = false;
            try
            {
                Logger.Info("Initaited MongoRepository---> InsertObjectToMongoDB");
                if (string.IsNullOrEmpty(App.MongoServerUrl) || string.IsNullOrEmpty(App.MongoDB) || string.IsNullOrEmpty(App.MongoCollection))
                {
                    //this.exception =App.CreateException(HttpStatusCode.InternalServerError, "Unable to connect mongodb. Please check MongoDB server Url in configuration.");
                }
                else
                {
                    using (Stream memoryStream = new MemoryStream(imageInfo.Content))
                    {
                        //Stream memoryStream = new MemoryStream(imageInfo.Content);
                        MongoGridFSFileInfo gfsi = database.GridFS.Upload(memoryStream, imageInfo.Name);
                        BsonDocument photoMetadata = new BsonDocument
                                         { { "FileName", imageInfo.Name },
                                         { "Type", imageInfo.ContentType},
                                         { "Extension", imageInfo.ContentExtension},
                                         { "UniqueID", gfsi.Id.AsObjectId.ToString()}
                                      };
                        database.GridFS.SetMetadata(gfsi, photoMetadata);
                        imageInfo._id = gfsi.Id.AsObjectId;
                        imageInfo.IMGID = gfsi.Id.AsObjectId.ToString();
                        imageInfo.Content = null;
                        collection.Insert(imageInfo);
                        imageInfo.IMGID = imageInfo.IMGID;
                        isSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Exception while Inserting to Mongo: " + ex.Message);
                //this.exception = App.CreateException(HttpStatusCode.InternalServerError, ex.Message);
            }
            return isSuccess;
        }
    }
}