﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;

namespace ApolloWebsite.Logic
{
    public class ImageInformation
    {
        public ObjectId _id { get; set; }

        public string IMGID { get; set; }

        public string Name { get; set; }

        public string ContentType { get; set; }

        public string ContentExtension { get; set; }

        public byte[] Content { get; set; }
        public DateTime LastUpdateTime { get; set; }
    }
}