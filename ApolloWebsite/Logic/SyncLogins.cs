﻿using ApolloDB.DBFolder;
using CMCAPI.Client.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;

namespace ApolloWebsite.Logic
{
    public class SyncLogins
    {
        private static HttpClient client = null;
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public static HttpClient InitiateHttp(string userName,string password)
        {
            string frameURI = ConfigurationManager.AppSettings["cmcapi"];
            client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            string authentication = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(userName + ":" + password));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authentication);
            Uri baseAddress = new Uri(frameURI);
            client.BaseAddress = baseAddress;
            return client;
        }
        public bool InitiateSync(long id,string loginType)
        {
            client.DefaultRequestHeaders.Remove("adminType");
            client.DefaultRequestHeaders.Add("adminType", loginType);
            switch (loginType)
            {
                case "A":
                    AdminDTO adminDto= new AdminDTO();
                    adminDto = GetData<AdminDTO>("api/RegisteredApp/GetRegisteredApp", adminDto);
                    return adminDto == null ? false : UpdateData<AdminDTO>(adminDto, loginType);
                case "D":
                    DepartmentDTO deptDto = new DepartmentDTO();
                    deptDto = GetData<DepartmentDTO>("api/Department/GetDepartment", deptDto);
                    return deptDto == null ? false : UpdateData<DepartmentDTO>(deptDto, loginType);
                case "S":
                    StoreDTO storeDto = new StoreDTO();
                    storeDto = GetData<StoreDTO>("api/Store/GetStore", storeDto);
                    return storeDto == null ? false : UpdateData<StoreDTO>(storeDto, loginType);
                default:
                    return false;
            }
        }

        private bool UpdateData<T>(T data, string loginType)
        {
            switch (loginType)
            {
                case "A":
                    AdminDTO adminData = data as AdminDTO;
                    using (ApolloDBContext dbContext = new ApolloDBContext())
                    {
                        List<long> lstDepts = adminData.LstDepts.Select(c => c.SNO).ToList();
                        List<long> newDepartments = lstDepts.Where(c => !dbContext.DEPARTMENTID.Select(d => d.ID).Contains(c)).ToList();
                        newDepartments.ForEach(c =>
                            {
                                ApolloDB.Models.DepartmentID department = new ApolloDB.Models.DepartmentID();
                              
                                department.ID = c;
                                dbContext.DEPARTMENTID.Add(department);
                            });
                        adminData.LstDepts.ForEach(c =>
                        {
                            List<StoreDTO> lstStoreDto= GetStoresUnderDepartment(c.SNO);
                            List<long> lstStores = lstStoreDto.Select(d => d.SNO).ToList();
                            lstStores = lstStores.Where(d => !dbContext.LOCATIONID.Select(f => f.ID).Contains(d)).ToList();
                            lstStores.ForEach(d =>
                            {
                                ApolloDB.Models.LocationID location = new ApolloDB.Models.LocationID();
                                location.ID = d;
                                dbContext.LOCATIONID.Add(location);
                            });
                        });
                        dbContext.SaveChanges();
                        return true;
                    }
                case "D":
                    DepartmentDTO deptData = data as DepartmentDTO;
                    using (ApolloDBContext dbContext = new ApolloDBContext())
                    {
                        if (!dbContext.DEPARTMENTID.Any(c => c.ID.Equals(deptData.SNO)))
                        {
                            ApolloDB.Models.DepartmentID department = new ApolloDB.Models.DepartmentID();
                            department.ID = deptData.SNO;
                            dbContext.DEPARTMENTID.Add(department);
                        }
                        long deptid = deptData.SNO;
                        List<StoreDTO> lstStoreDto = GetStoresUnderDepartment(deptid);
                        List<long> lstStores = lstStoreDto.Select(c => c.SNO).ToList();
                        List<long> newStores = lstStores.Where(c => !dbContext.DEPARTMENTID.Select(d => d.ID).Contains(c)).ToList();
                        newStores.ForEach(c =>
                            {
                                ApolloDB.Models.LocationID location = new ApolloDB.Models.LocationID();
                                location.ID = c;
                                dbContext.LOCATIONID.Add(location);
                            });
                        dbContext.SaveChanges();
                        return true;
                    }
                case "S":
                    StoreDTO storeData = data as StoreDTO;
                    using(ApolloDBContext dbContext=new ApolloDBContext())
                    {
                        if(!dbContext.LOCATIONID.Any(c=>c.ID.Equals(storeData.SNO)))
                        {
                            ApolloDB.Models.LocationID location = new ApolloDB.Models.LocationID();
                            location.ID = storeData.SNO;
                            dbContext.LOCATIONID.Add(location);
                        }
                        dbContext.SaveChanges();
                        return true;
                    }
                default:
                    break;
            }
            return false;
        }

        public List<StoreDTO> GetStoresUnderDepartment(long deptid)
        {
            List<StoreDTO> lstStoreDto = new List<StoreDTO>();
            lstStoreDto = GetData<List<StoreDTO>>("api/Store/GetStores/" + deptid, lstStoreDto);
            return lstStoreDto;
        }
        public StoreDTO GetStoreById()
        {
            StoreDTO storeDto = new StoreDTO();
            storeDto = GetData<StoreDTO>("api/Store/GetStore", storeDto);
            return storeDto;
        }
        public List<DepartmentDTO> GetDepartments()
        {
            List<DepartmentDTO> lstDepartmentDto = new List<DepartmentDTO>();
            lstDepartmentDto = GetData<List<DepartmentDTO>>("api/Department/GetDepartments", lstDepartmentDto);
            return lstDepartmentDto;
        }
        public DepartmentDTO Getdepartment()
        {
            DepartmentDTO deptDto = new DepartmentDTO();
            deptDto = GetData<DepartmentDTO>("api/Department/GetDepartment", deptDto);
            return deptDto;
        }
        private T GetData<T>(string apiUrl, T Value)
        {
            Logger.Info("ApiUrl", apiUrl);
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsAsync<T>().Result;
            }
            else
            {
                Logger.Info("Error StatusCode:" + response.StatusCode.ToString());
                Logger.Info("Error Message:" + response.Content.ReadAsAsync<string>().Result);
            }
            return Activator.CreateInstance<T>();
        }
    }
}