﻿using ApolloDB.DBFolder;
using ApolloDB.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApolloWebsite.FeedbackQuestLogic
{
    public class FeedBackQuestionInitScript
    {
        public List<FeedbackCategory> LstFeedbackQuest { get; set; }
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public FeedBackQuestionInitScript()
        {
            LstFeedbackQuest = LoadQuestions();
        }
        private List<FeedbackCategory> LoadQuestions()
        {
            List<FeedbackCategory> lstCategories = new List<FeedbackCategory>();

            #region Empty Category
            FeedbackCategory emptycategory = new FeedbackCategory() { Name = PredefinedCategory.Empty };
            emptycategory.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.FirstQuest });
            lstCategories.Add(emptycategory);
            #endregion

            #region OverAll Experience Category
            FeedbackCategory overallExp = new FeedbackCategory() { Name = PredefinedCategory.OverAllExp };
            overallExp.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.SecondQuest });
            overallExp.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.ThirdQuest });
            overallExp.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.FourthQuest });
            overallExp.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.FifthQuest });
            overallExp.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.SixthQuest });
            lstCategories.Add(overallExp);
            #endregion

            #region Appointment Experience
            FeedbackCategory appointmentExp = new FeedbackCategory() { Name = PredefinedCategory.AppointmentExp };
            appointmentExp.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.SeventhQuest });
            appointmentExp.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.EigthQuest });
            appointmentExp.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.NinthQuest });
            lstCategories.Add(appointmentExp);
            #endregion

            #region RegBill Experience
            FeedbackCategory regBillExp = new FeedbackCategory() { Name = PredefinedCategory.RegBillExp };
            regBillExp.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.TenthQuest });
            regBillExp.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.EleventhQuest });
            regBillExp.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.TwelveQuest });
            lstCategories.Add(regBillExp);
            #endregion

            #region Lobby Experience
            FeedbackCategory lobbyExp = new FeedbackCategory() { Name = PredefinedCategory.LobbyExp };
            lobbyExp.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.ThirteenQuest });
            lobbyExp.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.FourteenQuest });
            lobbyExp.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.FifteenQuest });
            lobbyExp.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.SixteenQuest });
            lstCategories.Add(lobbyExp);
            #endregion

            #region Diagnostic And Ancillary Experience
            FeedbackCategory diagnosticAncillaryExp = new FeedbackCategory() { Name = PredefinedCategory.Diagnostic_AncillaryServices };
            diagnosticAncillaryExp.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.SeventeenQuest });
            diagnosticAncillaryExp.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.EighteenQuest });
            diagnosticAncillaryExp.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.NinteenQuest });
            diagnosticAncillaryExp.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.TwentyQuest });
            diagnosticAncillaryExp.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.TwentyOneQuest });
            diagnosticAncillaryExp.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.TwentyTwoQuest });
            lstCategories.Add(diagnosticAncillaryExp);
            #endregion

            #region Food And Beverage Experience
            FeedbackCategory foodBeverageExp = new FeedbackCategory() { Name = PredefinedCategory.Food_BeverageExp };
            foodBeverageExp.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.TwentyThreeQuest });
            lstCategories.Add(foodBeverageExp);
            #endregion

            #region Report Collection
            FeedbackCategory reportCollection = new FeedbackCategory() { Name = PredefinedCategory.ReportCollection };
            reportCollection.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.TwentyFourQuest });
            lstCategories.Add(reportCollection);
            #endregion

            #region Consultation Experience
            FeedbackCategory consultationExp = new FeedbackCategory() { Name = PredefinedCategory.ConsultationExp };
            consultationExp.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.TwentyFiveQuest });
            consultationExp.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.TwentySixQuest });
            consultationExp.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.TwentySevenQuest });
            lstCategories.Add(consultationExp);
            #endregion

            return lstCategories;
        }

        public void RunInitScript()
        {
            Initiate();
        }
        private void Initiate()
        {
            using (ApolloDBContext dbContext = new ApolloDBContext())
            {
                long maxCateg = dbContext.FEEDBACKCATEGORY.ToList().Count > 0 ? dbContext.FEEDBACKCATEGORY.Max(e => e.ID) + 1 : 1;
                long maxFeedbackQuest = dbContext.FEEDBACKQUESTIONS.ToList().Count > 0 ? dbContext.FEEDBACKQUESTIONS.Max(e => e.ID) + 1 : 1;

                try
                {
                    LstFeedbackQuest.ForEach(c =>
                    {
                        ApolloDB.Models.FeedbackCategory masterCateg = dbContext.FEEDBACKCATEGORY.FirstOrDefault(d => d.NAME.Equals(c.Name, StringComparison.InvariantCultureIgnoreCase));
                        if (masterCateg == null)
                        {
                            masterCateg = PrepareFeedbackCategory(maxCateg, c);
                            maxCateg++;
                            dbContext.FEEDBACKCATEGORY.Add(masterCateg);
                        }

                        c.LstFeedbackQuests.ForEach(d =>
                        {
                            if (!dbContext.FEEDBACKQUESTIONS.Any(e => e.QUESTION.Equals(d.Name, StringComparison.InvariantCultureIgnoreCase)))
                            {
                                ApolloDB.Models.FeedBackQuestions question = PrepareFeedbackQuestion(maxFeedbackQuest, d);
                                maxFeedbackQuest++;
                                if (masterCateg.LSTFEEDBACKQUESTIONS == null)
                                {
                                    masterCateg.LSTFEEDBACKQUESTIONS = new List<ApolloDB.Models.FeedBackQuestions>();
                                }
                                masterCateg.LSTFEEDBACKQUESTIONS.Add(question);
                            }
                        });
                        dbContext.SaveChanges();
                    });
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message, ex);
                }
            }
        }
        private FeedBackQuestions PrepareFeedbackQuestion(long maxFeedbackQuest, FeedbackQuest d)
        {
            FeedBackQuestions question = new FeedBackQuestions();
            question.QUESTION = d.Name;
            question.DESCRIPTION = string.Empty;
            question.ID = maxFeedbackQuest;
            question.ISACTIVE = true;
            question.LASTUPDATEDTIME = DateTime.Now;
            question.VERSION = 1;
            return question;
        }

        private ApolloDB.Models.FeedbackCategory PrepareFeedbackCategory(long maxCateg, FeedbackCategory c)
        {
            ApolloDB.Models.FeedbackCategory category = new ApolloDB.Models.FeedbackCategory();
            category.NAME = c.Name;
            category.ID = maxCateg;
            category.ISACTIVE = true;
            category.LASTUPDATEDTIME = DateTime.Now;
            category.VERSION = 1;
            return category;
        }
    }
}