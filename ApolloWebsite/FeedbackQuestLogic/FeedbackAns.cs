﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApolloWebsite.FeedbackQuestLogic
{
    public class FeedbackAns
    {
        public FeedbackAns(int questionNbr)
        {
            this.QuestionNumber = questionNbr;
            this.Rating = 0;
        }

        public int QuestionNumber { get; set; }
        public int Rating { get; set; }
    }
}