﻿using ApolloDB.DBFolder;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Reflection.Emit;
using System.Web;

namespace ApolloWebsite
{
    public class App
    {
        public ApolloDBContext ApolloDBcontext = new ApolloDBContext();
        public static string MongoServerUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["mongourl"];
            }
        }
        public static string MongoDB
        {
            get
            {
                return ConfigurationManager.AppSettings["mongoDB"];
            }
        }
        public static string MongoCollection
        {
            get
            {
                return ConfigurationManager.AppSettings["mongoCollectionName"];
            }
        }
    }
}