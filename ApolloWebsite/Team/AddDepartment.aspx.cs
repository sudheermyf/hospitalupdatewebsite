﻿using ApolloDB.DBFolder;
using ApolloDB.Models;
using ApolloWebsite.Logic;
using CMCAPI.Client.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ApolloWebsite.Team
{
    public partial class AddDepartment : System.Web.UI.Page
    {
        object lockTarget = new object();
        HttpClient client;
        string username = string.Empty, password = string.Empty;
        protected string adminType = string.Empty;
        long adminid = 0;
        SyncLogins syncLogin = new SyncLogins();
        private Logger Logger = LogManager.GetCurrentClassLogger();
        protected void Page_Load(object sender, EventArgs e)
        {
            DepartmentControl.bindRelatedData -= DepartmentControl_bindRelatedData;
            DepartmentControl.bindRelatedData += DepartmentControl_bindRelatedData;
            if (Session["adminloginid"] != null)
            {
                adminType = Convert.ToString(Session["adminType"], CultureInfo.CurrentCulture);
                if (Int64.TryParse(Convert.ToString(Session["adminloginid"], CultureInfo.CurrentCulture), out adminid) && adminid > 0)
                {

                }
                else
                {
                    Response.Redirect("../Default.aspx");
                }
                username = Convert.ToString(Session["adminusername"], CultureInfo.CurrentCulture);
                password = Convert.ToString(Session["adminpassword"], CultureInfo.CurrentCulture);
            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }
            client = Logic.SyncLogins.InitiateHttp(username, password);
            client.DefaultRequestHeaders.Add("AdminType", adminType);
            if (!IsPostBack)
            {
                BindDepartments();
            }
        }

        private void DepartmentControl_bindRelatedData(string abcd)
        {
            long id = 0;
            if (Int64.TryParse(Convert.ToString(lblID.Text,CultureInfo.CurrentCulture), out id) && id > 0)
            {
                GetEditedData(id);
            }
        }

        private void GetEditedData(long id)
        {
            try
            {
                lock (lockTarget)
                {
                    Logger.Info("Starts When Edit Button Clicks in AddDepartment.aspx");
                    List<StoreDTO> lstStores = new List<StoreDTO>();
                    using (ApolloDBContext dbContext = new ApolloDBContext())
                    {
                        ApolloDB.Models.Department dept = dbContext.DEPARTMENTS.FirstOrDefault(c => c.ID.Equals(id));
                        if (dept != null)
                        {
                            lblID.Text = Convert.ToString(id, CultureInfo.CurrentCulture);
                            ListBox ddlDepartments = (ListBox)DepartmentControl.FindControl("ddlDepartments");
                            GroupDropDownList.GroupDropDownList ddlStores = (GroupDropDownList.GroupDropDownList)DepartmentControl.FindControl("ddlStores");
                            Label lblDepartments = (Label)DepartmentControl.FindControl("lblDepartments");
                            Label lblStores = (Label)DepartmentControl.FindControl("lblStores");
                            List<long> lstDepartments2 = new List<long>();
                            lstDepartments2 = Array.ConvertAll(ddlDepartments.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                            switch (adminType)
                            {
                                case "A":
                                    lblDepartments.Visible = true;
                                    lblStores.Visible = true;
                                    ddlDepartments.Visible = true;
                                    ddlStores.Visible = true;
                                    if (lstDepartments2 != null)
                                    {
                                        lstDepartments2.ForEach(c =>
                                        {
                                            ddlDepartments.Items.FindByValue(Convert.ToString(c,CultureInfo.CurrentCulture)).Selected = true;
                                            lstStores.AddRange(syncLogin.GetStoresUnderDepartment(c));
                                        });
                                        ddlStores.Items.Clear();
                                        ddlStores.DataSource = lstStores;
                                        ddlStores.DataGroupField = "DeptName";
                                        ddlStores.DataTextField = "StoreName";
                                        ddlStores.DataValueField = "SNO";
                                        ddlStores.DataBind();
                                        if (lstStores != null && lstStores.Count > 0)
                                        {
                                            lstStores.ForEach(t =>
                                            {
                                                if (dept.LstLocations.Contains(dbContext.LOCATIONID.FirstOrDefault(c => c.ID.Equals(t.SNO))))
                                                {
                                                    ddlStores.Items.FindByValue(Convert.ToString(t.SNO,CultureInfo.CurrentCulture)).Selected = true;
                                                }
                                            });
                                        }
                                    }
                                    break;
                                case "D":
                                    lblDepartments.Visible = false;
                                    ddlDepartments.Visible = false;
                                    ddlStores.Visible = true;
                                    lblStores.Visible = true;
                                    List<DepartmentID> lstDepartments = dept.LstDepartments.Where(c => c.ID.Equals(adminid)).ToList();
                                    if (lstDepartments != null)
                                    {
                                        lstDepartments.ForEach(c =>
                                        {
                                            lstStores.AddRange(syncLogin.GetStoresUnderDepartment(c.ID));
                                        });
                                        ddlStores.Items.Clear();
                                        ddlStores.DataSource = lstStores;
                                        ddlStores.DataGroupField = "DeptName";
                                        ddlStores.DataTextField = "StoreName";
                                        ddlStores.DataValueField = "SNO";
                                        ddlStores.DataBind();
                                        if (lstStores != null && lstStores.Count > 0)
                                        {
                                            lstStores.ForEach(t =>
                                            {
                                                if (dept.LstLocations.Contains(dbContext.LOCATIONID.FirstOrDefault(c => c.ID.Equals(t.SNO))))
                                                {
                                                    ddlStores.Items.FindByValue(Convert.ToString(t.SNO,CultureInfo.CurrentCulture)).Selected = true;
                                                }
                                            });
                                        }
                                    }
                                    break;
                                case "S":
                                    lblStores.Visible = false;
                                    ddlStores.Visible = false;
                                    lblDepartments.Visible = false;
                                    ddlDepartments.Visible = false;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    Logger.Info("Ends When Edit Button Clicks in AddDepartment.aspx");
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }
        private void BindDepartments()
        {
            gvViewDepartments.DataSource = null;
            gvViewDepartments.DataBind();
            Logger.Info("Starts BindDepartments() in AddDepartment.aspx");
            using (ApolloDBContext dbContext = new ApolloDBContext())
            {
                if (dbContext.DEPARTMENTS.Count() > 0)
                {
                    switch (adminType)
                    {
                        case "A":
                             gvViewDepartments.DataSource = dbContext.DEPARTMENTS.Where(d=>d.RegAppID.Equals(adminid)).OrderByDescending(c => c.LastUpdatedTime).ToList();
                             gvViewDepartments.DataBind();
                            break;
                        case "D":
                             gvViewDepartments.DataSource = dbContext.DEPARTMENTS.Where(d=>d.LstDepartments.Any(g=>g.ID.Equals(adminid))).OrderByDescending(c => c.LastUpdatedTime).ToList();
                             gvViewDepartments.DataBind();
                            break;
                        case "S":
                              gvViewDepartments.DataSource = dbContext.DEPARTMENTS.Where(d=>d.LstLocations.Any(g=>g.ID.Equals(adminid))).OrderByDescending(c => c.LastUpdatedTime).ToList();
                              gvViewDepartments.DataBind();
                            break;
                        default:
                            break;
                    }
                   
                }
            }
            Logger.Info("Ends BindDepartments() in AddDepartment.aspx");
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            if (btnClear.Text.Equals(MainResource.Clear))
            {
                ClearAll();
            }
            else if (btnClear.Text.Equals(MainResource.Reset))
            {
                long id = 0;
                if (Int64.TryParse(lblID.Text, out id) && id > 0)
                {
                    GetEditData(id);
                }
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            lock (lockTarget)
            {

                string path = MainResource.ServerPath;
                string folder = path + @"\DepartmentImages";
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
                if (btnSubmit.Text.Equals(MainResource.Submit))
                {
                    try
                    {
                        Logger.Info("Starts When Submit() button Clicks() in AddDepartment.aspx");
                        using (ApolloDBContext dbContext = new ApolloDBContext())
                        {
                            if (CheckNameExists())
                            {
                                ApolloDB.Models.Department dept = new ApolloDB.Models.Department();
                                dept.ID = dbContext.DEPARTMENTS.ToList().Count > 0 ? dbContext.DEPARTMENTS.Max(c => c.ID) + 1 : 1;
                                dept.Description = txtDeptDesc.Text;
                                dept.Name = txtDepartmentName.Text.ToUpper(CultureInfo.CurrentCulture);
                                dept.Priority = 0;
                                dept.LastUpdatedTime = DateTime.Now;
                                foreach (HttpPostedFile Img in fuDepartmentImage.PostedFiles)
                                {
                                    lock (lockTarget)
                                    {
                                        string fileName = Path.GetFileName(Img.FileName);
                                        string strRandomNumber = new Random().Next(1000, 100000).ToString() + txtDepartmentName.Text.Trim().ToUpper() + Path.GetExtension(fileName);
                                        Img.SaveAs(Path.Combine(folder, strRandomNumber));
                                        ImageInformation imgInfo = new ImageInformation();
                                        imgInfo.Name = strRandomNumber;
                                        imgInfo.Content = File.ReadAllBytes(folder + "\\" + strRandomNumber);
                                        imgInfo.ContentType = enums.Image.ToString();
                                        imgInfo.ContentExtension = Path.GetExtension(Img.FileName);
                                        dept.ImageExtension = Path.GetExtension(Img.FileName);
                                        MongoOperation mongo = new MongoOperation();
                                        if (mongo.InsertObjectToMongoDB(imgInfo))
                                        {
                                            dept.ImageID = imgInfo.IMGID;
                                        }
                                        File.Delete(folder + "\\" + strRandomNumber);
                                    }
                                }
                                if (chkIsActive.Checked)
                                {
                                    dept.IsActive = true;
                                }
                                List<long> lstDepartments = new List<long>();
                                List<long> lstStores = new List<long>();
                                ListBox ddlDepartments = (ListBox)DepartmentControl.FindControl("ddlDepartments");
                                GroupDropDownList.GroupDropDownList ddlStores = (GroupDropDownList.GroupDropDownList)DepartmentControl.FindControl("ddlStores");
                                switch (adminType)
                                {
                                    case "A":
                                        lstDepartments = Array.ConvertAll(ddlDepartments.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                                        dept.LstDepartments = dbContext.DEPARTMENTID.Where(c => lstDepartments.Contains(c.ID)).ToList();
                                        lstStores = Array.ConvertAll(ddlStores.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                                        dept.LstLocations = dbContext.LOCATIONID.Where(c => lstStores.Contains(c.ID)).ToList();
                                        dept.RegAppID = adminid;
                                        break;
                                    case "D":
                                        dept.LstDepartments = dbContext.DEPARTMENTID.Where(c => c.ID.Equals(adminid)).ToList();
                                        lstStores = Array.ConvertAll(ddlStores.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                                        dept.LstLocations = dbContext.LOCATIONID.Where(c => lstStores.Contains(c.ID)).ToList();
                                        DepartmentDTO dept1 = syncLogin.Getdepartment();
                                        dept.RegAppID = dept1.RegAppID;
                                        break;
                                    case "S":
                                        dept.LstLocations = dbContext.LOCATIONID.Where(c => c.ID.Equals(adminid)).ToList();
                                        StoreDTO store = new StoreDTO();
                                        store = syncLogin.GetStoreById();
                                        dept.LstDepartments = dbContext.DEPARTMENTID.Where(c => c.ID.Equals(store.DeptID)).ToList();
                                        dept.RegAppID = store.RegAppID;
                                        break;
                                    default:
                                        break;
                                }
                                dbContext.DEPARTMENTS.Add(dept);
                                dbContext.SaveChanges();
                                ClientScript.RegisterStartupScript(GetType(), "notifier15", "successAlert('Department Added Successfully');", true);
                                ClearAll();
                                BindDepartments();

                                Logger.Info("Ends When Submit() button Clicks() in AddDepartment.aspx");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex, ex.Message);
                    }

                }
                else if (btnSubmit.Text.Equals(MainResource.Update))
                {
                    try
                    {
                        Logger.Info("Starts When Update() button Clicks() in AddDepartment.aspx");
                        long id = 0;
                        if (Int64.TryParse(lblID.Text, out id) && id > 0)
                        {
                            using (ApolloDBContext dbContext = new ApolloDBContext())
                            {
                                ApolloDB.Models.Department dept = dbContext.DEPARTMENTS.FirstOrDefault(c => c.ID.Equals(id));
                                if (!dept.Name.Equals(txtDepartmentName.Text))
                                {
                                    CheckNameExists();
                                }
                                dept.Name = txtDepartmentName.Text.ToUpper(CultureInfo.CurrentCulture);
                                dept.Priority = 0;
                                dept.Description = txtDeptDesc.Text;
                                dept.LastUpdatedTime = DateTime.Now;
                                if (fuDepartmentImage.HasFile)
                                {
                                    foreach (HttpPostedFile Img in fuDepartmentImage.PostedFiles)
                                    {
                                        lock (lockTarget)
                                        {
                                            string fileName = Path.GetFileName(Img.FileName);
                                            string strRandomNumber = new Random().Next(1000, 100000).ToString() + txtDepartmentName.Text.Trim().ToUpper() + Path.GetExtension(fileName);
                                            Img.SaveAs(Path.Combine(folder, strRandomNumber));
                                            ImageInformation imgInfo = new ImageInformation();
                                            imgInfo.Name = strRandomNumber;
                                            imgInfo.Content = File.ReadAllBytes(folder + "\\" + strRandomNumber);
                                            imgInfo.ContentType = enums.Image.ToString();
                                            imgInfo.ContentExtension = Path.GetExtension(Img.FileName);
                                            dept.ImageExtension = Path.GetExtension(Img.FileName);
                                            MongoOperation mongo = new MongoOperation();
                                            if (mongo.InsertObjectToMongoDB(imgInfo))
                                            {
                                                dept.ImageID = imgInfo.IMGID;
                                            }
                                            File.Delete(folder + "\\" + strRandomNumber);
                                        }
                                    }
                                }
                                if (chkIsActive.Checked)
                                {
                                    dept.IsActive = true;
                                }
                                else
                                {
                                    dept.IsActive = false;
                                }
                                List<long> lstDepts = new List<long>();
                                List<long> lstStores = new List<long>();
                                ListBox ddlDepartments = (ListBox)DepartmentControl.FindControl("ddlDepartments");
                                GroupDropDownList.GroupDropDownList ddlStores = (GroupDropDownList.GroupDropDownList)DepartmentControl.FindControl("ddlStores");

                                switch (adminType)
                                {
                                    case "A":
                                        #region Department operations
                                        lstDepts = Array.ConvertAll(ddlDepartments.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                                        List<long> RemoveLstDepartments = dept.LstDepartments.Where(c => !lstDepts.Contains(c.ID)).Select(c => c.ID).ToList();
                                        if (RemoveLstDepartments != null && RemoveLstDepartments.Count > 0)
                                        {
                                            RemoveLstDepartments.ForEach(d =>
                                            {
                                                DepartmentID RemoveDeptDto = dept.LstDepartments.FirstOrDefault(c => c.ID.Equals(d));
                                                if (RemoveDeptDto != null)
                                                {
                                                    dept.LstDepartments.Remove(RemoveDeptDto);
                                                }
                                            });
                                        }
                                        lock (lockTarget)
                                        {
                                            List<DepartmentID> dpt = dbContext.DEPARTMENTID.Where(c => lstDepts.Contains(c.ID)).ToList();
                                            dept.LstDepartments = dpt;
                                        }
                                        #endregion
                                        #region Store Operation
                                        List<long> RemoveLstStores = dept.LstLocations.Where(c => !lstStores.Contains(c.ID)).Select(c => c.ID).ToList();
                                        RemoveLstStores.ToList().ForEach(d =>
                                        {
                                            LocationID RemovelocationDto = dept.LstLocations.ToList().Where(c => c.ID.Equals(d)).FirstOrDefault();
                                            dept.LstLocations.Remove(RemovelocationDto);
                                        });
                                        lstStores = Array.ConvertAll(ddlStores.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                                        lock (lockTarget)
                                        {
                                            List<LocationID> location = dbContext.LOCATIONID.Where(c => lstStores.Contains(c.ID)).ToList();
                                            dept.LstLocations = location;
                                        }
                                        #endregion
                                        break;
                                    case "D":
                                        #region Store Operation
                                        List<long> RemoveLstStores1 = dept.LstLocations.Where(c => !lstStores.Contains(c.ID)).Select(c => c.ID).ToList();
                                        RemoveLstStores1.ToList().ForEach(d =>
                                        {
                                            LocationID RemovelocationDto = dept.LstLocations.ToList().Where(c => c.ID.Equals(d)).FirstOrDefault();
                                            dept.LstLocations.Remove(RemovelocationDto);
                                        });
                                        lstStores = Array.ConvertAll(ddlStores.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                                        lock (lockTarget)
                                        {
                                            List<LocationID> location = dbContext.LOCATIONID.Where(c => lstStores.Contains(c.ID)).ToList();
                                            dept.LstLocations = location;
                                        }
                                        #endregion
                                        break;
                                    case "S":
                                        break;
                                    default:
                                        break;
                                }
                                dbContext.SaveChanges();
                                ClientScript.RegisterStartupScript(GetType(), "notifier15", "successAlert('Department Updated Successfully');", true);
                                BindDepartments();
                                CancelUpdate();
                                Logger.Info("Ends When Update() button Clicks() in AddDepartment.aspx");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex, ex.Message);
                    }
                }
            }
        }
        private bool CheckNameExists()
        {
            string deptName = txtDepartmentName.Text;
            using (ApolloDBContext dbContext = new ApolloDBContext())
            {
                ApolloDB.Models.Department dept = dbContext.DEPARTMENTS.FirstOrDefault(c => c.Name.Equals(deptName));
                if (btnSubmit.Text.Equals(MainResource.Submit))
                {
                    if (dept == null)
                    {
                        deptError.InnerText = string.Empty;
                        return true;
                    }
                }
                if (btnSubmit.Text.Equals(MainResource.Update))
                {
                    long id = 0;
                    if (Int64.TryParse(lblID.Text, out id) && id > 0)
                    {
                        if (dept == null || dept.ID.Equals(id))
                        {
                            deptError.InnerText = string.Empty;
                            return true;
                        }
                    }
                }
            }
            deptError.InnerText = "Department name already exists";
            ClientScript.RegisterStartupScript(GetType(), "notifier16", "errorAlert('Department name already exists');", true);
            return false;
        }
        protected void btnUpdateCancel_Click(object sender, EventArgs e)
        {
            CancelUpdate();
        }
        private void CancelUpdate()
        {
            btnSubmit.Text = MainResource.Submit;
            btnClear.Text = MainResource.Clear;
            lblPageHeading.Text = MainResource.Department;
            btnUpdateCancel.Visible = false;
            lblID.Text = string.Empty;
            deptError.InnerText = string.Empty;
            imgDept.Visible = false;
            ClearAll();
        }

        private void ClearAll()
        {
            txtDeptDesc.Text = string.Empty;
            txtDepartmentName.Text = string.Empty;
            chkIsActive.Checked = true;
            deptError.InnerText = string.Empty;
            ListBox ddlDepartments = (ListBox)DepartmentControl.FindControl("ddlDepartments");
            GroupDropDownList.GroupDropDownList ddlStores = (GroupDropDownList.GroupDropDownList)DepartmentControl.FindControl("ddlStores");
            ddlDepartments.ClearSelection();
            ddlStores.ClearSelection();
            if (adminType.Equals("A"))
            {
                ddlStores.Items.Clear();
            }
           
        }
        protected void gvViewDepartments_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            long id = 0;
            if (Int64.TryParse(e.CommandArgument.ToString(), out id) && id > 0)
            {
                if (e.CommandName.Equals(MainResource.Edit))
                {
                    GetEditData(id);
                }
                else if (e.CommandName.Equals(MainResource.Delete))
                {
                    try
                    {
                        lock (lockTarget)
                        {
                            Logger.Info("Starts Delete Button in AddDepartment.aspx");
                            using (ApolloDBContext dbContext = new ApolloDBContext())
                            {
                                ApolloDB.Models.Department dept = dbContext.DEPARTMENTS.FirstOrDefault(c => c.ID.Equals(id));
                                if (dept != null && dept.LstDoctors != null && dept.LstDoctors.Count > 0)
                                {
                                    ClientScript.RegisterStartupScript(GetType(), "notifier15", "errorAlert('Please delete Doctors under this Department.');", true);
                                }
                                else
                                {
                                    dbContext.DEPARTMENTS.Remove(dept);
                                    dbContext.SaveChanges();
                                    ClientScript.RegisterStartupScript(GetType(), "notifier15", "successAlert('Deleted Successfully');", true);
                                    CancelUpdate();
                                }
                            }
                            BindDepartments();
                            Logger.Info("Ends Delete Button in AddDepartment.aspx");
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex, ex.Message);
                    }
                }
            }
        }

        private void GetEditData(long id)
        {
            try
            {
                lock (lockTarget)
                {
                    Logger.Info("Starts When Edit Button Clicks in AddDepartment.aspx");
                    List<StoreDTO> lstStores = new List<StoreDTO>();
                    using (ApolloDBContext dbContext = new ApolloDBContext())
                    {
                        ApolloDB.Models.Department dept = dbContext.DEPARTMENTS.FirstOrDefault(c => c.ID.Equals(id));
                        if (dept != null)
                        {
                            lblID.Text = Convert.ToString(id, CultureInfo.CurrentCulture);
                            txtDepartmentName.Text = dept.Name.ToUpper(CultureInfo.CurrentCulture);
                            txtDeptDesc.Text = dept.Description;
                            if (dept.IsActive)
                            {
                                chkIsActive.Checked = true;
                            }
                            else
                            {
                                chkIsActive.Checked = false;
                            }
                            imgDept.ImageUrl = ConfigurationManager.AppSettings["apiurl"] + "api/Image/" + dept.ImageID;
                            imgDept.Visible = true;
                            ListBox ddlDepartments = (ListBox)DepartmentControl.FindControl("ddlDepartments");
                            GroupDropDownList.GroupDropDownList ddlStores = (GroupDropDownList.GroupDropDownList)DepartmentControl.FindControl("ddlStores");
                            Label lblDepartments = (Label)DepartmentControl.FindControl("lblDepartments");
                            Label lblStores = (Label)DepartmentControl.FindControl("lblStores");
                            switch (adminType)
                            {
                                case "A":
                                    lblDepartments.Visible = true;
                                    lblStores.Visible = true;
                                    ddlDepartments.Visible = true;
                                    ddlStores.Visible = true;
                                    if (dept.LstDepartments != null)
                                    {
                                        dept.LstDepartments.ForEach(c =>
                                        {
                                            ddlDepartments.Items.FindByValue(Convert.ToString(c.ID,CultureInfo.CurrentCulture)).Selected = true;
                                            lstStores.AddRange(syncLogin.GetStoresUnderDepartment(c.ID));
                                        });
                                        ddlStores.Items.Clear();
                                        ddlStores.DataSource = lstStores;
                                        ddlStores.DataGroupField = "DeptName";
                                        ddlStores.DataTextField = "StoreName";
                                        ddlStores.DataValueField = "SNO";
                                        ddlStores.DataBind();
                                        if (lstStores != null && lstStores.Count > 0)
                                        {
                                            lstStores.ForEach(t =>
                                            {
                                                if (dept.LstLocations.Contains(dbContext.LOCATIONID.FirstOrDefault(c => c.ID.Equals(t.SNO))))
                                                {
                                                    ddlStores.Items.FindByValue(Convert.ToString(t.SNO,CultureInfo.CurrentCulture)).Selected = true;
                                                }
                                            });
                                        }
                                    }
                                    break;
                                case "D":
                                    lblDepartments.Visible = false;
                                    ddlDepartments.Visible = false;
                                    ddlStores.Visible = true;
                                    lblStores.Visible = true;
                                    List<DepartmentID> lstDepartments = dept.LstDepartments.Where(c => c.ID.Equals(adminid)).ToList();
                                    if (lstDepartments != null)
                                    {
                                        lstDepartments.ForEach(c =>
                                        {
                                            lstStores.AddRange(syncLogin.GetStoresUnderDepartment(c.ID));
                                        });
                                        ddlStores.Items.Clear();
                                        ddlStores.DataSource = lstStores;
                                        ddlStores.DataGroupField = "DeptName";
                                        ddlStores.DataTextField = "StoreName";
                                        ddlStores.DataValueField = "SNO";
                                        ddlStores.DataBind();
                                        if (lstStores != null && lstStores.Count > 0)
                                        {
                                            lstStores.ForEach(t =>
                                            {
                                                if (dept.LstLocations.Contains(dbContext.LOCATIONID.FirstOrDefault(c => c.ID.Equals(t.SNO))))
                                                {
                                                    ddlStores.Items.FindByValue(Convert.ToString(t.SNO,CultureInfo.CurrentCulture)).Selected = true;
                                                }
                                            });
                                        }
                                    }
                                    break;
                                case "S":
                                    lblStores.Visible = false;
                                    ddlStores.Visible = false;
                                    lblDepartments.Visible = false;
                                    ddlDepartments.Visible = false;
                                    break;
                                default:
                                    break;
                            }
                            btnUpdateCancel.Visible = true;
                            btnSubmit.Text = MainResource.Update;
                            btnClear.Text = MainResource.Reset;
                            lblPageHeading.Text = MainResource.EditDepartment;
                        }
                    }
                    Logger.Info("Ends When Edit Button Clicks in AddDepartment.aspx");
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }
        protected void gvViewDepartments_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblIsActive = ((Label)e.Row.FindControl("lblIsActive"));
                if (lblIsActive.Text == MainResource.False)
                {
                    lblIsActive.Attributes.Add("class", "badge bg-red");
                    lblIsActive.Attributes["data-toggle"] = "tooltip";
                    lblIsActive.Attributes["data-placement"] = "top";
                    lblIsActive.Attributes["title"] = "Please Edit to enable.";
                }
            }
        }

        protected void gvViewDepartments_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void gvViewDepartments_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }
    }
}