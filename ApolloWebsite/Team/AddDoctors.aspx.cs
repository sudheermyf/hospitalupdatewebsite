﻿using ApolloDB.DBFolder;
using ApolloDB.Models;
using ApolloWebsite.Logic;
using CMCAPI.Client.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ApolloWebsite.Team
{
    public partial class AddDoctors : System.Web.UI.Page
    {
        object lockTarget = new object();
        HttpClient client;
        string username = string.Empty, password = string.Empty;
        protected string adminType = string.Empty;
        long adminid = 0;
        SyncLogins syncLogin = new SyncLogins();
        private Logger Logger = LogManager.GetCurrentClassLogger();
        protected void Page_Load(object sender, EventArgs e)
        {
            DepartmentControl.bindRelatedData -= DepartmentControl_bindRelatedData;
            DepartmentControl.bindRelatedData += DepartmentControl_bindRelatedData;
            if (Session["adminloginid"] != null)
            {
                adminType = Convert.ToString(Session["adminType"], CultureInfo.CurrentCulture);
                if (Int64.TryParse(Convert.ToString(Session["adminloginid"], CultureInfo.CurrentCulture), out adminid) && adminid > 0)
                {

                }
                else
                {
                    Response.Redirect("../Default.aspx");
                }
                username = Convert.ToString(Session["adminusername"], CultureInfo.CurrentCulture);
                password = Convert.ToString(Session["adminpassword"], CultureInfo.CurrentCulture);
            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }
            client = Logic.SyncLogins.InitiateHttp(username, password);
            client.DefaultRequestHeaders.Add("AdminType", adminType);
            if(!IsPostBack)
            {
                LoadDepartments();
                BindDoctors();
            }
        }

        private void DepartmentControl_bindRelatedData(string abcd)
        {
            long id = 0;
            if (Int64.TryParse(Convert.ToString(lblID.Text, CultureInfo.CurrentCulture), out id) && id > 0)
            {
                GetEditedData(id);
            }
        }

        private void GetEditedData(long id)
        {
            try
            {
                using (ApolloDBContext dbContext = new ApolloDBContext())
                {
                    ListBox ddlDepartments = (ListBox)DepartmentControl.FindControl("ddlDepartments");
                    GroupDropDownList.GroupDropDownList ddlStores = (GroupDropDownList.GroupDropDownList)DepartmentControl.FindControl("ddlStores");
                    Label lblDepartments = (Label)DepartmentControl.FindControl("lblDepartments");
                    Label lblStores = (Label)DepartmentControl.FindControl("lblStores");
                    List<StoreDTO> lstStores = new List<StoreDTO>();
                    Logger.Info("Starts When Edit Button Clicks in AddDoctors.aspx");
                    ApolloDB.Models.Doctor doctor = dbContext.DOCTORS.FirstOrDefault(c => c.ID.Equals(id));
                    List<long> lstDepartments2 = new List<long>();
                    lstDepartments2 = Array.ConvertAll(ddlDepartments.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                    if (doctor != null)
                    {
                        lblID.Text = Convert.ToString(id,CultureInfo.CurrentCulture);
                        switch (adminType)
                        {
                            case "A":
                                lblDepartments.Visible = true;
                                lblStores.Visible = true;
                                ddlDepartments.Visible = true;
                                ddlStores.Visible = true;
                                if (lstDepartments2 != null)
                                {
                                    lstDepartments2.ForEach(c =>
                                    {
                                        ddlDepartments.Items.FindByValue(Convert.ToString(c, CultureInfo.CurrentCulture)).Selected = true;
                                        lstStores.AddRange(syncLogin.GetStoresUnderDepartment(c));
                                    });
                                    ddlStores.Items.Clear();
                                    ddlStores.DataSource = lstStores;
                                    ddlStores.DataGroupField = "DeptName";
                                    ddlStores.DataTextField = "StoreName";
                                    ddlStores.DataValueField = "SNO";
                                    ddlStores.DataBind();
                                    if (lstStores != null && lstStores.Count > 0)
                                    {
                                        lstStores.ForEach(t =>
                                        {
                                            if (doctor.LstLocations.Contains(dbContext.LOCATIONID.FirstOrDefault(c => c.ID.Equals(t.SNO))))
                                            {
                                                ddlStores.Items.FindByValue(Convert.ToString(t.SNO,CultureInfo.CurrentCulture)).Selected = true;
                                            }
                                        });
                                    }
                                }
                                break;
                            case "D":
                                lblDepartments.Visible = false;
                                ddlDepartments.Visible = false;
                                ddlStores.Visible = true;
                                lblStores.Visible = true;
                                List<DepartmentID> lstDepartments = doctor.LstDepartments.Where(c => c.ID.Equals(adminid)).ToList();
                                if (lstDepartments != null)
                                {
                                    lstDepartments.ForEach(c =>
                                    {
                                        lstStores.AddRange(syncLogin.GetStoresUnderDepartment(c.ID));
                                    });
                                    ddlStores.Items.Clear();
                                    ddlStores.DataSource = lstStores;
                                    ddlStores.DataGroupField = "DeptName";
                                    ddlStores.DataTextField = "StoreName";
                                    ddlStores.DataValueField = "SNO";
                                    ddlStores.DataBind();
                                    if (lstStores != null && lstStores.Count > 0)
                                    {
                                        lstStores.ForEach(t =>
                                        {
                                            if (doctor.LstLocations.Contains(dbContext.LOCATIONID.FirstOrDefault(c => c.ID.Equals(t.SNO))))
                                            {
                                                ddlStores.Items.FindByValue(Convert.ToString(t.SNO,CultureInfo.CurrentCulture)).Selected = true;
                                            }
                                        });
                                    }
                                }
                                break;
                            case "S":
                                lblStores.Visible = false;
                                ddlStores.Visible = false;
                                lblDepartments.Visible = false;
                                ddlDepartments.Visible = false;
                                break;
                            default:
                                break;
                        }
                    }
                }
                Logger.Info("Ends When Edit Button Clicks in AddDoctors.aspx");
            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(GetType(), "notifier3", "errorAlert('" + ex.Message + "');", true);
            }
        }
        private void LoadDepartments()
        {
            ddlViewDepartments.DataSource = null;
            ddlViewDepartments.DataBind();
            ddlDepartment.DataSource = null;
            ddlDepartment.DataBind();
            ddlViewDepartments.Items.Clear();
            ddlViewDepartments.Items.Insert(0, "Select");
            ddlDepartment.Items.Clear();
            ddlDepartment.Items.Insert(0, "Select");
            Logger.Info("Starts LoadDepartments() in AddDoctors.aspx");
            using (ApolloDBContext dbContext = new ApolloDBContext())
            {
                if (dbContext.DEPARTMENTS.ToList().Count > 0)
                {
                    switch (adminType)
                    {
                        case "A":
                            ddlDepartment.DataSource = dbContext.DEPARTMENTS.Where(d => d.RegAppID.Equals(adminid)).ToList();
                            ddlDepartment.DataBind();
                            ddlViewDepartments.DataSource = dbContext.DEPARTMENTS.Where(c => c.RegAppID.Equals(adminid) && c.LstDoctors.Count > 0).ToList();
                            ddlViewDepartments.DataBind();
                            break;
                        case "D":
                            ddlDepartment.DataSource = dbContext.DEPARTMENTS.Where(d => d.LstDepartments.Any(g=> g.ID.Equals(adminid))).ToList();
                            ddlDepartment.DataBind();
                            ddlViewDepartments.DataSource = dbContext.DEPARTMENTS.Where(c => c.LstDepartments.Any(d=>d.ID.Equals(adminid)) && c.LstDoctors.Count > 0).ToList();
                            ddlViewDepartments.DataBind();
                            break;
                        case "S":
                            ddlDepartment.DataSource = dbContext.DEPARTMENTS.Where(d => d.LstLocations.Any(g=> g.ID.Equals(adminid))).ToList();
                            ddlDepartment.DataBind();
                            ddlViewDepartments.DataSource = dbContext.DEPARTMENTS.Where(c => c.LstLocations.Any(d=>d.ID.Equals(adminid)) && c.LstDoctors.Count > 0).ToList();
                            ddlViewDepartments.DataBind();
                            break;
                        default:
                            break;
                    }
                }
            }
            Logger.Info("Ends LoadDepartments() in AddDoctors.aspx");
        }
        private void BindDoctors()
        {
            gvViewDoctors.DataSource = null;
            gvViewDoctors.DataBind();
            if (ddlViewDepartments.SelectedIndex > 0)
            {
                long deptId = 0;
                if (Int64.TryParse(ddlViewDepartments.SelectedValue, out deptId) && deptId > 0)
                {
                    using(ApolloDBContext dbContext=new ApolloDBContext())
                    {
                        ApolloDB.Models.Department dept = dbContext.DEPARTMENTS.FirstOrDefault(c => c.ID.Equals(deptId));
                        if (dept != null && dept.LstDoctors != null && dept.LstDoctors.Count > 0)
                        {
                            gvViewDoctors.DataSource = dept.LstDoctors;
                            gvViewDoctors.DataBind();
                        }
                    }
                }
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (CheckNameExists())
                {
                    lock (lockTarget)
                    {
                        Logger.Info("Starts When Submit Button Clicks in AddDoctors.aspx");
                        string path = MainResource.ServerPath;
                        string folder = path + @"\DoctorImages";
                        if (!Directory.Exists(folder))
                        {
                            Directory.CreateDirectory(folder);
                        }

                        if (btnSubmit.Text.Equals(MainResource.Submit))
                        {
                            try
                            {
                                using (ApolloDBContext dbContext = new ApolloDBContext())
                                {
                                    Doctor doctor = new Doctor();
                                    string strDoctorName = ddlDepartment.SelectedItem.Text;
                                    ApolloDB.Models.Department dept = dbContext.DEPARTMENTS.FirstOrDefault(c => c.Name.Equals(strDoctorName));
                                    doctor.Department = dept;
                                    doctor.ID = dbContext.DOCTORS.Count() > 0 ? dbContext.DOCTORS.Max(c => c.ID) + 1 : 1;
                                    doctor.Description = txtdesc.Text;
                                    foreach (HttpPostedFile Img in fuDoctorImage.PostedFiles)
                                    {
                                        lock (lockTarget)
                                        {
                                            string fileName = Path.GetFileName(Img.FileName);
                                            string strRandomNumber = new Random().Next(1000, 100000).ToString() + txtDoctorName.Text.Trim().ToUpper(CultureInfo.CurrentCulture) + Path.GetExtension(fileName);
                                            Img.SaveAs(Path.Combine(folder, strRandomNumber));
                                            ImageInformation imgInfo = new ImageInformation();
                                            imgInfo.Name = strRandomNumber;
                                            imgInfo.Content = File.ReadAllBytes(folder + "\\" + strRandomNumber);
                                            imgInfo.ContentType = enums.Image.ToString();
                                            imgInfo.ContentExtension = Path.GetExtension(Img.FileName);
                                            doctor.ImageExtension = Path.GetExtension(Img.FileName);
                                            MongoOperation mongo = new MongoOperation();
                                            if (mongo.InsertObjectToMongoDB(imgInfo))
                                            {
                                                doctor.ImageID = imgInfo.IMGID;
                                            }
                                            File.Delete(folder + "\\" + strRandomNumber);
                                            doctor.Name = txtDoctorName.Text;
                                            doctor.Specilization = txtSpecialization.Text;
                                            doctor.Experience = txtNoYears.Text;
                                            doctor.OperationsDone = txtOperationsDone.Text;
                                            doctor.LastUpdatedTime = DateTime.Now;
                                            if (chkIsActive.Checked)
                                            {
                                                doctor.IsActive = true;
                                            }
                                            else
                                            {
                                                doctor.IsActive = false;
                                            }
                                            List<long> lstDepartments = new List<long>();
                                            List<long> lstStores = new List<long>();
                                            ListBox ddlDepartments = (ListBox)DepartmentControl.FindControl("ddlDepartments");
                                            GroupDropDownList.GroupDropDownList ddlStores = (GroupDropDownList.GroupDropDownList)DepartmentControl.FindControl("ddlStores");
                                            switch (adminType)
                                            {
                                                case "A":
                                                    lstDepartments = Array.ConvertAll(ddlDepartments.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                                                    doctor.LstDepartments = dbContext.DEPARTMENTID.Where(c => lstDepartments.Contains(c.ID)).ToList();
                                                    lstStores = Array.ConvertAll(ddlStores.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                                                    doctor.LstLocations = dbContext.LOCATIONID.Where(c => lstStores.Contains(c.ID)).ToList();
                                                    doctor.RegAppID = adminid;
                                                    break;
                                                case "D":
                                                    doctor.LstDepartments = dbContext.DEPARTMENTID.Where(c => c.ID.Equals(adminid)).ToList();
                                                    lstStores = Array.ConvertAll(ddlStores.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                                                    doctor.LstLocations = dbContext.LOCATIONID.Where(c => lstStores.Contains(c.ID)).ToList();
                                                    DepartmentDTO dept1 = syncLogin.Getdepartment();
                                                    doctor.RegAppID = dept1.RegAppID;
                                                    break;
                                                case "S":
                                                    doctor.LstLocations = dbContext.LOCATIONID.Where(c => c.ID.Equals(adminid)).ToList();
                                                    StoreDTO store = new StoreDTO();
                                                    store = syncLogin.GetStoreById();
                                                    doctor.LstDepartments = dbContext.DEPARTMENTID.Where(c => c.ID.Equals(store.DeptID)).ToList();
                                                    doctor.RegAppID = store.RegAppID;
                                                    break;
                                                default:
                                                    break;
                                            }
                                            dbContext.DOCTORS.Add(doctor);
                                            dbContext.SaveChanges();
                                            ClientScript.RegisterStartupScript(GetType(), "notifier15", "successAlert('Doctor Added Successfully');", true);
                                            ClearAll();
                                            LoadDepartments();
                                            BindDoctors();
                                        }
                                    }
                                    Logger.Info("Ends When Submit Button Clicks in AddDoctors.aspx");
                                }
                            }
                            catch (Exception ex)
                            {
                                ClientScript.RegisterStartupScript(GetType(), "notifier15", "errorAlert('" + ex.Message + "');", true);
                            }
                        }
                        else if (btnSubmit.Text.Equals(MainResource.Update))
                        {
                            try
                            {
                                Logger.Info("Starts When Update() button Clicks() in AddDoctors.aspx");
                                long id = 0;
                                if (Int64.TryParse(lblID.Text, out id) && id > 0)
                                {
                                    using (ApolloDBContext dbContext = new ApolloDBContext())
                                    {
                                        Doctor doctor = dbContext.DOCTORS.FirstOrDefault(c => c.ID.Equals(id));
                                        if (!doctor.Name.Equals(txtDoctorName.Text))
                                        {
                                            CheckNameExists();
                                        }
                                        doctor.Name = txtDoctorName.Text;
                                        doctor.Order = 1;
                                        doctor.ColumnNbr = 0;
                                        doctor.Description = txtdesc.Text;
                                        if (fuDoctorImage.HasFile)
                                        {
                                            foreach (HttpPostedFile Img in fuDoctorImage.PostedFiles)
                                            {
                                                lock (lockTarget)
                                                {
                                                    string fileName = Path.GetFileName(Img.FileName);
                                                    string strRandomNumber = new Random().Next(1000, 100000).ToString() + txtDoctorName.Text.Trim().ToUpper() + Path.GetExtension(fileName);
                                                    Img.SaveAs(Path.Combine(folder, strRandomNumber));
                                                    ImageInformation imgInfo = new ImageInformation();
                                                    imgInfo.Name = strRandomNumber;
                                                    imgInfo.Content = File.ReadAllBytes(folder + "\\" + strRandomNumber);
                                                    imgInfo.ContentType = enums.Image.ToString();
                                                    imgInfo.ContentExtension = Path.GetExtension(Img.FileName);
                                                    doctor.ImageExtension = Path.GetExtension(Img.FileName);
                                                    MongoOperation mongo = new MongoOperation();
                                                    if (mongo.InsertObjectToMongoDB(imgInfo))
                                                    {
                                                        doctor.ImageID = imgInfo.IMGID;
                                                    }
                                                    File.Delete(folder + "\\" + strRandomNumber);
                                                }
                                            }
                                        }
                                        doctor.Specilization = txtSpecialization.Text;
                                        doctor.Experience = txtNoYears.Text;
                                        doctor.OperationsDone = txtOperationsDone.Text;
                                        doctor.LastUpdatedTime = DateTime.Now;
                                        if (chkIsActive.Checked)
                                        {
                                            doctor.IsActive = true;
                                        }
                                        else
                                        {
                                            doctor.IsActive = false;
                                        }
                                        List<long> lstDepts = new List<long>();
                                        List<long> lstStores = new List<long>();
                                        ListBox ddlDepartments = (ListBox)DepartmentControl.FindControl("ddlDepartments");
                                        GroupDropDownList.GroupDropDownList ddlStores = (GroupDropDownList.GroupDropDownList)DepartmentControl.FindControl("ddlStores");

                                        switch (adminType)
                                        {
                                            case "A":
                                                #region Department operations
                                                lstDepts = Array.ConvertAll(ddlDepartments.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                                                List<long> RemoveLstDepartments = doctor.LstDepartments.Where(c => !lstDepts.Contains(c.ID)).Select(c => c.ID).ToList();
                                                if (RemoveLstDepartments != null && RemoveLstDepartments.Count > 0)
                                                {
                                                    RemoveLstDepartments.ForEach(d =>
                                                    {
                                                        DepartmentID RemoveDeptDto = doctor.LstDepartments.FirstOrDefault(c => c.ID.Equals(d));
                                                        if (RemoveDeptDto != null)
                                                        {
                                                            doctor.LstDepartments.Remove(RemoveDeptDto);
                                                        }
                                                    });
                                                }
                                                lock (lockTarget)
                                                {
                                                    List<DepartmentID> dpt = dbContext.DEPARTMENTID.Where(c => lstDepts.Contains(c.ID)).ToList();
                                                    doctor.LstDepartments = dpt;
                                                }
                                                #endregion
                                                #region Store Operation
                                                List<long> RemoveLstStores = doctor.LstLocations.Where(c => !lstStores.Contains(c.ID)).Select(c => c.ID).ToList();
                                                RemoveLstStores.ToList().ForEach(d =>
                                                {
                                                    LocationID RemovelocationDto = doctor.LstLocations.ToList().Where(c => c.ID.Equals(d)).FirstOrDefault();
                                                    doctor.LstLocations.Remove(RemovelocationDto);
                                                });
                                                lstStores = Array.ConvertAll(ddlStores.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                                                lock (lockTarget)
                                                {
                                                    List<LocationID> location = dbContext.LOCATIONID.Where(c => lstStores.Contains(c.ID)).ToList();
                                                    doctor.LstLocations = location;
                                                }
                                                #endregion
                                                break;
                                            case "D":
                                                #region Store Operation
                                                List<long> RemoveLstStores1 = doctor.LstLocations.Where(c => !lstStores.Contains(c.ID)).Select(c => c.ID).ToList();
                                                RemoveLstStores1.ToList().ForEach(d =>
                                                {
                                                    LocationID RemovelocationDto = doctor.LstLocations.ToList().Where(c => c.ID.Equals(d)).FirstOrDefault();
                                                    doctor.LstLocations.Remove(RemovelocationDto);
                                                });
                                                lstStores = Array.ConvertAll(ddlStores.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                                                lock (lockTarget)
                                                {
                                                    List<LocationID> location = dbContext.LOCATIONID.Where(c => lstStores.Contains(c.ID)).ToList();
                                                    doctor.LstLocations = location;
                                                }
                                                #endregion
                                                break;
                                            case "S":
                                                break;
                                            default:
                                                break;
                                        }
                                        dbContext.SaveChanges();
                                        ClientScript.RegisterStartupScript(GetType(), "notifier15", "successAlert('Doctor Updated Successfully');", true);
                                        CancelUpdate();
                                        LoadDepartments();
                                        BindDoctors();
                                      
                                        Logger.Info("Ends When Update() button Clicks() in AddDoctors.aspx");
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.Error(ex, ex.Message);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                ClientScript.RegisterStartupScript(GetType(), "notifier3", "errorAlert('" + ex.Message + "');", true);
            }
        }

        private void ClearAll()
        {
            txtdesc.Text = string.Empty;
            txtDoctorName.Text = string.Empty;
            txtNoYears.Text = string.Empty;
            txtOperationsDone.Text = string.Empty;
            txtSpecialization.Text = string.Empty;
            ddlDepartment.ClearSelection();
            ListBox ddlDepartments = (ListBox)DepartmentControl.FindControl("ddlDepartments");
            GroupDropDownList.GroupDropDownList ddlStores = (GroupDropDownList.GroupDropDownList)DepartmentControl.FindControl("ddlStores");
            ddlDepartments.ClearSelection();
            ddlStores.ClearSelection();
            if (adminType.Equals("A"))
            {
                ddlStores.Items.Clear();
            }
        }
        private bool CheckNameExists()
        {
            string doctorName = txtDoctorName.Text;
            long doctorID = 0;
            if (Int64.TryParse(ddlDepartment.SelectedValue, out doctorID) && doctorID > 0)
            {
                using (ApolloDBContext dbContext = new ApolloDBContext())
                {
                    ApolloDB.Models.Department dept = dbContext.DEPARTMENTS.FirstOrDefault(c => c.ID.Equals(doctorID));
                    if (dept != null && dept.LstDoctors != null && dept.LstDoctors.Count > 0)
                    {
                        Doctor doctor = dept.LstDoctors.FirstOrDefault(c => c.Name.Equals(doctorName));
                        if (btnSubmit.Text.Equals(MainResource.Submit))
                        {
                            if (doctor == null)
                            {
                                nameError.InnerText = string.Empty;
                                return true;
                            }
                        }
                        if (btnSubmit.Text.Equals(MainResource.Update))
                        {
                            long id = 0;
                            if (Int64.TryParse(lblID.Text, out id) && id > 0)
                            {
                                if (doctor == null || doctor.ID.Equals(id))
                                {
                                    nameError.InnerText = string.Empty;
                                    return true;
                                }
                            }
                        }
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            nameError.InnerText = "Doctor name already exists";
            ClientScript.RegisterStartupScript(GetType(), "notifier16", "errorAlert('Doctor name already exists');", true);
            return false;
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            if (btnClear.Text.Equals(MainResource.Clear))
            {
                ClearAll();
            }
            else
            {
                Logger.Info("Starts Reset Button in AddDoctors Page");
                long id = 0;
                if (Int64.TryParse(lblID.Text, out id) && id > 0)
                {
                    using (ApolloDBContext dbContext = new ApolloDBContext())
                    {
                        GetEditData(id, dbContext);
                        Logger.Info("Ends Reset Button in AddDoctors Page");
                    }
                }
            }
        }
        protected void btnUpdateCancel_Click(object sender, EventArgs e)
        {
            CancelUpdate();
        }
        private void CancelUpdate()
        {
            lblPageHeading.Text = MainResource.Doctor;
            btnSubmit.Text = MainResource.Submit;
            btnClear.Text = MainResource.Clear;
            btnUpdateCancel.Visible = false;
            lblHeads.Text = MainResource.Doctor;
            lblID.Text = string.Empty;
            imgDoctor.Visible = false;
            ClearAll();
        }
        protected void gvViewDoctors_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            long id = 0;
            if (Int64.TryParse(e.CommandArgument.ToString(), out id) && id > 0)
            {
                using (ApolloDBContext dbContext = new ApolloDBContext())
                {
                    if (e.CommandName.Equals(MainResource.Edit))
                    {
                        GetEditData(id, dbContext);
                    }
                    else if (e.CommandName.Equals(MainResource.Delete))
                    {
                        Logger.Info("Starts When Delete Button Clicks in AddDoctors.aspx");
                        Doctor doc = dbContext.DOCTORS.FirstOrDefault(c => c.ID.Equals(id));
                        if (doc != null)
                        {
                            dbContext.DOCTORS.Remove(doc);
                            dbContext.SaveChanges();
                            ScriptManager.RegisterStartupScript(this, GetType(), "notifier15", "successAlert('Deleted Successfully');", true);
                            CancelUpdate();
                        }
                        BindDoctors();
                        Logger.Info("Ends When Delete Button Clicks in AddDoctors.aspx");
                    }
                    else if (e.CommandName.Equals(MainResource.View))
                    {
                        lock (lockTarget)
                        {
                            Logger.Info("Starts When View Button Clicks in AddDoctors.aspx");
                            Doctor doc = dbContext.DOCTORS.FirstOrDefault(c => c.ID.Equals(id));
                            if (doc != null)
                            {
                                lblDepartment.Text = doc.Department.Name;
                                lblDocName.Text = doc.Name;
                                if (string.IsNullOrEmpty(doc.Experience))
                                {
                                    liexp.Visible = false;
                                    lblexperience.Visible = false;
                                }
                                else
                                {
                                    liexp.Visible = true;
                                    lblexperience.Visible = true;
                                    lblexp.Text = doc.Experience;
                                }
                                if (string.IsNullOrEmpty(doc.Specilization))
                                {
                                    lispec.Visible = false;
                                    lblSpecialization.Visible = false;
                                }
                                else
                                {
                                    lispec.Visible = true;
                                    lblSpecialization.Visible = true;
                                    lblSpec1.Text = doc.Specilization;
                                }
                                Image1.ImageUrl = ConfigurationManager.AppSettings["apiurl"] + "api/Image/" + doc.ImageID;
                                if (string.IsNullOrEmpty(doc.OperationsDone))
                                {
                                    lioperation.Visible = false;
                                    lbloperation.Visible = false;
                                }
                                else
                                {
                                    lioperation.Visible = true;
                                    lbloperation.Visible = true;
                                    txtOperation.Text = doc.OperationsDone;
                                }
                                if (string.IsNullOrEmpty(doc.Description))
                                {
                                    lidesc.Visible = false;
                                    lblDesc.Visible = false;
                                }
                                else
                                {
                                    lidesc.Visible = true;
                                    lblDesc.Visible = true;
                                    txtDesc1.Text = doc.Description;
                                }
                                updModal.Update();
                                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                                sb.Append("<script type='text/javascript'>");
                                sb.Append("$('.modalDoctor').modal('show');");
                                sb.Append("</script>");
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "LaunchServerSide", sb.ToString(), false);
                            }
                            Logger.Info("Ends When View Button Clicks in AddDoctors.aspx");
                        }
                    }
                }
            }
        }

        private void GetEditData(long id, ApolloDBContext dbContext)
        {
            try
            {
                ListBox ddlDepartments = (ListBox)DepartmentControl.FindControl("ddlDepartments");
                GroupDropDownList.GroupDropDownList ddlStores = (GroupDropDownList.GroupDropDownList)DepartmentControl.FindControl("ddlStores");
                Label lblDepartments = (Label)DepartmentControl.FindControl("lblDepartments");
                Label lblStores = (Label)DepartmentControl.FindControl("lblStores");
                Logger.Info("Starts When Edit Button Clicks in AddDoctors.aspx");
                List<StoreDTO> lstStores = new List<StoreDTO>();
                ApolloDB.Models.Doctor doctor = dbContext.DOCTORS.FirstOrDefault(c => c.ID.Equals(id));
                if (doctor != null)
                {
                    lblID.Text = Convert.ToString(id,CultureInfo.CurrentCulture);
                    txtdesc.Text = doctor.Description;
                    txtDoctorName.Text = doctor.Name;
                    ddlDepartment.ClearSelection();
                    string strSelectedDepartment = doctor.Department.Name;
                    ddlDepartment.Items.FindByText(strSelectedDepartment).Selected = true;
                    if (doctor.IsActive)
                    {
                        chkIsActive.Checked = true;
                    }
                    else
                    {
                        chkIsActive.Checked = false;
                    }
                    txtNoYears.Text = doctor.Experience;
                    txtOperationsDone.Text = doctor.OperationsDone;
                    txtSpecialization.Text = doctor.Specilization;
                    imgDoctor.ImageUrl = ConfigurationManager.AppSettings["apiurl"] + "api/Image/" + doctor.ImageID;
                    imgDoctor.Visible = true;
                    switch (adminType)
                    {
                        case "A":
                            lblDepartments.Visible = true;
                            lblStores.Visible = true;
                            ddlDepartments.Visible = true;
                            ddlStores.Visible = true;
                            if (doctor.LstDepartments != null)
                            {
                                doctor.LstDepartments.ForEach(c =>
                                {
                                    ddlDepartments.Items.FindByValue(Convert.ToString(c.ID,CultureInfo.CurrentCulture)).Selected = true;
                                    lstStores.AddRange(syncLogin.GetStoresUnderDepartment(c.ID));
                                });
                                ddlStores.Items.Clear();
                                ddlStores.DataSource = lstStores;
                                ddlStores.DataGroupField = "DeptName";
                                ddlStores.DataTextField = "StoreName";
                                ddlStores.DataValueField = "SNO";
                                ddlStores.DataBind();
                                if (lstStores != null && lstStores.Count > 0)
                                {
                                    lstStores.ForEach(t =>
                                    {
                                        if (doctor.LstLocations.Contains(dbContext.LOCATIONID.FirstOrDefault(c => c.ID.Equals(t.SNO))))
                                        {
                                            ddlStores.Items.FindByValue(Convert.ToString(t.SNO,CultureInfo.CurrentCulture)).Selected = true;
                                        }
                                    });
                                }
                            }
                            break;
                        case "D":
                            lblDepartments.Visible = false;
                            ddlDepartments.Visible = false;
                            ddlStores.Visible = true;
                            lblStores.Visible = true;
                            List<DepartmentID> lstDepartments = doctor.LstDepartments.Where(c => c.ID.Equals(adminid)).ToList();
                            if (lstDepartments != null)
                            {
                                lstDepartments.ForEach(c =>
                                {
                                    lstStores.AddRange(syncLogin.GetStoresUnderDepartment(c.ID));
                                });
                                ddlStores.Items.Clear();
                                ddlStores.DataSource = lstStores;
                                ddlStores.DataGroupField = "DeptName";
                                ddlStores.DataTextField = "StoreName";
                                ddlStores.DataValueField = "SNO";
                                ddlStores.DataBind();
                                if (lstStores != null && lstStores.Count > 0)
                                {
                                    lstStores.ForEach(t =>
                                    {
                                        if (doctor.LstLocations.Contains(dbContext.LOCATIONID.FirstOrDefault(c => c.ID.Equals(t.SNO))))
                                        {
                                            ddlStores.Items.FindByValue(Convert.ToString(t.SNO,CultureInfo.CurrentCulture)).Selected = true;
                                        }
                                    });
                                }
                            }
                            break;
                        case "S":
                            lblStores.Visible = false;
                            ddlStores.Visible = false;
                            lblDepartments.Visible = false;
                            ddlDepartments.Visible = false;
                            break;
                        default:
                            break;
                    }
                    btnSubmit.Text = MainResource.Update;
                    btnClear.Text = MainResource.Reset;
                    lblHeads.Text = MainResource.EditDoctor;
                    btnUpdateCancel.Visible = true;
                    lblPageHeading.Text = MainResource.EditDoctor;
                }
                Logger.Info("Ends When Edit Button Clicks in AddDoctors.aspx");
            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(GetType(), "notifier3", "errorAlert('" + ex.Message + "');", true);
            }
        }
        protected void gvViewDoctors_RowDataBound(object sender, GridViewRowEventArgs e)
        {
             if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblIsActive = ((Label)e.Row.FindControl("lblIsActive"));
                if (lblIsActive.Text == MainResource.False)
                {
                    lblIsActive.Attributes.Add("class", "badge bg-red");
                    lblIsActive.Attributes["data-toggle"] = "tooltip";
                    lblIsActive.Attributes["data-placement"] = "top";
                    lblIsActive.Attributes["title"] = "Please Edit to enable.";
                }
            }
        }
        protected void gvViewDoctors_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }
        protected void gvViewDoctors_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }
        protected void ddlViewDepartments_SelectedIndexChanged(object sender, EventArgs e)
        {
            gvViewDoctors.DataSource = null;
            gvViewDoctors.DataBind();
            if(ddlViewDepartments.SelectedIndex>0)
            {
                BindDoctors();
            }
        }
    }
}