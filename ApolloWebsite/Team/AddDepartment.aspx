﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMasterPage.Master" AutoEventWireup="true" CodeBehind="AddDepartment.aspx.cs" Inherits="ApolloWebsite.Team.AddDepartment" %>

<%@ Register Src="~/DepartmentControl.ascx" TagPrefix="uc1" TagName="DepartmentControl" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="http://code.jquery.com/jquery-1.9.0.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>
    <style>
        .text1 {
            word-wrap: break-word;
        }
    </style>
    <script>
        $(document).ready(function () {
            $(".deptDesc").keyup(function () {
                var deptDesc = $(".deptDesc").val();
                if (deptDesc.length > 200)   //categName maximum length is 50 characters
                {
                    $(".deptDescError").text("maximum 200 characters");
                    return false;
                }
                else {
                    $(".deptDescError").text("");
                }
            });
            $(".deptName").keyup(function () {
                var deptName = $(".deptName").val();
                if (deptName == "") {
                    $(".deptNameError").text("Please Enter Department Name.");
                    return false;
                }
                if (deptName.length < 3)    //categName minimum length is 3 characters
                {
                    $(".deptNameError").text("minimum 3 characters");
                    return false;
                }
                if (deptName.length > 50)   //categName maximum length is 50 characters
                {
                    $(".deptNameError").text("maximum 50 characters");
                    return false;
                }
                else {
                    $(".deptNameError").text("");
                }
            });
            $(".imageupload1").change(function () {
                var btnText = $(".savebtn").val();
                var imageupload1 = $(".imageupload1").val();
                var pngimg = imageupload1.substring(imageupload1.lastIndexOf(".") + 1, imageupload1.length);

                if (btnText == "Submit") {
                    if (imageupload1 == "") {
                        $(".fuDeptImageError").text("Please Choose Image");
                        return false;
                    }
                    if (pngimg.toUpperCase() != "PNG") {
                        $(".fuDeptImageError").text("Please Choose PNG Format Only");
                        return false;
                    }
                }
                if (btnText == "Update") {
                    if (imageupload1 != "") {
                        if (pngimg.toUpperCase() != "PNG") {
                            $(".fuDeptImageError").text("Please Choose PNG Format Only");
                            return false;
                        }
                        else {
                            $(".fuDeptImageError").text("");
                        }
                    }
                }
                else {
                    $(".fuDeptImageError").text("");
                }
            });
            $(".imageupload1").blur(function () {
                var imageupload1 = $(".imageupload1").val();
                var btnText = $(".savebtn").val();
                if (btnText == "Update") {
                    if (imageupload1 == "") {
                        $(".fuDeptImageError").text("");
                    }
                }
            });
            $(".savebtn").click(function () {
                var btnText = $(".savebtn").val();
                var count = 0;
                var deptName = $(".deptName").val();
                var deptDesc = $(".deptDesc").val();
                var imageupload1 = $(".imageupload1").val();
                var pngimg = imageupload1.substring(imageupload1.lastIndexOf(".") + 1, imageupload1.length);
                if (deptDesc.length > 200)   //categName maximum length is 50 characters
                {
                    $(".deptDescError").text("maximum 200 characters");
                    count = count + 1;
                }
                if (deptName.length < 3)    //categName minimum length is 3 characters
                {
                    $(".deptNameError").text("minimum 3 characters");
                    count = count + 1;
                }
                if (deptName.length > 50)   //categName maximum length is 50 characters
                {
                    $(".deptNameError").text("maximum 50 characters");
                    count = count + 1;
                }
                if (btnText == "Submit") {
                    if (pngimg.toUpperCase() != "PNG") {
                        $(".fuDeptImageError").text("Please Choose PNG Format Only");
                        count = count + 1;
                    }
                    if (imageupload1 == "") {
                        $(".fuDeptImageError").text("Please Choose Image");
                        count = count + 1;
                    }
                    var admintypedata = "<%=this.adminType%>";
                    if (admintypedata == "A") {
                        var Listoptions = document.getElementById("<%=DepartmentControl.FindControl("ddlDepartments").ClientID%>").options;
                        var Selectedcount = 0;
                        if (Listoptions.count > 0) {
                            for (var i = 0; i < Listoptions.length; i++) {
                                if (Listoptions[i].selected == true) {
                                    Selectedcount++;
                                }
                            }
                            if (Selectedcount < 1) {
                                $(".lstDeptError").text("Please select atleast one Department");
                                count = count + 1;
                            }
                        }
                    }
                    if (admintypedata == "A" || admintypedata == "D") {
                        var Listoptions1 = document.getElementById("<%=DepartmentControl.FindControl("ddlStores").ClientID%>").options;
                        var Selectedcount1 = 0;
                        for (var i = 0; i < Listoptions1.length; i++) {
                            if (Listoptions1[i].selected == true) {
                                Selectedcount1++;
                            }
                        }
                        if (Selectedcount1 < 1) {
                            $(".lstStoreError").text("Please select atleast one Location");
                            count = count + 1;
                        }
                    }
                }
                else if (btnText == "Update") {
                    if (imageupload1 != "") {
                        if ($('#fuDept').text() == "Please Choose PNG Format Only") {
                            count = count + 1;
                        }
                    }
                }
                if (deptName == "") {
                    $(".deptNameError").text("Please Enter Department Name.");
                    count = count + 1;
                }
                if (count > 0) {
                    return false;
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content-header">
        <h1>
            <asp:Label ID="lblPageHeading" runat="server" Text="Departments" /><small>(Add / View)</small></h1>
        <ol class="breadcrumb">
            <li><a href="<%=Page.ResolveUrl("~/Home.aspx") %>"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a>Our Team</a></li>
            <li class="active"><a href="javascript:void(0);">Departments</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <div class="form-group">
                            <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="Label1" runat="server" Text="Department Name"></asp:Label>
                            <asp:TextBox ID="txtDepartmentName" runat="server" class="form-control deptName" placeholder="Enter DepartmentName" />
                            <span id="deptError" runat="server" class="deptNameError error-code"></span>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="lblDesc" runat="server" Text="Description"></asp:Label>
                            <asp:TextBox ID="txtDeptDesc" runat="server" class="form-control deptDesc" placeholder="Enter Description" />
                            <span class="deptDescError error-code"></span>
                        </div>
                        <div class="form-group">
                            <uc1:DepartmentControl runat="server" ID="DepartmentControl" />
                               <div class="col-md-12">
                                    <div class="col-md-6">
                                        <span class="lstDeptError error-code"></span>
                                    </div>
                                    <div class="col-md-6">
                                        <span class="lstStoreError error-code"></span>
                                    </div>
                                </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label3" runat="server" Text="Image"></asp:Label>
                            <asp:FileUpload ID="fuDepartmentImage" runat="server" CssClass="imageupload1" />
                            <asp:HiddenField ID="hdGuid" runat="server" />
                            <span id="fuDept" class="fuDeptImageError error-code"></span>
                            <p class="badge bg-light-blue">Best fit icon size is 100 x 100 pixels. </p>
                            <asp:Image ID="imgDept" runat="server" Style="height: 35px; width: 35px;" Visible="false" />
                        </div>
                        <div class="checkbox form-group">
                            <asp:CheckBox ID="chkIsActive" runat="server" Checked="true" CssClass="checkbox" Text="IsActive" />
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <asp:Button ID="btnSubmit" runat="server" class="btn btn-primary savebtn" Text="Submit" OnClick="btnSubmit_Click" />
                        <asp:Button ID="btnClear" runat="server" class="btn btn-default" Text="Clear" OnClick="btnClear_Click" />
                        <asp:Button ID="btnUpdateCancel" runat="server" class="btn btn-default" Text="Cancel Update" Visible="false" OnClick="btnUpdateCancel_Click" />
                    </div>
                </div>
                <!-- /.box -->

            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">View Departments</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <asp:GridView ID="gvViewDepartments" runat="server" class="table table-hover" AutoGenerateColumns="false" GridLines="None"
                            OnRowCommand="gvViewDepartments_RowCommand" OnRowDataBound="gvViewDepartments_RowDataBound"
                            OnRowEditing="gvViewDepartments_RowEditing"
                            OnRowDeleting="gvViewDepartments_RowDeleting">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <div class="col-md-12">

                                            <div class="col-md-4 col-xs-4">
                                                <asp:Label ID="Label2" runat="server" Text="Image" />
                                            </div>
                                            <div class="col-md-5 col-xs-5">
                                                <asp:Label ID="Label5" runat="server" Text="Department Name" />
                                            </div>
                                            <div class="col-md-1 col-xs-1">
                                                <asp:Label ID="Label6" runat="server" Text="Active" />
                                            </div>
                                            <div class="col-md-1 col-xs-1">
                                                <asp:Label ID="Label7" runat="server" Text="Edit" />
                                            </div>
                                            <div class="col-md-1 col-xs-1">
                                                <asp:Label ID="Label8" runat="server" Text="Delete" />
                                            </div>
                                        </div>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div class="col-md-12">
                                            <div class="col-md-4 col-xs-4">
                                                <asp:Label ID="lblID" runat="server" Text='<%#Eval("ID") %>' Visible="false" />
                                                <asp:Image ID="imgCategory" runat="server" Style="height: 35px; width: 35px;" ImageUrl='<%#string.Format(ConfigurationManager.AppSettings["apiurl"]+"api/Image/{0}",Eval("ImageID")) %>' />
                                            </div>
                                            <div class="col-md-5 col-xs-5">
                                                <asp:Label ID="lblName" runat="server" CssClass="text1" Text='<%#Eval("Name") %>' />
                                            </div>
                                            <div class="col-md-1 col-xs-1">
                                                <span>
                                                    <asp:Label ID="lblIsActive" runat="server" class="badge bg-green" Text='<%#Eval("IsActive") %>' /></span>
                                            </div>
                                            <div class="col-md-1 col-xs-1">
                                                <asp:ImageButton ID="imgEdit" runat="server" CommandName="Edit"
                                                    CommandArgument='<%#Eval("ID") %>' ImageUrl="~/Icons/edit_ic.png" data-toggle="tooltip"
                                                    data-placement="top" title="Edit Record" />
                                            </div>
                                            <div class="col-md-1 col-xs-1">
                                                <asp:ImageButton ID="imgDelete" runat="server" OnClientClick="javascript:return ConfirmDelete();" CommandName="Delete"
                                                    CommandArgument='<%#Eval("ID") %>' ImageUrl="~/Icons/delete_ic.png" data-toggle="tooltip"
                                                    data-placement="top" title="Delete Record" />
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                <ul style="text-align: center;">
                                    <li class="list-group-item">No Records Found</li>
                                </ul>
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
</asp:Content>
