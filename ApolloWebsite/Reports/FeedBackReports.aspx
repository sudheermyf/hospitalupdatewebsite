﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMasterPage.Master" AutoEventWireup="true" CodeBehind="FeedBackReports.aspx.cs" Inherits="ApolloWebsite.Reports.FeedBackReports" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
       <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/start/jquery-ui.css">
<script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript" src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <style type="text/css">
    .ui-progressbar
    {
        height:1.5em;
        position: relative;
    }
    .ui-widget-header{
        border: 1px solid #2191C0;
        background: #2191C0;}
    .ui-progressbar .ui-progressbar-value{margin:0;}
    .progress-label
    {
        position: absolute;
        left: 30%;
        font-size:10px;
        text-shadow: 1px 1px 0 #fff;
    }
    /*body
    {
        font-family: Arial;
        font-size: 8pt;
    }*/
</style>
    <script type="text/javascript">
    $(function () {
        $(".progress").each(function () {
            $(this).progressbar({
                value: parseInt($(this).find('.progress-label').text())
            });
        });
    });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <section class="content-header">
          <h1>
            FeedBack
            <small>Reports</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a>Reports</a></li>
            <li class="active"><a href="javascript:void(0);">Feedback Reports</a></li>
          </ol>
        </section>
 <section class="content">
          <!-- Info boxes -->
          <div class="row">
           <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Reports</h3>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <asp:GridView ID="gvViewFeedback" runat="server" class="table table-hover" AutoGenerateColumns="false" GridLines="None"
                    OnRowCommand="gvViewFeedback_RowCommand" OnRowDataBound="gvViewFeedback_RowDataBound"
                    OnRowEditing="gvViewFeedback_RowEditing"
                    OnRowDeleting="gvViewFeedback_RowDeleting">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <div class="col-md-12">
                                        <div class="col-md-5 col-xs-5">
                                            <asp:Label ID="Label9" runat="server" Text="Question" />
                                        </div>
                                        
                                        <div class="col-md-7 col-xs-7">
                                        <div class="col-md-2 col-xs-2">
                                            <asp:Label ID="Label5" runat="server" Text="Extremely Good" />
                                        </div>
                                        <div class="col-md-2 col-xs-2">
                                            <asp:Label ID="Label10" runat="server" Text="Very Good" />
                                        </div>
                                        <div class="col-md-2 col-xs-2">
                                            <asp:Label ID="Label11" runat="server" Text="Good" />
                                        </div>
                                        <div class="col-md-2 col-xs-2">
                                            <asp:Label ID="Label12" runat="server" Text="Average" />
                                        </div>
                                        <div class="col-md-2 col-xs-2">
                                            <asp:Label ID="Label13" runat="server" Text="Bad" />
                                        </div>
                                            
                                            </div>
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="col-md-12">
                                        <div class="col-md-5 col-xs-5">
                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("strQuestion") %>' />
                                        </div>
                                        <div class="col-md-7 col-xs-7">
                                            
                                        <div class="col-md-2 col-xs-2">
                                            <div class='progress'>
                                                <div class="progress-label">
                                                    <%# Eval("strOne") %>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-xs-2">
                                         <div class='progress'>
                                                <div class="progress-label">
                                                    <%# Eval("strTwo") %>
                                                </div>
                                            </div>
                                        </div>
                                            
                                        <div class="col-md-2 col-xs-2">
                                              <div class='progress'>
                                                <div class="progress-label">
                                                    <%# Eval("strThree") %>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-xs-2">
                                            <div class='progress'>
                                                <div class="progress-label">
                                                    <%# Eval("strfour") %>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-xs-2">
                                             <div class='progress'>
                                                <div class="progress-label">
                                                    <%# Eval("strfive") %>
                                                </div>
                                            </div>
                                        </div>
                                       </div>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                        <ul style="text-align: center;">
                            <li class="list-group-item">No Records Found</li>
                        </ul>
                    </EmptyDataTemplate>
                    </asp:GridView>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div><!-- /.row -->
        </section>
</asp:Content>
