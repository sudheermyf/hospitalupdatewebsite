﻿using ApolloDB.DBFolder;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ApolloWebsite.Reports
{
    public partial class FeedBackReports : System.Web.UI.Page
    {
        DataTable dt;
        object lockTarget = new object();
        private Logger Logger = LogManager.GetCurrentClassLogger();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["adminloginid"] != null)
            {
                using (var dbContext = new ApolloDBContext())
                {
                    dt = new DataTable();
                    dt.Locale = CultureInfo.InvariantCulture;
                    DataRow dr = null;
                    dt.Columns.Add(new DataColumn("strSno", typeof(string)));
                    dt.Columns.Add(new DataColumn("strQuestion", typeof(string)));
                    dt.Columns.Add(new DataColumn("strOne", typeof(string)));
                    dt.Columns.Add(new DataColumn("strTwo", typeof(string)));
                    dt.Columns.Add(new DataColumn("strThree", typeof(string)));
                    dt.Columns.Add(new DataColumn("strfour", typeof(string)));
                    dt.Columns.Add(new DataColumn("strfive", typeof(string)));
                    if (dbContext.FEEDBACKQUESTIONS.ToList().Count > 0)
                    {
                        gvViewFeedback.DataSource = null;
                        gvViewFeedback.DataBind();

                        foreach (var item in dbContext.FEEDBACKQUESTIONS.ToList())
                        {
                            long strSno = item.ID;
                            //long strSno = item.ID;
                            lock (lockTarget)
                            {
                                try
                                {
                                    ApolloDB.Models.FeedBackQuestions feedBackquest = dbContext.FEEDBACKQUESTIONS.FirstOrDefault(c => c.ID.Equals(strSno));

                                    if (feedBackquest != null && feedBackquest.LSTFEEDBACKS != null && feedBackquest.LSTFEEDBACKS.Count > 0)
                                    {
                                        dr = dt.NewRow();
                                        dr["strSno"] = strSno;
                                        dr["strQuestion"] = feedBackquest.QUESTION;
                                        dr["strOne"] = string.Format(CultureInfo.CurrentCulture,"{0:0.00%}", (Convert.ToDecimal(feedBackquest.LSTFEEDBACKS.Where(d => d.RATING.Equals(1)).Count()) / feedBackquest.LSTFEEDBACKS.Count));
                                        dr["strTwo"] = string.Format(CultureInfo.CurrentCulture, "{0:0.00%}", (Convert.ToDecimal(feedBackquest.LSTFEEDBACKS.Where(d => d.RATING.Equals(2)).Count()) / feedBackquest.LSTFEEDBACKS.Count));
                                        dr["strThree"] = string.Format(CultureInfo.CurrentCulture, "{0:0.00%}", (Convert.ToDecimal(feedBackquest.LSTFEEDBACKS.Where(d => d.RATING.Equals(3)).Count()) / feedBackquest.LSTFEEDBACKS.Count));
                                        dr["strfour"] = string.Format(CultureInfo.CurrentCulture, "{0:0.00%}", (Convert.ToDecimal(feedBackquest.LSTFEEDBACKS.Where(d => d.RATING.Equals(4)).Count()) / feedBackquest.LSTFEEDBACKS.Count));
                                        dr["strfive"] = string.Format(CultureInfo.CurrentCulture, "{0:0.00%}", (Convert.ToDecimal(feedBackquest.LSTFEEDBACKS.Where(d => d.RATING.Equals(5)).Count()) / feedBackquest.LSTFEEDBACKS.Count));
                                        dt.Rows.Add(dr);
                                        //Store the DataTable in ViewState
                                        ViewState["CurrentTable"] = dt;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error(ex, ex.Message);
                                }
                            }
                        }
                        gvViewFeedback.DataSource = dt;
                        gvViewFeedback.DataBind();
                    }
                    else
                    {
                        gvViewFeedback.DataSource = null;
                        gvViewFeedback.DataBind();
                    }
                }
            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }
        }
        protected void gvViewFeedback_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void gvViewFeedback_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void gvViewFeedback_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void gvViewFeedback_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }
    }
}