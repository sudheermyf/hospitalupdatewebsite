﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ApolloDB.DBFolder;
using ApolloDB.Models;
using System.Globalization;

namespace ApolloWebsite.Settings
{
    public partial class OrderManagement : System.Web.UI.Page
    {
        string username = string.Empty, password = string.Empty;
        protected string adminType = string.Empty;
        long adminid = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["adminloginid"] != null)
            {
                adminType = Convert.ToString(Session["adminType"], CultureInfo.CurrentCulture);
                if (Int64.TryParse(Convert.ToString(Session["adminloginid"], CultureInfo.CurrentCulture), out adminid) && adminid > 0)
                {

                }
                else
                {
                    Response.Redirect("../Default.aspx");
                }
                username = Convert.ToString(Session["adminusername"], CultureInfo.CurrentCulture);
                password = Convert.ToString(Session["adminpassword"], CultureInfo.CurrentCulture);
            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }
            if (!IsPostBack)
            {
                BindCategories();
            }
        }

        private void BindCategories()
        {
            gvViewCategories.DataSource = null;
            gvViewCategories.DataBind();
            using (ApolloDBContext dbContext = new ApolloDBContext())
            {
                if (dbContext.CATEGORY.Count() > 0)
                {
                    switch (adminType)
                    {
                        case "A":
                            gvViewCategories.DataSource = dbContext.CATEGORY.Where(d =>d.RegAppID.Equals(adminid)).OrderBy(g=>g.Order).ToList();
                            gvViewCategories.DataBind();
                            break;
                        case "D":
                            gvViewCategories.DataSource = dbContext.CATEGORY.Where(d => d.LstDepartments.Any(g => g.ID.Equals(adminid))).OrderBy(g => g.Order).ToList();
                            gvViewCategories.DataBind();
                            break;
                        case "S":
                            gvViewCategories.DataSource = dbContext.CATEGORY.Where(d => d.LstLocations.Any(g => g.ID.Equals(adminid))).OrderBy(g => g.Order).ToList();
                            gvViewCategories.DataBind();
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        protected void btnUpdateSortOrder_Click(object sender, EventArgs e)
        {
            // stores id_ in array
            if (Request.Form.GetValues("categid") != null)
            {
                string[] id_language = Request.Form.GetValues("categid");
                int sortNumber = 1;

                // Loop over array, which contains id_ 
                foreach (string i in id_language)
                {
                    // method which which fire update query order save into database
                    updateRecord(i, sortNumber);
                    sortNumber++;
                }
                //Response.Redirect(Request.RawUrl);
                BindCategories();
            }
        }
        private void updateRecord(string categId, int sortNumber)
        {
            long id = 0;
            if (Int64.TryParse(categId, out id) && id > 0)
            {
                using (ApolloDBContext dbContext = new ApolloDBContext())
                {
                    Category categ=dbContext.CATEGORY.FirstOrDefault(c => c.ID.Equals(id));
                    if (categ != null)
                    {
                        categ.Order = sortNumber;
                        categ.LastUpdatedTime = DateTime.Now;
                        dbContext.SaveChanges();
                        ClientScript.RegisterStartupScript(GetType(), "notifier15", "successAlert('Screen Order Updated Successfully');", true);
                    }
                }
            }
        }

       
    }
}