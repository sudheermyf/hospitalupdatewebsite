﻿using ApolloDB.DBFolder;
using ApolloDB.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ApolloWebsite.Settings
{
    public partial class DoctorsOrder : System.Web.UI.Page
    {
        string username = string.Empty, password = string.Empty;
        protected string adminType = string.Empty;
        long adminid = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["adminloginid"] != null)
            {
                adminType = Convert.ToString(Session["adminType"], CultureInfo.CurrentCulture);
                if (Int64.TryParse(Convert.ToString(Session["adminloginid"], CultureInfo.CurrentCulture), out adminid) && adminid > 0)
                {

                }
                else
                {
                    Response.Redirect("../Default.aspx");
                }
                username = Convert.ToString(Session["adminusername"], CultureInfo.CurrentCulture);
                password = Convert.ToString(Session["adminpassword"], CultureInfo.CurrentCulture);
            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }
            if (!IsPostBack)
            {
                BindDepartments();
            }
        }

        private void BindDepartments()
        {
            ddlDepartment.DataSource = null;
            ddlDepartment.DataBind();
            using (ApolloDBContext dbContext = new ApolloDBContext())
            {
                if (dbContext.DEPARTMENTS.ToList().Count > 0)
                {
                    switch (adminType)
                    {
                        case "A":
                            ddlDepartment.DataSource = dbContext.DEPARTMENTS.Where(d => d.RegAppID.Equals(adminid)).ToList();
                            ddlDepartment.DataBind();
                            break;
                        case "D":
                            ddlDepartment.DataSource = dbContext.DEPARTMENTS.Where(d => d.LstDepartments.Any(g => g.ID.Equals(adminid))).ToList();
                            ddlDepartment.DataBind();
                            break;
                        case "S":
                            ddlDepartment.DataSource = dbContext.DEPARTMENTS.Where(d => d.LstLocations.Any(g => g.ID.Equals(adminid))).ToList();
                            ddlDepartment.DataBind();
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
           BindDoctors();
        }
        private void BindDoctors()
        {
            gvViewDoctors.DataSource = null;
            gvViewDoctors.DataBind();
            if (ddlDepartment.SelectedIndex > 0)
            {
                long deptId = 0;
                if (Int64.TryParse(ddlDepartment.SelectedValue, out deptId) && deptId > 0)
                {
                    using (ApolloDBContext dbContext = new ApolloDBContext())
                    {
                        ApolloDB.Models.Department dept = dbContext.DEPARTMENTS.FirstOrDefault(c => c.ID.Equals(deptId));
                        if (dept != null && dept.LstDoctors != null && dept.LstDoctors.Count > 0)
                        {
                            List<Doctor> lstDoctors = dept.LstDoctors.OrderBy(c => c.Order).ToList();
                            gvViewDoctors.DataSource = lstDoctors;
                            gvViewDoctors.DataBind();
                        }
                    }
                }
            }
        }
        protected void btnUpdateSortOrder_Click(object sender, EventArgs e)
        {
            // stores id_ in array
            if(Request.Form.GetValues("doctorid")!=null)
            {
                string[] id_language = Request.Form.GetValues("doctorid");
                int sortNumber = 1;

                // Loop over array, which contains id_ 
                foreach (string i in id_language)
                {
                    // method which which fire update query order save into database
                    updateRecord(i, sortNumber);
                    sortNumber++;
                }
                //Response.Redirect(Request.RawUrl);
                ClearAll();
            }
        }

        private void ClearAll()
        {
            ddlDepartment.ClearSelection();
            gvViewDoctors.DataSource = null;
            gvViewDoctors.DataBind();
        }

        private void updateRecord(string doctorId, int sortNumber)
        {
            long id = 0;
            if (Int64.TryParse(doctorId, out id) && id > 0)
            {
                using (ApolloDBContext dbContext = new ApolloDBContext())
                {
                    Doctor doc = dbContext.DOCTORS.FirstOrDefault(c => c.ID.Equals(id));
                    if (doc != null)
                    {
                        doc.Order = sortNumber;
                        doc.LastUpdatedTime = DateTime.Now;
                        dbContext.SaveChanges();
                        ClientScript.RegisterStartupScript(GetType(), "notifier15", "successAlert('Doctors Order Updated Successfully');", true);
                    }
                }
            }
        }
    }
}