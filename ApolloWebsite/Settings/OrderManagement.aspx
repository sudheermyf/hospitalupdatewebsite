﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMasterPage.Master" AutoEventWireup="true" CodeBehind="OrderManagement.aspx.cs" Inherits="ApolloWebsite.Settings.OrderManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">    
     <script>
        $(document).ready(function () {
          
            $(".gvLanguageClass").sortable({
                items: 'tr:not(tr:first-child)',
                cursor: 'pointer',
                axis: 'y',
                dropOnEmpty: false,
                start: function (e, ui) {
                    ui.item.addClass("selected");
                },
                stop: function (e, ui) {
                    ui.item.removeClass("selected");
                },
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content-header">
        <h1>Screen Order</h1>
        <ol class="breadcrumb">
            <li><a href="<%=Page.ResolveUrl("~/Home.aspx") %>"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a>Settings</a></li>
            <li class="active"><a href="javascript:void(0);">Screen Order</a></li>
        </ol>
    </section>

  <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <asp:Label ID="Label4" runat="server" Text="Set Order" /></h3>
                    </div>
                    <!-- form start -->
                    <div class="box-body body-min">
                            <div class="form-group table-responsive">
                            <asp:GridView ID="gvViewCategories" CssClass="gvLanguageClass table table-hover" runat="server" GridLines="None" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="SNo" ItemStyle-Width="50">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                            <input type="hidden" name="categid" value='<%# Eval("ID") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Image" ItemStyle-Width="50">
                                        <ItemTemplate>
                                            <asp:Image ID="imgCategory" runat="server" Style="height: 35px; width: 35px;" ImageUrl='<%#string.Format(ConfigurationManager.AppSettings["apiurl"]+"api/Image/{0}",Eval("ImageID")) %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Name" HeaderText="Category Name" ItemStyle-Width="160" />
                                    <asp:BoundField DataField="Order" HeaderText="Sort Order" ItemStyle-Width="50" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <asp:Button ID="btnUpdateSortOrder" runat="server" Text="Update Sort Order" CssClass="btn btn-primary"
                            OnClick="btnUpdateSortOrder_Click" />
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
</asp:Content>
