﻿using ApolloDB.DBFolder;
using ApolloDB.Models;
using ApolloWebsite.Logic;
using CMCAPI.Client.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ApolloWebsite.Settings
{
    public partial class ContactUs : System.Web.UI.Page
    {
        object lockTarget = new object();
        string username = string.Empty, password = string.Empty;
        protected string adminType = string.Empty;
        long adminid = 0;
        SyncLogins syncLogin = new SyncLogins();
        private Logger Logger = LogManager.GetCurrentClassLogger();
        protected void Page_Load(object sender, EventArgs e)
        {
            DepartmentControl.bindRelatedData -= DepartmentControl_bindRelatedData;
            DepartmentControl.bindRelatedData += DepartmentControl_bindRelatedData;
            if (Session["adminloginid"] != null)
            {
                adminType = Convert.ToString(Session["adminType"], CultureInfo.CurrentCulture);
                if (Int64.TryParse(Convert.ToString(Session["adminloginid"], CultureInfo.CurrentCulture), out adminid) && adminid > 0)
                {

                }
                else
                {
                    Response.Redirect("../Default.aspx");
                }
                username = Convert.ToString(Session["adminusername"], CultureInfo.CurrentCulture);
                password = Convert.ToString(Session["adminpassword"], CultureInfo.CurrentCulture);
            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }
            if(!IsPostBack)
            {
                BindContacts();
            }
        }

        private void DepartmentControl_bindRelatedData(string abcd)
        {
            long id = 0;
            if (Int64.TryParse(Convert.ToString(lblID.Text, CultureInfo.CurrentCulture), out id) && id > 0)
            {
                GetEditedData(id);
            }
        }

        private void GetEditedData(long id)
        {
            try
            {
                lock (lockTarget)
                {
                    Logger.Info("Starts When Edit Button Clicks in ContactUs.aspx");
                    using (ApolloDBContext dbContext = new ApolloDBContext())
                    {
                        ListBox ddlDepartments = (ListBox)DepartmentControl.FindControl("ddlDepartments");
                        GroupDropDownList.GroupDropDownList ddlStores = (GroupDropDownList.GroupDropDownList)DepartmentControl.FindControl("ddlStores");
                        List<long> lstnewDepartments = new List<long>();
                        lstnewDepartments = Array.ConvertAll(ddlDepartments.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                        Label lblDepartments = (Label)DepartmentControl.FindControl("lblDepartments");
                        Label lblStores = (Label)DepartmentControl.FindControl("lblStores");
                        List<StoreDTO> lstStores = new List<StoreDTO>();
                        ContactUS contact = dbContext.CONTACTUS.FirstOrDefault(c => c.ID.Equals(id));

                        lblID.Text = Convert.ToString(id, CultureInfo.CurrentCulture);
                        switch (adminType)
                        {
                            case "A":
                                lblDepartments.Visible = true;
                                lblStores.Visible = true;
                                ddlDepartments.Visible = true;
                                ddlStores.Visible = true;
                                if (lstnewDepartments != null)
                                {
                                    lstnewDepartments.ForEach(c =>
                                    {
                                        ddlDepartments.Items.FindByValue(c.ToString(CultureInfo.CurrentCulture)).Selected = true;
                                        lstStores.AddRange(syncLogin.GetStoresUnderDepartment(c));
                                    });
                                    ddlStores.Items.Clear();
                                    ddlStores.DataSource = lstStores;
                                    ddlStores.DataGroupField = "DeptName";
                                    ddlStores.DataTextField = "StoreName";
                                    ddlStores.DataValueField = "SNO";
                                    ddlStores.DataBind();
                                    if (lstStores != null && lstStores.Count > 0)
                                    {
                                        lstStores.ForEach(t =>
                                        {
                                            if (contact.LstLocations.Contains(dbContext.LOCATIONID.FirstOrDefault(c => c.ID.Equals(t.SNO))))
                                            {
                                                ddlStores.Items.FindByValue(Convert.ToString(t.SNO,CultureInfo.CurrentCulture)).Selected = true;
                                            }
                                        });
                                    }
                                }
                                break;
                            case "D":
                                lblDepartments.Visible = false;
                                ddlDepartments.Visible = false;
                                ddlStores.Visible = true;
                                lblStores.Visible = true;
                                List<DepartmentID> lstDepartments = contact.LstDepartments.Where(c => c.ID.Equals(adminid)).ToList();
                                if (lstDepartments != null)
                                {
                                    lstDepartments.ForEach(c =>
                                    {
                                        lstStores.AddRange(syncLogin.GetStoresUnderDepartment(c.ID));
                                    });
                                    ddlStores.Items.Clear();
                                    ddlStores.DataSource = lstStores;
                                    ddlStores.DataGroupField = "DeptName";
                                    ddlStores.DataTextField = "StoreName";
                                    ddlStores.DataValueField = "SNO";
                                    ddlStores.DataBind();
                                    if (lstStores != null && lstStores.Count > 0)
                                    {
                                        lstStores.ForEach(t =>
                                        {
                                            if (contact.LstLocations.Contains(dbContext.LOCATIONID.FirstOrDefault(c => c.ID.Equals(t.SNO))))
                                            {
                                                ddlStores.Items.FindByValue(Convert.ToString(t.SNO, CultureInfo.CurrentCulture)).Selected = true;
                                            }
                                        });
                                    }
                                }
                                break;
                            case "S":
                                lblStores.Visible = false;
                                ddlStores.Visible = false;
                                lblDepartments.Visible = false;
                                ddlDepartments.Visible = false;
                                break;
                            default:
                                break;
                        }
                        Logger.Info("Ends When Edit Button Clicks in ContactUs.aspx");
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }
        private void BindContacts()
        {
            gvViewContact.DataSource = null;
            gvViewContact.DataBind();
            using(ApolloDBContext dbContext=new ApolloDBContext())
            {
                if (dbContext.CONTACTUS.ToList().Count > 0)
                {
                    gvViewContact.DataSource = dbContext.CONTACTUS.ToList();
                    gvViewContact.DataBind();
                }
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            lock (lockTarget)
            {
                if (btnSubmit.Text.Equals(MainResource.Submit))
                {
                    try
                    {
                        Logger.Info("Starts When Submit() button Clicks() in ContactUs.aspx");
                        ContactUS contact = new ContactUS();
                        using(ApolloDBContext dbContext=new ApolloDBContext())
                        {
                            contact.ID = dbContext.CONTACTUS.Count() > 0 ? dbContext.CONTACTUS.Max(c => c.ID) + 1 : 1;
                            contact.Address1 = txtAddress1.Text;
                            contact.Address2 = txtAddress2.Text;
                            contact.CellPhone = txtMobile.Text;
                            contact.City = txtCity.Text;
                            contact.Country = txtCountry.Text;
                            contact.Email = txtEmail.Text;
                            contact.Fax = txtFax.Text;
                            contact.Name = txtName.Text;
                            if (!string.IsNullOrEmpty(txtLatitude.Text) && !string.IsNullOrEmpty(txtLogitude.Text))
                            {
                                string location = String.Format(CultureInfo.CurrentCulture, "POINT({0} {1})", txtLogitude.Text, txtLatitude.Text);
                                contact.Location = DbGeography.FromText(location);
                            }
                            contact.State = txtState.Text;
                            contact.TelePhone = txtTelephone.Text;
                            contact.TollFree = txttollFree.Text;
                            contact.Zip = txtZipCode.Text;
                            contact.LastUpdatedTime = DateTime.Now;
                            if (chkIsActive.Checked)
                            {
                                contact.IsActive = true;
                            }
                            else
                            {
                                contact.IsActive = false;
                            }
                            List<long> lstDepartments = new List<long>();
                            List<long> lstStores = new List<long>();
                            ListBox ddlDepartments = (ListBox)DepartmentControl.FindControl("ddlDepartments");
                            GroupDropDownList.GroupDropDownList ddlStores = (GroupDropDownList.GroupDropDownList)DepartmentControl.FindControl("ddlStores");
                            switch (adminType)
                            {
                                case "A":
                                    lstDepartments = Array.ConvertAll(ddlDepartments.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                                    contact.LstDepartments = dbContext.DEPARTMENTID.Where(c => lstDepartments.Contains(c.ID)).ToList();
                                    lstStores = Array.ConvertAll(ddlStores.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                                    contact.LstLocations = dbContext.LOCATIONID.Where(c => lstStores.Contains(c.ID)).ToList();
                                    contact.RegAppID = adminid;
                                    break;
                                case "D":

                                    contact.LstDepartments = dbContext.DEPARTMENTID.Where(c => c.ID.Equals(adminid)).ToList();
                                    lstStores = Array.ConvertAll(ddlStores.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                                    contact.LstLocations = dbContext.LOCATIONID.Where(c => lstStores.Contains(c.ID)).ToList();
                                    DepartmentDTO dept = syncLogin.Getdepartment();
                                    contact.RegAppID = dept.RegAppID;

                                    break;
                                case "S":
                                    contact.LstLocations = dbContext.LOCATIONID.Where(c => c.ID.Equals(adminid)).ToList();
                                    StoreDTO store = new StoreDTO();
                                    store = syncLogin.GetStoreById();
                                    contact.LstDepartments = dbContext.DEPARTMENTID.Where(c => c.ID.Equals(store.DeptID)).ToList();
                                    contact.RegAppID = store.RegAppID;
                                    break;
                                default:
                                    break;
                            }
                            dbContext.CONTACTUS.Add(contact);
                            dbContext.SaveChanges();
                            ClientScript.RegisterStartupScript(GetType(), "notifier15", "successAlert('Contact Added Successfully');", true);
                            ClearAll();
                      
                            BindContacts();
                            Logger.Info("Ends When Submit() button Clicks() in ContactUs.aspx");
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex, ex.Message);
                    }
                }
                else if (btnSubmit.Text.Equals(MainResource.Update))
                {
                    try
                    {
                        Logger.Info("Starts When Update() button Clicks() in ContactUs.aspx");
                        long id = 0;
                        if (Int64.TryParse(lblID.Text, out id) && id > 0)
                        {
                            using (ApolloDBContext dbContext = new ApolloDBContext())
                            {
                                ContactUS contact = dbContext.CONTACTUS.FirstOrDefault(c => c.ID.Equals(id));
                                contact.Address1 = txtAddress1.Text;
                                contact.Address2 = txtAddress2.Text;
                                contact.CellPhone = txtMobile.Text;
                                contact.City = txtCity.Text;
                                contact.Country = txtCountry.Text;
                                contact.Email = txtEmail.Text;
                                contact.Fax = txtFax.Text;
                                contact.Name = txtName.Text;
                                if (!string.IsNullOrEmpty(txtLatitude.Text) && !string.IsNullOrEmpty(txtLogitude.Text))
                                {
                                    string location = String.Format(CultureInfo.InvariantCulture,"POINT({0} {1})", txtLogitude.Text, txtLatitude.Text);
                                    contact.Location = DbGeography.FromText(location);
                                }
                                contact.State = txtState.Text;
                                contact.TelePhone = txtTelephone.Text;
                                contact.TollFree = txttollFree.Text;
                                contact.Zip = txtZipCode.Text;
                                if (chkIsActive.Checked)
                                {
                                    contact.IsActive = true;
                                }
                                else
                                {
                                    contact.IsActive = false;
                                }
                                List<long> lstDepts = new List<long>();
                                List<long> lstStores = new List<long>();
                                ListBox ddlDepartments = (ListBox)DepartmentControl.FindControl("ddlDepartments");
                                GroupDropDownList.GroupDropDownList ddlStores = (GroupDropDownList.GroupDropDownList)DepartmentControl.FindControl("ddlStores");

                                switch (adminType)
                                {
                                    case "A":
                                        #region Department operations
                                        lstDepts = Array.ConvertAll(ddlDepartments.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                                        List<long> RemoveLstDepartments = contact.LstDepartments.Where(c => !lstDepts.Contains(c.ID)).Select(c => c.ID).ToList();
                                        if (RemoveLstDepartments != null && RemoveLstDepartments.Count > 0)
                                        {
                                            RemoveLstDepartments.ForEach(d =>
                                            {
                                                DepartmentID RemoveDeptDto = contact.LstDepartments.FirstOrDefault(c => c.ID.Equals(d));
                                                if (RemoveDeptDto != null)
                                                {
                                                    contact.LstDepartments.Remove(RemoveDeptDto);
                                                }
                                            });
                                        }
                                        lock (lockTarget)
                                        {
                                            List<DepartmentID> dpt = dbContext.DEPARTMENTID.Where(c => lstDepts.Contains(c.ID)).ToList();
                                            contact.LstDepartments = dpt;
                                        }
                                        #endregion
                                        #region Store Operation
                                        List<long> RemoveLstStores = contact.LstLocations.Where(c => !lstStores.Contains(c.ID)).Select(c => c.ID).ToList();
                                        RemoveLstStores.ToList().ForEach(d =>
                                        {
                                            LocationID RemovelocationDto = contact.LstLocations.ToList().Where(c => c.ID.Equals(d)).FirstOrDefault();
                                            contact.LstLocations.Remove(RemovelocationDto);
                                        });
                                        lstStores = Array.ConvertAll(ddlStores.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                                        lock (lockTarget)
                                        {
                                            List<LocationID> location = dbContext.LOCATIONID.Where(c => lstStores.Contains(c.ID)).ToList();
                                            contact.LstLocations = location;
                                        }
                                        #endregion
                                        break;
                                    case "D":
                                        #region Store Operation
                                        List<long> RemoveLstStores1 = contact.LstLocations.Where(c => !lstStores.Contains(c.ID)).Select(c => c.ID).ToList();
                                        RemoveLstStores1.ToList().ForEach(d =>
                                        {
                                            LocationID RemovelocationDto = contact.LstLocations.ToList().Where(c => c.ID.Equals(d)).FirstOrDefault();
                                            contact.LstLocations.Remove(RemovelocationDto);
                                        });
                                        lstStores = Array.ConvertAll(ddlStores.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => i.Value).ToArray(), long.Parse).ToList();
                                        lock (lockTarget)
                                        {
                                            List<LocationID> location = dbContext.LOCATIONID.Where(c => lstStores.Contains(c.ID)).ToList();
                                            contact.LstLocations = location;
                                        }
                                        #endregion
                                        break;
                                    case "S":
                                        break;
                                    default:
                                        break;
                                }
                                contact.LastUpdatedTime = DateTime.Now;
                                dbContext.SaveChanges();
                                ClientScript.RegisterStartupScript(GetType(), "notifier15", "successAlert('Contact Updated Successfully');", true);
                                BindContacts();
                                CancelUpdate();
                                if (adminType.Equals("A"))
                                {
                                    ddlStores.Items.Clear();
                                }
                                Logger.Info("Ends When Update() button Clicks() in ContactUs.aspx");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex, ex.Message);
                    }
                }
            }
        }

        private void CancelUpdate()
        {
            btnSubmit.Text = MainResource.Submit;
            btnClear.Text = MainResource.Clear;
            lblHead.Text = MainResource.Contact;
            btnUpdateCancel.Visible = false;
            lblID.Text = string.Empty;
            ClearAll();
        }

        private void ClearAll()
        {
            txtAddress1.Text = string.Empty;
            txtAddress2.Text = string.Empty;
            txtCity.Text = string.Empty;
            txtCountry.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtFax.Text = string.Empty;
            txtLatitude.Text = string.Empty;
            txtLogitude.Text = string.Empty;
            txtMobile.Text = string.Empty;
            txtName.Text = string.Empty;
            txtState.Text = string.Empty;
            txtTelephone.Text = string.Empty;
            txttollFree.Text = string.Empty;
            txtZipCode.Text = string.Empty;
            ListBox ddlDepartments = (ListBox)DepartmentControl.FindControl("ddlDepartments");
            GroupDropDownList.GroupDropDownList ddlStores = (GroupDropDownList.GroupDropDownList)DepartmentControl.FindControl("ddlStores");
            ddlDepartments.ClearSelection();
            ddlStores.ClearSelection();
            if (adminType.Equals("A"))
            {
                ddlStores.Items.Clear();
            }
          
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            if (btnClear.Text.Equals(MainResource.Clear))
            {
                ClearAll();
            }
            else if (btnClear.Text.Equals(MainResource.Reset))
            {
                long id = 0;
                if (Int64.TryParse(lblID.Text, out id) && id > 0)
                {
                    using (ApolloDBContext dbContext = new ApolloDBContext())
                    {
                        ContactUS contact = dbContext.CONTACTUS.FirstOrDefault(c => c.ID.Equals(id));
                        txtAddress1.Text = contact.Address1;
                        txtAddress2.Text = contact.Address2;
                        txtCity.Text = contact.City;
                        txtCountry.Text = contact.Country;
                        txtEmail.Text = contact.Email;
                        txtFax.Text = contact.Fax;
                        txtMobile.Text = contact.CellPhone;
                        txtName.Text = contact.Name;
                        txtState.Text = contact.State;
                        txtTelephone.Text = contact.TelePhone;
                        txttollFree.Text = contact.TollFree;
                        txtZipCode.Text = contact.Zip;
                        if (contact.IsActive)
                        {
                            chkIsActive.Checked = true;
                        }
                        else
                        {
                            chkIsActive.Checked = false;
                        }
                    }
                }
            }
        }

        protected void btnUpdateCancel_Click(object sender, EventArgs e)
        {
            CancelUpdate();
        }
        protected void gvViewContact_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            long id = 0;
            if (Int64.TryParse(e.CommandArgument.ToString(), out id) && id > 0)
            {
                if (e.CommandName.Equals(MainResource.Edit))
                {
                    GetEditData(id);
                }
                else if (e.CommandName.Equals(MainResource.Delete))
                {
                    try
                    {
                        Logger.Info("Starts Delete Button in ContactUs.aspx");
                        using (ApolloDBContext dbContext = new ApolloDBContext())
                        {
                            ContactUS contact = dbContext.CONTACTUS.FirstOrDefault(c => c.ID.Equals(id));
                            dbContext.CONTACTUS.Remove(contact);
                            dbContext.SaveChanges();
                            gvViewContact.DataSource = null;
                            gvViewContact.DataBind();
                            gvViewContact.DataSource = dbContext.CONTACTUS.ToList();
                            gvViewContact.DataBind();
                            ClientScript.RegisterStartupScript(GetType(), "notifier15", "successAlert('Contact Deleted Successfully');", true);
                            CancelUpdate();
                        }
                        Logger.Info("Ends Delete Button in ContactUs.aspx");
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex, ex.Message);
                    }
                }
            }
        }

        private void GetEditData(long id)
        {
            try
            {
                lock (lockTarget)
                {
                    Logger.Info("Starts When Edit Button Clicks in ContactUs.aspx");
                    using (ApolloDBContext dbContext = new ApolloDBContext())
                    {
                        ListBox ddlDepartments = (ListBox)DepartmentControl.FindControl("ddlDepartments");
                        GroupDropDownList.GroupDropDownList ddlStores = (GroupDropDownList.GroupDropDownList)DepartmentControl.FindControl("ddlStores");
                        Label lblDepartments = (Label)DepartmentControl.FindControl("lblDepartments");
                        Label lblStores = (Label)DepartmentControl.FindControl("lblStores");
                        ContactUS contact = dbContext.CONTACTUS.FirstOrDefault(c => c.ID.Equals(id));
                        lblID.Text = Convert.ToString(id,CultureInfo.CurrentCulture);
                        txtAddress1.Text = contact.Address1;
                        txtAddress2.Text = contact.Address2;
                        txtCity.Text = contact.City;
                        txtCountry.Text = contact.Country;
                        txtEmail.Text = contact.Email;
                        txtFax.Text = contact.Fax;
                        txtMobile.Text = contact.CellPhone;
                        txtName.Text = contact.Name;
                        txtState.Text = contact.State;
                        txtTelephone.Text = contact.TelePhone;
                        txttollFree.Text = contact.TollFree;
                        txtZipCode.Text = contact.Zip;
                        if (contact.IsActive)
                        {
                            chkIsActive.Checked = true;
                        }
                        else
                        {
                            chkIsActive.Checked = false;
                        }
                        switch (adminType)
                        {
                            case "A":
                                lblDepartments.Visible = true;
                                lblStores.Visible = true;
                                ddlDepartments.Visible = true;
                                ddlStores.Visible = true;
                                break;
                            case "D":
                                lblDepartments.Visible = false;
                                ddlDepartments.Visible = false;
                                ddlStores.Visible = true;
                                lblStores.Visible = true;
                                break;
                            case "S":
                                lblStores.Visible = false;
                                ddlStores.Visible = false;
                                lblDepartments.Visible = false;
                                ddlDepartments.Visible = false;
                                break;
                            default:
                                break;
                        }
                        List<StoreDTO> lstStores = new List<StoreDTO>();
                        if (contact.LstDepartments != null)
                        {
                            contact.LstDepartments.ForEach(c =>
                            {
                                ddlDepartments.Items.FindByValue(Convert.ToString(c.ID, CultureInfo.CurrentCulture)).Selected = true;
                                lstStores.AddRange(syncLogin.GetStoresUnderDepartment(c.ID));
                            });

                            ddlStores.Items.Clear();
                            ddlStores.DataSource = lstStores;
                            ddlStores.DataGroupField = "DeptName";
                            ddlStores.DataTextField = "StoreName";
                            ddlStores.DataValueField = "SNO";
                            ddlStores.DataBind();
                            if (contact.LstLocations != null)
                            {
                                if (contact.LstLocations.Count > 0)
                                {
                                    contact.LstLocations.ToList().ForEach(t =>
                                    {
                                        ddlStores.Items.FindByValue(Convert.ToString(t.ID,CultureInfo.CurrentCulture)).Selected = true;
                                    });
                                }
                            }
                        }
                        btnUpdateCancel.Visible = true;
                        btnSubmit.Text = MainResource.Update;
                        btnClear.Text = MainResource.Reset;
                        lblHead.Text = MainResource.EditContact;
                        //lblPageHeading.Text = MainResource.EditContact;
                        upd1.Update();
                        upd2.Update();
                        Logger.Info("Ends When Edit Button Clicks in ContactUs.aspx");
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }

        protected void gvViewContact_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void gvViewContact_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }
    }
}