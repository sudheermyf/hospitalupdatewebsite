﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ApolloWebsite
{
    public partial class SiteMasterPage : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["adminloginid"]!=null)
            {
                if (Convert.ToString(Session["adminType"], CultureInfo.CurrentCulture).Equals("A"))
                {
                    lblHead.Text = "Welcome Admin";
                }
                else if(Convert.ToString(Session["adminType"],CultureInfo.CurrentCulture)=="D" || Convert.ToString(Session["adminType"],CultureInfo.CurrentCulture)=="S")
                {
                    lblHead.Text = "Welcome" + " " + Convert.ToString(Session["adminusername"],CultureInfo.CurrentCulture);
                }
            }
            else
            {
                Response.Redirect("Default.aspx", false);
            }
        }

        protected void imgL_Click(object sender, ImageClickEventArgs e)
        {
            if (Session["adminloginid"]!=null)
            {
                //Response.Cookies["username"].Expires = DateTime.Now.AddDays(-1);
                //Response.Cookies["password"].Expires = DateTime.Now.AddDays(-1);
                Session.Abandon();
                Response.Redirect("~/Home.aspx");
            }
        }
    }
}